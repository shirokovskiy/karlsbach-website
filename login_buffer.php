<?php
// redirect site to proper address, eliminates confirmation @ refresh
ob_start();
session_start();

require_once 'conf.php';

header('Location: '.$_POST['redirect']);
exit;
