<?php
ob_start();
session_start();
session_destroy();

require_once 'conf.php';

header('Location: '.URL.'/dms/');
exit;
