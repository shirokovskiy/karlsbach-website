<?php
session_start();
include('conf.php');

if ($_SESSION['pricelist_login'] == 'login') {
	$sql = 'SELECT *
			FROM '.SQL_PREFIX.'files
			WHERE id="'.intval($_GET['id']).'"';
	$res = $db->query($sql);
	$row = $res->getRow(0);

// We'll be outputting a PDF
	header('Content-type: application/pdf');

// It will be called downloaded.pdf
	header('Content-Disposition: attachment; filename="pricelist.pdf"');

// The PDF source is in original.pdf
	readfile($row['path'].$row['filename']);
} else {
	header('Location: /');
	exit;
}

