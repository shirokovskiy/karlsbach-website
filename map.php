<!doctype html>

<html lang="en">
<head>
    <style>
        .map { width: 100%; height: 400px; }
    </style>
</head>
<body>
<div id="map_canvas" class="map"></div>
    <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
    <script type="text/javascript" src="http://jquery-ui-map.googlecode.com/svn/trunk/demos/js/jquery-1.7.1/jquery.min.js"></script>
    <script type="text/javascript" src="js/jquery.ui.map.js"></script>
    <script type="text/javascript">

$(function() {
	$('#map_canvas').gmap().bind('init', function() {
        // This URL won't work on your localhost, so you need to change it
        // see http://en.wikipedia.org/wiki/Same_origin_policy
		$.getJSON( 'mapdata.php', function(data) {
			$.each( data.markers, function(i, marker) {
				$('#map_canvas').gmap('addMarker', {
					'position': new google.maps.LatLng(marker.latitude, marker.longitude)
				}).click(function() {
					$('#map_canvas').gmap('openInfoWindow', { 'content': marker.content }, this);
				});
			});
		});
	});

	$('#map_canvas').gmap('option', 'center', new google.maps.LatLng(58.510465, 26.898994));
	$('#map_canvas').gmap('option', 'zoom', 10);
});

/*
    $(function() {
        demo.add(function() {
            $('#map_canvas').gmap({'center': '58.510465,26.898994', 'zoom': 14, 'disableDefaultUI':true, 'callback': function() {
                var self = this;
                self.addMarker({'position': this.get('map').getCenter() }).click(function() {
                    self.openInfoWindow({ 'content': '<div style="color: #d00;">Hello World!<br />tere</div>' }, this);
                });
            }});
            $('#map_canvas').gmap({'center': '58.509927,26.892214', 'zoom': 14, 'disableDefaultUI':true, 'callback': function() {
                var self = this;
                self.addMarker({'position': this.get('map').getCenter() }).click(function() {
                    self.openInfoWindow({ 'content': '<div style="color: #d00;">Hello World!<br />tere</div>' }, this);
                });
            }});
        }).load();
    });
*/
    </script>
</body>
</html>
