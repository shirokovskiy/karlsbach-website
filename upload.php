<?php
/*TODO:auth check here*/
session_start();

if (isset($_POST["PHPSESSID"])) {
	session_id($_POST["PHPSESSID"]);
} else if (isset($_GET["PHPSESSID"])) {
	session_id($_GET["PHPSESSID"]);
}

$POST_MAX_SIZE = ini_get('post_max_size');
$unit = strtoupper(substr($POST_MAX_SIZE, -1));
$multiplier = ($unit == 'M' ? 1048576 : ($unit == 'K' ? 1024 : ($unit == 'G' ? 1073741824 : 1)));

if ((int)$_SERVER['CONTENT_LENGTH'] > $multiplier*(int)$POST_MAX_SIZE && $POST_MAX_SIZE) {
	header("HTTP/1.1 500 Internal Server Error"); // This will trigger an uploadError event in SWFUpload
	echo "POST exceeded maximum allowed size.";
	exit(0);
}

// Settings
// upload image dir format:
// images/YYYY-MM

$save_path = 'upload/images/';

if ($_GET['fileupload'])
	$save_path = 'upload/files/';

$date_add = date('Y-m/');
if (!file_exists($save_path.$date_add)) {
	mkdir($save_path.$date_add);
	chmod($save_path.$date_add, 0777);
	if (!$_GET['fileupload']) {
		mkdir($save_path.$date_add.'_orig/');
		chmod($save_path.$date_add.'_orig/', 0777);
		mkdir($save_path.$date_add.'tnb/');
		chmod($save_path.$date_add.'tnb/', 0777);
		mkdir($save_path.$date_add.'475/');
		chmod($save_path.$date_add.'475/', 0777);
		mkdir($save_path.$date_add.'644/');
		chmod($save_path.$date_add.'644/', 0777);
		mkdir($save_path.$date_add.'270/');
		chmod($save_path.$date_add.'270/', 0777);
	}
}
$save_path.= $date_add;

$upload_name = "Filedata";
$max_file_size_in_bytes = 4147483647;				// 2GB in bytes
//	$extension_whitelist = array("jpg", "gif", "png");	// Allowed file extensions
if (!$_GET['fileupload'])
	$extension_whitelist = array("jpg");	// Allowed file extensions
else
	$extension_whitelist = array("pdf");	// Allowed file extensions
$valid_chars_regex = '.A-Z0-9_ !@#$%^&()+={}\[\]\',~`-';				// Characters allowed in the file name (in a Regular Expression format)

// Other variables
$MAX_FILENAME_LENGTH = 260;
$file_name = "";
$file_extension = "";
$uploadErrors = array(
	0 => "There is no error, the file uploaded with success",
	1 => "The uploaded file exceeds the upload_max_filesize directive in php.ini",
	2 => "The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form",
	3 => "The uploaded file was only partially uploaded",
	4 => "No file was uploaded",
	6 => "Missing a temporary folder"
);

// Validate the upload
if (!isset($_FILES[$upload_name])) {
	HandleError("No upload found in \$_FILES for " . $upload_name);
	exit(0);
} else if (isset($_FILES[$upload_name]["error"]) && $_FILES[$upload_name]["error"] != 0) {
	HandleError($uploadErrors[$_FILES[$upload_name]["error"]]);
	exit(0);
} else if (!isset($_FILES[$upload_name]["tmp_name"]) || !@is_uploaded_file($_FILES[$upload_name]["tmp_name"])) {
	HandleError("Upload failed is_uploaded_file test.");
	exit(0);
} else if (!isset($_FILES[$upload_name]['name'])) {
	HandleError("File has no name.");
	exit(0);
}

// Validate the file size (Warning: the largest files supported by this code is 2GB)
$file_size = @filesize($_FILES[$upload_name]["tmp_name"]);
if (!$file_size || $file_size > $max_file_size_in_bytes) {
	HandleError("File exceeds the maximum allowed size");
	exit(0);
}

if ($file_size <= 0) {
	HandleError("File size outside allowed lower bound");
	exit(0);
}

// Validate file name (for our purposes we'll just remove invalid characters)
$file_name = preg_replace('/[^'.$valid_chars_regex.']|\.+$/i', "", basename($_FILES[$upload_name]['name']));
if (strlen($file_name) == 0 || strlen($file_name) > $MAX_FILENAME_LENGTH) {
	HandleError("Invalid file name");
	exit(0);
}

// Validate that we won't over-write an existing file
function RandomString($len) {
	srand(date("s"));

	while($i < $len) {
		$str.= chr((rand()%26) + 97);
		$i++;
	}

	$str = $str.substr(uniqid (""), 0, 22);

	return $str;
}

$file_name = RandomString(5).$file_name;
$file_name = str_replace(' ', '_', $file_name);

if (file_exists($save_path . $file_name)) {
	HandleError("File with this name already exists");
	exit(0);
}

// Validate file extension
$path_info = pathinfo($_FILES[$upload_name]['name']);
$file_extension = $path_info["extension"];
$is_valid_extension = false;

foreach ($extension_whitelist as $extension) {
	if (strcasecmp($file_extension, $extension) == 0) {
		$is_valid_extension = true;
		break;
	}
}

if (!$is_valid_extension) {
	HandleError("Invalid file extension");
	exit(0);
}

if (!@move_uploaded_file($_FILES[$upload_name]["tmp_name"], $save_path.$file_name)) {
	HandleError("File could not be saved.");
	exit(0);
} else {
	include('conf.php');
	include('setup.php');

$sql = 'SELECT *
		FROM '.SQL_PREFIX.'page
		WHERE id="'.intval($_GET['id']).'"';
$res = $db->query($sql);
$page = $res->getRow(0);

if ($page['parent'] != 'parent') {
	$sql = 'SELECT *
			FROM '.SQL_PREFIX.'page
			WHERE id="'.$row['parent'].'"';
	$res = $db->query($sql);
	$parent = $res->getRow(0);
}

	function addWaterMark($image, $target, $watermark = 'flagsmall.jpg') {
$uploaded = imagecreatefromjpeg($image);
$image = imagecreatetruecolor(IMAGE_WIDTH, IMAGE_HEIGHT);
imagecopyresampled($image, $uploaded, 0, 0, 0, 0, IMAGE_WIDTH, IMAGE_HEIGHT, imagesx($uploaded), imagesy($uploaded));
imagealphablending($image,true); //allows us to apply a 24-bit watermark over $image

//Load the sold watermark
$sold_band = imagecreatefrompng($watermark);
imagealphablending($sold_band,true);

//Apply watermark and save
$image = image_overlap($image, $sold_band);
imagecopy($image,$sold_band,IMAGE_WIDTH - SOLD_WIDTH,IMAGE_HEIGHT - SOLD_HEIGHT,0,0,SOLD_WIDTH,SOLD_HEIGHT);
$success = imagejpeg($image,$target,85);
	}

	function ResizeJPGOLD($sourcefile, $targetfile, $jpegqual, $maxsize) {
		$picsize = getimagesize($sourcefile);
		$src_x  = $picsize[0];
		$src_y  = $picsize[1];
		$src_ratio = $src_y / $src_x;

		$pic_w = $maxsize;
		$pic_h = round($maxsize * $src_ratio);
		$src_y = round($src_x * $src_ratio);
		$start_y = round(($picsize[1] - $src_y) / 2);

		if (!$start_x)
			$start_x = 0;
		if (!$start_y)
			$start_y = 0;

		if ($start_y < 0) {
			$start_y = 0;
			$src_y = $picsize[1];
			$src_x = round($src_y / $src_ratio);
			$start_x = round(($picsize[0] - $src_x) / 2);
		}

		$source_id = imagecreatefromjpeg($sourcefile);
		$target_id = imagecreatetruecolor($pic_w, $pic_h);
		$target_pic = imagecopyresampled($target_id, $source_id,
										0, 0, $start_x, $start_y,
										$pic_w, $pic_h,
										$src_x, $src_y);
		imagejpeg ($target_id, $targetfile, $jpegqual);

		return true;
	}

	function ResizeJPGOLDheight($sourcefile, $targetfile, $jpegqual, $maxsize) {
		$picsize = getimagesize($sourcefile);
		$src_x  = $picsize[0];
		$src_y  = $picsize[1];
		$src_ratio = $src_y / $src_x;

		$pic_h = $maxsize;
		$pic_w = round($maxsize / $src_ratio);
		$src_y = round($src_x * $src_ratio);
		$start_y = round(($picsize[1] - $src_y) / 2);

		if (!$start_x)
			$start_x = 0;
		if (!$start_y)
			$start_y = 0;

		if ($start_y < 0) {
			$start_y = 0;
			$src_y = $picsize[1];
			$src_x = round($src_y / $src_ratio);
			$start_x = round(($picsize[0] - $src_x) / 2);
		}

		if ($maxsize == 356 && $pic_w < 356) {
			$pic_h = $pic_h * 356 / $pic_w;
			$pic_w = 356;
		}
		if ($maxsize == 200 && $pic_w < 185) {
			$pic_h = $pic_h * 185 / $pic_w;
			$pic_w = 185;
		}
		if ($maxsize == 100 && $pic_w < 81) {
			$pic_h = $pic_h * 81 / $pic_w;
			$pic_w = 81;
		}
		if ($maxsize == 380 && $pic_w < 321) {
			$pic_h = $pic_h * 321 / $pic_w;
			$pic_w = 321;
		}
		if ($maxsize == 270 && $pic_w < 380) {
			$pic_h = $pic_h * 380 / $pic_w;
			$pic_w = 380;
		}

		$source_id = imagecreatefromjpeg($sourcefile);
		$target_id = imagecreatetruecolor($pic_w, $pic_h);
		$target_pic = imagecopyresampled($target_id, $source_id,
										0, 0, $start_x, $start_y,
										$pic_w, $pic_h,
										$src_x, $src_y);
		imagejpeg ($target_id, $targetfile, $jpegqual);

		return true;
	}

function resizeJPG($sourcefile, $targetfile, $jpegqual, $maxX, $maxY, $type = 'resize') {
	$picsize = getimagesize($sourcefile);
	$src_x = $picsize[0];
	$src_y = $picsize[1];
	$src_ratio = $src_y / $src_x;

	if ($type == 'resize') {
		$pic_w = $maxX;
		$pic_h = round($maxX * $src_ratio);
		$src_y = round($src_x * $src_ratio);
		$start_y = round(($picsize[1] - $src_y) / 2);

		if (!$start_x)
			$start_x = 0;
		if (!$start_y)
			$start_y = 0;

		if ($start_y < 0) {
			$start_y = 0;
			$src_y = $picsize[1];
			$src_x = round($src_y / $src_ratio);
			$start_x = round(($picsize[0] - $src_x) / 2);
		}
	}

	$source_id = imagecreatefromjpeg($sourcefile);
	$target_id = imagecreatetruecolor($pic_w, $pic_h);
	$target_pic = imagecopyresampled($target_id, $source_id,
									0, 0, $start_x, $start_y,
									$pic_w, $pic_h,
									$src_x, $src_y);
	imagejpeg ($target_id, $targetfile, $jpegqual);

	return true;
}
/*
	$picsize = getimagesize($save_path.$file_name);
	$source_x  = $picsize[0];
	$source_y  = $picsize[1];

	$big = 800;
	$tnb = 200;

	copy($save_path.$file_name, $save_path.'_orig/'.$file_name);
	ResizeJPG($save_path.$file_name, $save_path.'tnb/'.$file_name, 90, $tnb);

	if (($source_x > $source_y && $source_x > $big) || ($source_x < $source_y && $source_y > $big))
		ResizeJPG($save_path.$file_name, $save_path.'/'.$file_name, 90, $big);
	else
		copy($save_path.$file_name, $save_path.'/'.$file_name);
*/

$f = fopen('a.txt', 'w');

if ($_GET['page'] == 'portfolio') {
	$dir = 'upload/portfolio/';

	copy($save_path.$file_name, $dir.$file_name);
	unlink($save_path.$file_name);

	for ($i = 0; $i < count($setup[$_GET['page']]['specpicresizeparam']); $i++) {
		$param = $setup[$_GET['page']]['specpicresizeparam'][$i];

		if (!file_exists($dir.$param['dir'])) {
			mkdir($dir.$param['dir']);
			chmod($dir.$param['dir'], 0777);
		}

		resizeJPG($dir.$file_name, $dir.$param['dir'].'/'.$file_name, $param['quality'], $param['x'], $param['y']);
		@chmod($dir.$param['dir'].'/'.$file_name, 0777);
	}
} elseif ($_GET['page'] == 'headerimage') {
	$dir = 'upload/header/';

	copy($save_path.$file_name, $dir.$file_name);
	unlink($save_path.$file_name);

	@chmod($dir.$file_name, 0777);
} else {
	if (!$_GET['fileupload']) {

		$picsize = getimagesize($save_path.$file_name);
		$source_x  = $picsize[0];
		$source_y  = $picsize[1];

//save original
		copy($save_path.$file_name, $save_path.'_orig/'.$file_name);

//max size
		$size = 800;
		if (($source_x > $source_y && $source_x > $size) || ($source_x < $source_y && $source_y > $size))
			ResizeJPGOLD($save_path.'_orig/'.$file_name, $save_path.'/'.$file_name, 90, $size);
		else
			copy($save_path.$file_name, $save_path.'/'.$file_name);

//tnb
		$size = 200;
		if (($source_x > $source_y && $source_x > $size) || ($source_x < $source_y && $source_y > $size))
			ResizeJPGOLD($save_path.'_orig/'.$file_name, $save_path.'tnb/'.$file_name, 90, $size);
		else
			copy($save_path.$file_name, $save_path.'tnb/'.$file_name);

//475
		$size = 475;
		if (($source_x > $source_y && $source_x > $size) || ($source_x < $source_y && $source_y > $size))
			ResizeJPGOLDheight($save_path.'_orig/'.$file_name, $save_path.$size.'/'.$file_name, 90, $size);
		else
			copy($save_path.$file_name, $save_path.$size.'/'.$file_name);

//644
		$size = 644;
		if (($source_x > $source_y && $source_x > $size) || ($source_x < $source_y && $source_y > $size))
			ResizeJPG($save_path.'_orig/'.$file_name, $save_path.$size.'/'.$file_name, 90, $size);
		else
			copy($save_path.$file_name, $save_path.$size.'/'.$file_name);

//200
		$size = 200;
		if (($source_x > $source_y && $source_x > $size) || ($source_x < $source_y && $source_y > $size))
			ResizeJPGOLDheight($save_path.'_orig/'.$file_name, $save_path.$size.'/'.$file_name, 90, $size);
		else
			copy($save_path.$file_name, $save_path.$size.'/'.$file_name);

//100
		$size = 100;
		if (($source_x > $source_y && $source_x > $size) || ($source_x < $source_y && $source_y > $size))
			ResizeJPGOLDheight($save_path.'_orig/'.$file_name, $save_path.$size.'/'.$file_name, 90, $size);
		else
			copy($save_path.$file_name, $save_path.$size.'/'.$file_name);

//380
		$size = 380;
		if (($source_x > $source_y && $source_x > $size) || ($source_x < $source_y && $source_y > $size))
			ResizeJPGOLDheight($save_path.'_orig/'.$file_name, $save_path.$size.'/'.$file_name, 90, $size);
		else
			copy($save_path.$file_name, $save_path.$size.'/'.$file_name);

//270
		$size = 270;
		if (($source_x > $source_y && $source_x > $size) || ($source_x < $source_y && $source_y > $size))
			ResizeJPGOLDheight($save_path.'_orig/'.$file_name, $save_path.$size.'/'.$file_name, 90, $size);
		else
			copy($save_path.$file_name, $save_path.$size.'/'.$file_name);


		chmod($save_path.'tnb/'.$file_name, 0755);
		chmod($save_path.'_orig/'.$file_name, 0755);
		chmod($save_path.'475/'.$file_name, 0755);
		chmod($save_path.'644/'.$file_name, 0755);
		chmod($save_path.'300/'.$file_name, 0755);
		chmod($save_path.'150/'.$file_name, 0755);
		chmod($save_path.'380/'.$file_name, 0755);
		chmod($save_path.'270/'.$file_name, 0755);
	}
fputs($f, $save_path.$file_name.'
');
	chmod($save_path.$file_name, 0755);
}

	if ($_GET['page'] == 'headerimage') {
		$sql = 'SELECT max(jrk)
				FROM '.SQL_PREFIX.'header
				WHERE parent="'.intval($_GET['id']).'"';
		$res = $db->query($sql);
		$row = $res->getRow(0);
		$jrk = $row[0] + 1;

		$sql = 'INSERT
				INTO '.SQL_PREFIX.'header (filename, jrk)
				VALUES ("'.$file_name.'", "'.$jrk.'")';
		$res = $db->query($sql);
	} else {
		if ($_GET['fileupload']) {
			$sql = 'SELECT max(jrk)
					FROM '.SQL_PREFIX.'files'.($_GET['page'] == 'portfolio' ? '_portfolio' : '').'
					WHERE parent="'.intval($_GET['id']).'"';
			$res = $db->query($sql);
			$row = $res->getRow(0);
			$jrk = $row[0] + 1;

fputs($f, $sql.'
');

			$sql = 'INSERT
					INTO '.SQL_PREFIX.'files'.($_GET['page'] == 'portfolio' ? '_portfolio' : '').' (parent, jrk, filename, path)
					VALUES ("'.intval($_GET['id']).'", "'.$jrk.'", "'.$file_name.'", "'.($_GET['page'] == 'portfolio' ? '' : $save_path).'")';
			$res = $db->query($sql);

fputs($f, $sql.'
');
		} else {
			$sql = 'SELECT max(jrk)
					FROM '.SQL_PREFIX.'images'.($_GET['page'] == 'portfolio' ? '_portfolio' : '').'
					WHERE parent="'.intval($_GET['id']).'"';
			$res = $db->query($sql);
			$row = $res->getRow(0);
			$jrk = $row[0] + 1;

			$sql = 'INSERT
					INTO '.SQL_PREFIX.'images'.($_GET['page'] == 'portfolio' ? '_portfolio' : '').' (parent, jrk, filename, path)
					VALUES ("'.intval($_GET['id']).'", "'.$jrk.'", "'.$file_name.'", "'.($_GET['page'] == 'portfolio' ? '' : $save_path).'")';
			$res = $db->query($sql);
		}
	}

}

fclose($f);
exit(0);

/* Handles the error output. This error message will be sent to the uploadSuccess event handler.  The event handler
will have to check for any error messages and react as needed. */
function HandleError($message) {
	echo $message;
}
