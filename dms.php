<?php
ob_start();
session_start();

require_once 'conf.php';

//TODO:check to not allow hacks

require_once 'class/admin.php';
$admin = new Admin();

if ($_GET['act'] == 'update_list' && $_POST['imageupload_delete'] == 1) {
	if ($_GET['page'] == 'portfolio')
		$_GET['act'] = 'portfolio_delete';

	if ($_GET['type'] == 'gallery') {
		$_GET['act'] = 'image_delete';
	} else {
		if ($_GET['page'] != 'portfolio') {
			$_GET['act'] = 'item_delete';
		}
	}
}
if ($_GET['act'] == 'update_list' && $_POST['fileupload_delete'] == 1) {
	if ($_GET['type'] == 'fileuploader')
		$_GET['act'] = 'file_delete';
}

//direct language "change" to change language display info rather than saving order list
if ($_GET['act'] == 'update_list' && $_GET['page'] == 'lang' && $_POST['bypass'] == 1)
	$_GET['act'] = 'update_item';

if ($_GET['page'] == 'ftp' && $_GET['act'] != 'delete_item')
	$_GET['act'] = $_GET['page'].'_'.$_GET['act'];

$func_name = 'admin_'.$_GET['act'];

if (method_exists($admin, $func_name))
	call_user_func_array(array('admin', $func_name), array($_GET['param'], $_GET['id'], $_GET['redirect']));
else
	if ($_GET['act'])
		$admin->die2('no function [hint=<strong>'.$_GET['act'].'</strong>] found! dying...');
	else
		$admin->die2('dying...');
