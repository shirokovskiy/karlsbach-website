<?php
if (isset($_GET['act']) && ($_GET['act'] == 'add_item' || $_GET['act'] == 'update_item')) {
	$this->showeditorStores();
} else {
	$attr = array(
		'obj' 			=> true,
		'table'			=> 'stores',
		'where'			=> 'parent="'.(isset($_GET['parent'])?intval($_GET['parent']):0).'"',
		'recursive' 	=> true,
		'level' 		=> 0,
		'order'			=> 'jrk DESC',
	);

	$list = $this->showList($attr, 'store');
	$this->outPut($list);
}
