<?php
if ($this->isLogged()) {
	$this->dms_mainmenu();
	$this->outPut('<div class="dmscontainer">');
	$this->dms_menu();
	$this->outPut('<div class="main">');
	$this->dms_content();
} else {
	$this->loginForm();
}
