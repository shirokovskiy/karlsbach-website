<?php
if (isset($_GET['act'])) {
    if ($_GET['act'] == 'add_item' || $_GET['act'] == 'update_item') {
        $this->showEditor();
    } elseif ($_GET['act'] == 'gallery') {
        $this->imageUploader();
    } elseif ($_GET['act'] == 'files') {
        $this->fileUploader();
    }
} else {
	$attr = array(
		'obj' 			=> true,
		'where' 		=> 'parent="parent" AND page="site" AND menu="'.(isset($_GET['p']) ? $_GET['p'] : 'main').'"',
		'where_add'		=> ' AND page="site"',
		'recursive' 	=> true,
		'level' 		=> 0,
		'order'			=> 'jrk',
	);

	$list = $this->showList($attr, 'mainlist');
	$this->outPut($list);
}
