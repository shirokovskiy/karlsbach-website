<?php
if ($_GET['act'] == 'add_item' || $_GET['act'] == 'update_item') {
	$this->showEditor();
} elseif ($_GET['act'] == 'gallery') {
	$this->imageUploader();
} else {
	$attr = array(
		'obj' 			=> true,
		'table'			=> 'portfolio',
		'where'			=> 'parent="'.$_GET['parent'].'"',
		'level' 		=> 0,
		'order'			=> 'jrk DESC',
	);

	$list = $this->showList($attr);
	$this->outPut($list);
}
