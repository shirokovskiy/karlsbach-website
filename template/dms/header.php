<?php
ini_set("post_max_size", "300M");
ini_set("upload_max_filesize", "300M");
?>
<!DOCTYPE html>
<html>

<head>
  <script type="text/javascript" src="/js/jquery-1.9.1.js"></script>
  <script type="text/javascript" src="/js/jquery-ui.js"></script>

  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <link rel="stylesheet" type="text/css" href="<?php echo DIR?>css/styles.css" media="screen" />
  <link rel="shortcut icon" type="image/x-icon" href="<?php echo DIR?>favicon.ico" />
<?php echo $this->page_info()?>

<!--[if IE]>
  <link rel="stylesheet" type="text/css" href="<?php echo DIR?>css/styles_ie.css" media="screen" /><![endif]-->
<!--[if lt IE 8]>
  <link rel="stylesheet" type="text/css" href="<?php echo DIR?>css/styles_ie7.css" media="screen" /><![endif]-->
<!--[if lt IE 7]>
  <link rel="stylesheet" type="text/css" href="<?php echo DIR?>css/styles_ie6.css" media="screen" /><![endif]-->
  <link rel="stylesheet" type="text/css" href="<?php echo DIR?>css/background.php" media="screen" />

<!--
  <script type="text/javascript" src="<?php echo URL?>/js/jquery.tooltip.js"></script>
-->

  <script type="text/javascript" src="<?php echo URL?>/ckeditor/ckeditor.js"></script>
  <script type="text/javascript" src="<?php echo URL?>/ckfinder/ckfinder.js"></script>
  <script type="text/javascript" src="<?php echo URL?>/js/multisortable.js"></script>

  <script src="http://maps.google.com/maps/api/js?sensor=false" type="text/javascript" ></script>
  <script type="text/javascript" src="<?php echo URL?>/js/jquery.ui.map.js"></script>
  <script type="text/javascript" src="https://www.google.com/jsapi"></script>

  <script>
$(document).ready(function() {
/*	$('.orderlist').multisortable({selectedClass:'orderlist-selected'});*/
	$('.orderlist').sortable();
	$('.tooltip').tooltip({showURL: false});
	$('.datepicker').datepicker({
		firstDay: 1,
		monthNames: ['Jaanuar','Veebruar','Märts','Aprill','Mai','Juuni','Juuli','August','September','Oktoober','November','Detsember'],
		monthNamesShort: ['Jaan','Veeb','Mär','Apr','Mai','Juun','Juul','Aug','Sept','Okt','Nov','Dets'],
		nextText: '>>',
		prevText: '<<',
		dayNames: ['Pühapäev', 'Esmaspäev', 'Teisipäev', 'Kolmapäev', 'Neljapäev', 'Reede', 'Laupäev'],
		dayNamesMin: ['P', 'E', 'T', 'K', 'N', 'R', 'L'],
		currentText: 'Täna',
		closeText: 'Valmis'
	});

	if ($("#gmessage").length > 0)
		$('#gmessage').fadeIn(200).delay(5000).fadeOut(300);

	$('.selectall').click(function() {
		$("INPUT[type='checkbox']").attr('checked', true);
	});
	$('.unselectall').click(function() {
		$("INPUT[type='checkbox']").attr('checked', false);
	});

	$('#bonus_price').bind("change keyup input",function() {
		val = $(this).val()
		val = 100 - (val * 100 / $('#bonus_oldprice').val()).toFixed(2)
		$('#bonus_percent').val(val)
	});
	$('#bonus_percent').bind("change keyup input",function() {
		val = $(this).val()
		val = 100 - (val * $('#bonus_oldprice').val() / 100).toFixed(2)
		$('#bonus_price').val(val)
	});

	$('.google_maps_detector_button').bind('click', function() {
		var address = strip_tags($('#map_address').val());
		var type = $('#store_maintype').val();
		var zoom = 18;

		if (type == 0)
			zoom = 10;
		else if (type == 1)
			zoom = 14;

//need ../ as for the system we're in 'dms' directory
//		$('#map_canvas').load("../getmap.php?address="+address);

		$.get('../getmap.php?address=' + address, function(data){
			if (data) {
				var loc = data + ',' + zoom;

				$('#map_location').val(loc);
				alert('<?php echo $this->trans("editor_map_locator_succsess")?> ' + loc);
			} else {
				alert('<?php echo $this->trans("editor_map_locator_fail")?>');
			}
		});
	});
});

function strip_tags(html){
	//PROCESS STRING
	if(arguments.length < 3) {
		html=html.replace(/<\/?(?!\!)[^>]*>/gi, '');
	} else {
		var allowed = arguments[1];
		var specified = eval("["+arguments[2]+"]");
		if(allowed){
			var regex='</?(?!(' + specified.join('|') + '))\b[^>]*>';
			html=html.replace(new RegExp(regex, 'gi'), '');
		} else{
			var regex='</?(' + specified.join('|') + ')\b[^>]*>';
			html=html.replace(new RegExp(regex, 'gi'), '');
		}
	}

	//CHANGE NAME TO CLEAN JUST BECAUSE
	var clean_string = html.split(' ').join('+');

	//RETURN THE CLEAN STRING
	return clean_string;
}

</script>
<?php if ((isset($_GET['act']) && $_GET['act'] == 'gallery') || (isset($_GET['page']) && $_GET['page'] == 'headerimage') || $this->is_filesuploader()) { ?>
  <script type="text/javascript" src="<?php echo URL?>/js/swfupload.js"></script>
  <script type="text/javascript" src="<?php echo URL?>/js/swfupload.queue.js"></script>
  <script type="text/javascript" src="<?php echo URL?>/js/fileprogress.js"></script>
  <script type="text/javascript" src="<?php echo URL?>/js/handlers.js"></script>

  <script type="text/javascript">
var swfu;

window.onload = function() {
	var settings = {
		flash_url : "<?php echo URL?>/swfupload.swf",
		upload_url: "<?php echo URL?>/upload.php?id=<?php echo $_GET['id']?>&page=<?php echo $_GET['page'].($this->is_filesuploader() ? '&fileupload=true' : '')?>",
		just_a_test: "",
		post_params: {"PHPSESSID" : "<?php echo session_id()?>"},
		file_size_limit : "300 MB",
		file_types : "<?php echo ($this->is_filesuploader() ? '*' : '*.jpg')?>",
		file_types_description : "<?php echo ($this->is_filesuploader() ? $this->trans('misc_allfiles') : $this->trans('misc_files_JPG'))?>",
		file_upload_limit : 100,
		file_queue_limit : 0,
		custom_settings : {
			progressTarget : "fsUploadProgress",
			cancelButtonId : "btnCancel"
		},
		debug: false,

		// Button settings
		button_image_url: "<?php echo DIR?>images/browsebg.gif",
		button_width: "65",
		button_height: "29",
		button_placeholder_id: "spanButtonPlaceHolder",
		button_text: '<span class="theFont"><?php echo $this->trans("button_browse")?></span>',
		button_text_style: ".theFont { font: bold 11px Verdana, Arial, Helvetica, sans-serif !important; text-align: center; }",
		button_text_left_padding: 0,
		button_text_top_padding: 4,

		// The event handler functions are defined in handlers.js
		file_queued_handler : fileQueued,
		file_queue_error_handler : fileQueueError,
		file_dialog_complete_handler : fileDialogComplete,
		upload_start_handler : uploadStart,
		upload_progress_handler : uploadProgress,
		upload_error_handler : uploadError,
		upload_success_handler : uploadSuccess,
		upload_complete_handler : uploadComplete,
		queue_complete_handler : queueComplete	// Queue plugin event
	};

	swfu = new SWFUpload(settings);
};
  </script>
<?php } ?>

  <script type="text/javascript" src="<?php echo URL?>/js/browser.detect.js"></script>
  <script type="text/javascript" src="<?php echo URL?>/js/dms.js"></script>
</head>

<body>
<a name="top"></a>
<div class="container">
<div class="centerizer">

