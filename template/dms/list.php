<?php
if ($_GET['act'] == 'add_item' || $_GET['act'] == 'update_item') {
	$this->showEditor();
} elseif ($_GET['act'] == 'gallery') {
	$this->imageUploader();
} else {
	$attr = array(
		'obj' 			=> true,
		'where' 		=> 'parent="'.intval($_GET['parent']).'" AND page="list"',
		'where_add'		=> ' AND page="list"',
		'recursive' 	=> true,
		'level' 		=> 0,
		'order'			=> 'jrk DESC',
	);

	$list = $this->showList($attr, 'mainlist');
	$this->outPut($list);
}
