
<?php
if (file_exists($_SERVER['DOCUMENT_ROOT'].'class/ftp.php'))
	require_once $_SERVER['DOCUMENT_ROOT'].'class/ftp.php';
$ftp = new ftp();

$ftp->renamer();
?>

<ul class="ftp_menu">
  <li><a href="?page=ftp"><?php echo $this->trans('dms_menu_ftp_upload')?></a></li>
  <li><a href="?page=ftp&act=access"><?php echo $this->trans('dms_menu_ftp_access')?></a></li>
  <li><a href="?page=ftp&act=schedule"><?php echo $this->trans('dms_menu_ftp_schedule')?></a></li>
  <li><a href="?page=ftp&act=users"><?php echo $this->trans('dms_menu_ftp_users')?></a></li>
</ul>
<?php if ($_GET['act'] == 'access') { ?>
<?php   $ftp->ftp_access(); ?>
<?php } elseif ($_GET['act'] == 'schedule') { ?>
<?php   $ftp->ftp_schedule(); ?>
<?php } elseif ($_GET['act'] == 'users') { ?>
<?php
        $ftp->ftp_userDetails();
        $ftp->ftp_userList();
?>
<?php } else { ?>

<iframe src="http://<?php echo $_SERVER['HTTP_HOST']?>/file/index.php?u=<?php echo $_SESSION['auth_username']?>&p=<?php echo $_SESSION['auth_password']?>" id="ftp" height="700"></iframe>
<?php } ?>
