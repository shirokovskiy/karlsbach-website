<?php
header("Content-type: text/css");
$url = 'http://'.$_SERVER['SERVER_NAME'];
$dir = $url.str_replace('css/font.php', '', $_SERVER['REQUEST_URI']);
/*
	src: url('<?php echo $dir?>font/Ubuntu-Regular.eot'), local('?'), url('<?php echo $dir?>font/Ubuntu-Regular.woff') format('woff'), url('<?php echo $dir?>font/Ubuntu-Regular.ttf') format('truetype'), url('<?php echo $dir?>font/ubunturegular.svg') format('svg');
*/
?>
@font-face {
	font-family: 'Ubuntu';
	src: url('<?php echo $dir?>font/Ubuntu-Regular.ttf') format('truetype');
	font-weight: normal;
	font-style: normal;
}
@font-face {
	font-family: 'UbuntuLight';
	src: url('<?php echo $dir?>font/Ubuntu-Light.ttf') format('truetype');
	font-weight: normal;
	font-style: normal;
}
@font-face {
	font-family: 'UbuntuMediun';
	src: url('<?php echo $dir?>font/Ubuntu-Medium.ttf') format('truetype');
	font-weight: normal;
	font-style: normal;
}
@font-face {
	font-family: 'OpenSansLight';
	src: url('<?php echo $dir?>font/OpenSans-Light.ttf') format('truetype');
	font-weight: normal;
	font-style: normal;
}
