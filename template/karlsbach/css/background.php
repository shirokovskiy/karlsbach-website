<?php
header("Content-type: text/css");
$url = 'http://'.$_SERVER['SERVER_NAME'];
$dir = $url.str_replace('css/background.php', '', $_SERVER['REQUEST_URI']);
?>
img, div { behavior: url('<?php echo $url?>/iepngfix.htc'); }

.lang { background-image: url('<?php echo $dir?>images/langbg.png'); }
.lang a { background-image: url('<?php echo $dir?>images/langarrow.png'); }
.search .button, .newsletterjoin .button, .page_catalog_gallery .button { background-image: url('<?php echo $dir?>images/searchbutton.gif'); }
.search_row .search .button { background-image: url('<?php echo $dir?>images/searchbutton2.jpg'); }
div.submenu, .product_submenu_container { background-image: url('<?php echo $dir?>images/submenubg.gif'); }
.hoversubmenu_container { background-image: url('<?php echo $dir?>images/hoversubmenubg.png'); }
.designer_contact { background-image: url('<?php echo $dir?>images/langarrow.png'); }
.services_gallery_coverup, .gallery_gallery_coverup_right, .gallery_gallery_coverup_left { background-image: url('<?php echo $dir?>images/langbg.png'); }

.jcarousel-skin-tango .jcarousel-next-horizontal { background-image: url('<?php echo $dir?>images/next.png'); }
.jcarousel-skin-tango-gallery .jcarousel-next-horizontal, .jcarousel-skin-tango-pdf .jcarousel-next-horizontal { background-image: url('<?php echo $dir?>images/next.png'); }
.jcarousel-skin-tango-gallery .jcarousel-prev-horizontal, .jcarousel-skin-tango-pdf .jcarousel-prev-horizontal { background-image: url('<?php echo $dir?>images/prev.png'); }

.storelist h2 { background-image: url('<?php echo $dir?>images/langmainarrow.png'); }
.storeico_showall { background-image: url('<?php echo $dir?>images/ico-showall.jpg'); }
.gallery_full_link:hover { background-image: url('<?php echo $dir?>images/galleryhover.png'); }
.gallery_full_link a { background-image: url('<?php echo $dir?>images/fullscreen.png'); }
.gallery_full_link a:hover { background-image: url('<?php echo $dir?>images/fullscreenhover.png'); }
.page_contact input { background-image: url('<?php echo $dir?>images/form1.gif'); }
.page_contact .input_street { background-image: url('<?php echo $dir?>images/form2.gif'); }
.contact_button { background-image: url('<?php echo $dir?>images/langarrow.png') !important; }
.contact_topic { background-image: url('<?php echo $dir?>images/form_topic.gif') !important; }

.page_catalog_icoset .enlarge { background-image: url('<?php echo $dir?>images/enlarge_all.png'); }
.page_catalog_icoset .download { background-image: url('<?php echo $dir?>images/download_all.png'); }
.page_catalog_icoset .print { background-image: url('<?php echo $dir?>images/print_all.png'); }

#fancybox-loading, .fancybox-close, .fancybox-prev span, .fancybox-next span { background-image: url('<?php echo $dir?>images/fancybox_sprite.png'); }
#fancybox-loading div { background-image: url('<?php echo $dir?>images/fancybox_loading.gif') center center no-repeat; }
.fancybox-nav { background-image: transparent url('<?php echo $dir?>images/blank.gif'); }

.jcarousel-control a { background-image: url('<?php echo $dir?>images/indexdot.gif'); }

.jcarousel-skin-tango-pdf li:hover .page_catalog_icoset, .page_gallery_icoset:hover, .expo_navi_bg_1, .expo_navi_bg_2, .expo_navi_bg_3, .products_navi { background-image: url('<?php echo $dir?>images/galleryhover.png'); }
.page_gallery_icoset .enlarge { background-image: url('<?php echo $dir?>images/enlarge_all.png'); }

.social_icons div { background-image: url('<?php echo $dir?>images/social_all.png'); }
