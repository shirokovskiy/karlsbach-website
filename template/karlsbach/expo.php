<?php
// expo_test fix 4 slider position

if (1) {
	$expos = $specific->getExpos();
    $allow = 0;
    $output = '';

	for ($i = 0; $i < count($expos); $i++) {
		$row = $expos[$i];

		$param = array(
			'single'	=> false,
			'table'		=> 'images',
			'order'		=> 'jrk',
			'where' 	=> 'parent="'.$row['id'].'"',
		);

		$expo_images[$i] = $this->getSQL($param);

		if (count($expo_images[$i]) > 0)
			$allow++;
	}

	if ($allow == 1)
		$output.= '<div class="expo_line_high">';

	$first = true;
	$count = 0;
	for ($i = 0; $i < count($expos); $i++) {
		$row = $expos[$i];
		$images = $expo_images[$i];
		$count++;
		if ($count > 3)
			$count = 1;

		if ($images) {
			while(count($images) < 7)
				$images = array_merge($images, $images);

			array_unshift($images, $images[count($images) - 1]);
			array_pop($images);
			array_unshift($images, $images[count($images) - 1]);
			array_pop($images);
		}

		if (is_array($images)) {
			$id = rand();

			$output.= '
<div class="expo_line'.($first ? ' expo_line_first' : '').' expo_line_'.$count.'" id="expo_liner_c_'.$id.'">
  <ul class="jcarousel-skin-tango-expo">';
//  <ul class="jcarousel-skin-tango-expo">
			foreach($images as $item) {
				$max = $item['path'];
				$dir = $item['path'].'475/';

				if ($item['filename'] != '' and file_exists($dir.$item['filename']))
					$output.= '
    <li><a rel="gallery_'.$i.'" href="'.URL.'/'.$max.$item['filename'].'" title="" class="opengallery" style="background-image:url(\''.URL.'/'.$dir.$item['filename'].'\');"></a></li>';
//    <li><img src="'.URL.'/'.$dir.$item['filename'].'" width="356" height="275" alt="" /><div class="gallery_full_link"><a href="#"></a></div></li>';
			}

			$output.= '
  </ul>

  <div class="expo_navi_container">
<div class="expo_navi_bg_'.$count.'" id="expo_liner_'.$id.'">
    <div class="expo_navi_'.$count.'">
      <a href="#" class="prev"><img src="'.DIR.'images/prev-small.png" alt="prev" /></a>
      '.stripslashes($row[$this->lang]).'
      <a href="#" class="next"><img src="'.DIR.'images/next-small.png" alt="next" /></a>
    </div>
</div>
  </div>

</div>

  <script type="text/javascript">
function isOdd(num) { return num % 2;}

function expo_line_initCallback_'.$id.'(carousel) {
    jQuery("#expo_liner_'.$id.' .next").bind("click", function() { carousel.next(); return false; });
    jQuery("#expo_liner_'.$id.' .prev").bind("click", function() { carousel.prev(); return false; });
};
//expo page gallery carousel 1
$(function() {
	$("#expo_liner_c_'.$id.' ul").jcarousel({
		wrap: "circular",
		scroll: 1,
		initCallback: expo_line_initCallback_'.$id.',
		buttonNextHTML: null,
		buttonPrevHTML: null
	});

var fix = 356;
var w = $("html").width();
var count = Math.ceil(w / fix) + 1;
if (!isOdd(count))
	count = count + 1;
var nw = count * fix;
nw = fix * count;

var left = -Math.floor((nw - w) / 2);
left = left + 44;

	$(".jcarousel-container-horizontal").css("left", 0);
	$(".jcarousel-container-horizontal").css("width", nw);
	$(".jcarousel-clip-horizontal").css("width", nw);

	$(".jcarousel-skin-tango-expo").css("left", left);
	$(".jcarousel-skin-tango-expo").css("width", nw);
	$(".jcarousel-skin-tango-expo").css("margin", "0 auto");

});
  </script>
';
		}

		$first = false;
	}

    if (count($expos) < 2)
        $output.= '</div>';

	$this->outPut($output);
} else {
    $specific->expo_test();
}
