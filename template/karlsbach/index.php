    <script type="text/javascript" src="<?php echo URL?>/js/jquery.nivo.slider.js"></script>
    <script type="text/javascript">
    $(window).load(function() {
        $('#slider').nivoSlider({
    	    effect: <?php echo "'".$this->configuration['index_page_effect']."'"?>, // Specify sets like: 'fold,fade,sliceDown'
	        slices: 15, // For slice animations
    	    boxCols: 8, // For box animations
	        boxRows: 4, // For box animations
    	    animSpeed: 500, // Slide transition speed
	        pauseTime: <?php echo ($this->configuration['index_page_animation_delay'] * 1000)?>, // How long each slide will show
    	    startSlide: 0, // Set starting Slide (0 index)
	        directionNav: false, // Next & Prev navigation
    	    pauseOnHover: true, // Stop animation while hovering
    	    prevText: '', // Prev directionNav text
	        nextText: '', // Next directionNav text
    	    randomStart: false, // Start on a random slide
	    });
    });
    </script>
<?php
$specific->indexanimation();
