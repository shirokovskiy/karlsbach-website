<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

    <link rel="stylesheet" type="text/css" href="<?php echo DIR?>css/styles.css" media="screen" />
    <link rel="shortcut icon" type="image/x-icon" href="<?php echo URL?>/favicon.ico" />
<?php //TODO>page info?>
<?php echo $this->page_info();?>

<!--[if IE]>
    <link rel="stylesheet" type="text/css" href="<?php echo DIR?>css/styles_ie.css" media="screen" /><![endif]-->
<!--[if lt IE 8]>
    <link rel="stylesheet" type="text/css" href="<?php echo DIR?>css/styles_ie7.css" media="screen" /><![endif]-->
<!--[if lt IE 7]>
    <link rel="stylesheet" type="text/css" href="<?php echo DIR?>css/styles_ie6.css" media="screen" /><![endif]-->
    <link rel="stylesheet" type="text/css" href="<?php echo DIR?>css/jquery.fancybox.css?v=2.0.6" media="screen" />
    <link rel="stylesheet" type="text/css" href="<?php echo DIR?>css/jquery.jcarousel.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="<?php echo DIR?>css/background.php" media="screen" />
    <link rel="stylesheet" type="text/css" href="<?php echo DIR?>css/font.php" media="screen" />

    <link rel="stylesheet" href="<?php echo DIR?>css/default/default.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="<?php echo DIR?>css/nivo-slider.css" type="text/css" media="screen" />

    <script type="text/javascript" src="<?php echo URL?>/js/common.js"></script>
    <script type="text/javascript" src="<?php echo URL?>/js/browser.detect.js"></script>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.min.js"></script>
    <script type="text/javascript" src="<?php echo URL?>/js/jquery.cycle.min.js"></script>
    <script type="text/javascript" src="<?php echo URL?>/js/jquery.jcarousel.min.js"></script>
    <script type="text/javascript" src="<?php echo URL?>/js/jquery.mousewheel-3.0.6.pack.js"></script>
    <script type="text/javascript" src="<?php echo URL?>/js/jquery.fancybox.pack.js?v=2.0.6"></script>
    <script type="text/javascript" src="<?php echo URL?>/js/anim.js"></script>
    <script type="text/javascript" src="<?php echo URL?>/js/screen_css.js"></script>

    <script type="text/javascript" src="<?php echo URL?>/js/lazyload.js"></script>

    <script src="http://maps.google.com/maps/api/js?sensor=false" type="text/javascript" ></script>
    <script type="text/javascript" src="<?php echo URL?>/js/jquery.ui.map.js"></script>
<?php
//if ($_SERVER['REMOTE_ADDR'] == '90.191.105.124') {
if (isset($_GET['test']) && $_GET['test'] == 'debug') {
	echo '<link rel="stylesheet" href="'.DIR.'css/styles_debug.css" type="text/css" media="screen" />';
}
?>

<script type="text/javascript">
$(window).load(function() {
	$(".loader").hide();
});
</script>

<script type="text/javascript">
//footer whitebox
function openWhiteBox(id, type) {
	add = '';
	if (type == 'news')
		add = '&type=news';
	else if (type == 'bonus')
		add = '&type=bonus';

	$('#whitebox').show();
	$('#whiteboxcontent').load("<?php echo URL?>/whitebox.php?id="+id+"&lang=<?php echo $this->lang?>"+add);
}
function closewhitebox() {
	$('#whitebox').hide();
}

function products_line_initCallback(carousel) {
    jQuery('#products_line_next').bind('click', function() { carousel.next(); return false; });
    jQuery('#products_line_prev').bind('click', function() { carousel.prev(); return false; });
};

$(function() {
    //show language dropdown
	$('.opengallery').fancybox();

	$('.langcontainer').mouseover(function() {
		$('.lang').show();
	});
    //hide language dropdown
	$('.langcontainer').mouseout(function() {
		$('.lang').hide();
	});

    //clear search input
	$('.search .keyword').click(function() {
		if ($('.search .keyword').val() == "<?php echo $this->trans('frontend_search')?>")
			$('.search .keyword').val('');
	})

    //clear newsletter email input
	$('.footercontainer .email').click(function() {
		if ($('.footercontainer .email').val() == "<?php echo $this->trans('frontend_newsletter_email')?>")
			$('.footercontainer .email').val('');
	})

    //submenu hover actions
    <?php echo $specific->hoversubmenu_js()?>

    //service page gallery carousel
	$('.page_services_gallery ul').jcarousel({
		wrap: 'circular',
		scroll: 1
    });

    //store selector
	$('.store_select_country').mouseover(function() {
		$('.store_select_country_list').show();
	});
	$('.store_select_country').mouseout(function() {
		$('.store_select_country_list').hide();
	});
	$('.store_select_town').mouseover(function() {
		$('.store_select_town_list').show();
	});
	$('.store_select_town').mouseout(function() {
		$('.store_select_town_list').hide();
	});

    //gallery page gallery carousel
	$('.page_gallery_gallery ul').jcarousel({
		wrap: 'circular',
		scroll: 1,
    });

    //whotebox closer
	$('.overlay').click(function() {
		$('#whitebox').hide();
	});

    //error page redirect
	$('#erroroverlay').click(function() {
		window.location.replace("<?php echo URL?>");
	});
	$('.error img').click(function() {
		window.location.replace("<?php echo URL?>");
	});
	$('.overlay_message').click(function() {
		window.location.replace($('#message_redirect').val());
	});
	$('.message_close img').click(function() {
		window.location.replace($('#message_redirect').val());
	});


    //products gallery
	$('.products_line ul').jcarousel({
		wrap: 'circular',
		scroll: 1,
		initCallback: products_line_initCallback,
		buttonNextHTML: null,
		buttonPrevHTML: null
    });

/*
    //index page gallery carousel
	$('.index_animations ul').jcarousel({
		scroll: 1,
		auto: <?php echo $this->configuration['index_page_animation_delay']?>,
		wrap: 'circular',
        buttonNextHTML: null,
        buttonPrevHTML: null,
		initCallback: mycarousel_initCallback,
		itemVisibleInCallback: {
			onAfterAnimation: function(c, o, i, s) {
				var size = c.options.size;
				i = (((i - 1) % size) + size) % size;
				$('.jcarousel-control a').removeClass('selected');
				$('.jcarousel-control a:eq('+i+')').addClass('selected');
			}
		}
	});
*/
	function mycarousel_initCallback(carousel) {
    	jQuery('.jcarousel-control a').bind('click', function() {
	        carousel.scroll(jQuery.jcarousel.intval(jQuery(this).text()));
    	    return false;
	    });

    	jQuery('.jcarousel-scroll select').bind('change', function() {
	        carousel.options.scroll = jQuery.jcarousel.intval(this.options[this.selectedIndex].value);
    	    return false;
	    });

	    jQuery('#mycarousel-next').bind('click', function() {
    	    carousel.next();
	        return false;
    	    carousel.startAuto(0);
	    });

	    jQuery('#mycarousel-prev').bind('click', function() {
    	    carousel.prev();
	        return false;
    	    carousel.startAuto(0);
	    });

    // Pause autoscrolling if the user moves with the cursor over the clip.
	    carousel.clip.hover(function() {
    	    carousel.stopAuto();
	    }, function() {
    	    carousel.startAuto();
	    });
	};

	$(".lazy").lazyload({
		effect       : "fadeIn"
	});

    //show language dropdown
	$('.contact_topic').mouseover(function() {
		$('.contact_topic ul').show();
	});
    //hide language dropdown
	$('.contact_topic').mouseout(function() {
		$('.contact_topic ul').hide();
	});

	$('.contact_topic li').click(function() {
		var selected = $(this).html();
		$('#contact_topic_display').html(selected);
		$('#topic').val(selected);
		$('.contact_topic ul').hide();
	})

	$('#contact_form').submit(function() {
		if ($('#name').val() == "") {
			alert("<?php echo $this->trans('contact_error_1')?>");
			$('#name').focus();
			return false ;
		}

		if ($('#lname').val() == "") {
			alert("<?php echo $this->trans('contact_error_2')?>");
			$('#lname').focus();
			return false ;
		}

		if ($('#street').val() == "") {
			alert("<?php echo $this->trans('contact_error_3')?>");
			$('#street').focus();
			return false ;
		}

		if ($('#appartment').val() == "") {
			alert("<?php echo $this->trans('contact_error_4')?>");
			$('#appartment').focus();
			return false ;
		}

		if ($('#town').val() == "") {
			alert("<?php echo $this->trans('contact_error_5')?>");
			$('#town').focus();
			return false ;
		}

		if ($('#phone').val() == "") {
			alert("<?php echo $this->trans('contact_error_6')?>");
			$('#phone').focus();
			return false ;
		}

		if ($('#email').val() == "") {
		    alert("<?php echo $this->trans('contact_error_7')?>");
			$('#email').focus();
		    return false ;
		} else if ($('#email').val() != "") {
			re = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
			if (re.test($('#email').val()) == false){
				alert("<?php echo $this->trans('contact_error_8')?>");
				$('#email').focus();
				return false;
			}
		}

		if ($('#text').val() == "") {
			alert("<?php echo $this->trans('contact_error_9')?>");
			$('#text').focus();
			return false ;
		}

		return true ;
	})


    //newsletter email check
	$('.newsletterjoin').submit(function() {
		if ($('.newsletterjoin .email').val() == "") {
		    alert("<?php echo $this->trans('contact_error_7')?>");
			$('.newsletterjoin .email').focus();
		    return false ;
		} else if ($('#email').val() != "") {
			re = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
			if (re.test($('.newsletterjoin .email').val()) == false){
				alert("<?php echo $this->trans('contact_error_8')?>");
				$('.newsletterjoin .email').focus();
				return false;
			}
		}

		return true ;
	});

    //service page gallery carousel
	$('.page_catalog_gallery ul').jcarousel({
		wrap: 'circular',
		scroll: 1
    });
});
</script>
<?php echo $this->googleAnalytics()?>
</head>

<body><a name="top"></a>
<!-- Yandex.Metrika counter --><script type="text/javascript">(function (d, w, c) { (w[c] = w[c] || []).push(function() { try { w.yaCounter28764691 = new Ya.Metrika({id:28764691, webvisor:true, clickmap:true, trackLinks:true, accurateTrackBounce:true}); } catch(e) { } }); var n = d.getElementsByTagName("script")[0], s = d.createElement("script"), f = function () { n.parentNode.insertBefore(s, n); }; s.type = "text/javascript"; s.async = true; s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js"; if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); } })(document, window, "yandex_metrika_callbacks");</script><noscript><div><img src="//mc.yandex.ru/watch/28764691" style="position:absolute; left:-9999px;" alt="" /></div></noscript><!-- /Yandex.Metrika counter -->
<div class="loader"><img src="<?php echo DIR?>images/loading_16.png" alt="loader" /></div>
<div class="container">
    <div class="centerizer">

        <div class="headercontainer">
            <div class="header">
                <a href="<?php echo URL.'/'.$this->lang.'/'?>"><img src="<?php echo DIR?>images/logo.gif" alt="<?php echo $this->configuration['page_title']?>" class="logo" /></a>
                <div class="langcontainer"><?php echo $specific->language()?></div>
                <?php echo $this->showSearch()?>
            </div>
            <?php echo $this->outPut($specific->display_menu(array('style' => 'table')))?>
        </div>

        <div class="maincontainer page_<?php echo $this->page['template']?><?php echo $specific->catalog_bg_white()?>"<?php echo ($this->page['template'] == 'text' || $this->page['template'] == 'catalog' ? $specific->text_background() : '')?>>
            <div class="main">
