<?php
$start_pos = explode(',', $this->configuration['stores_page_start_pos']);
?>

<div id="map_canvas" class="map"></div>

<script type="text/javascript" src="https://www.google.com/jsapi"></script>

<script type="text/javascript">
$(function() {
	$('#map_canvas').gmap().bind('init', function() {
        console.log('map');
// This URL won't work on your localhost, so you need to change it
// see http://en.wikipedia.org/wiki/Same_origin_policy
		$.getJSON( '<?php echo URL?>/mapdata.php?lang=<?php echo $this->lang?>', function(data) {
            console.log(data);
			$.each( data.markers, function(i, marker) {
				$('#map_canvas').gmap('addMarker', {
					'position': new google.maps.LatLng(marker.latitude, marker.longitude)
				}).click(function() {
					$('#map_canvas').gmap('openInfoWindow', { 'content': marker.content }, this);
				});
			});
		})
        .fail(function(d, textStatus, error) {
                console.error("getJSON failed, status: " + textStatus + ", error: "+error)
        });
	});

	$('#map_canvas').gmap('option', 'center', new google.maps.LatLng(<?php echo $start_pos[0]?>, <?php echo $start_pos[1]?>));
	$('#map_canvas').gmap('option', 'zoom', <?php echo $start_pos[2]?>);

// Set the map to some default center
// Try to get the user's position
		$('#map_canvas').gmap('getCurrentPosition', function(status, position) {
		if ( status === 'OK' ) {
			$('#map_canvas').gmap({'center': new google.maps.LatLng(position.coords.latitude, position.coords.longitude)});
			$('#from').val(position.coords.latitude+ ','+position.coords.longitude);
		} else {
// do some error handling
		}
	});

// Get the google loaders client location
// If it fails, return some defult value
	function getLatLng() {
		if ( google.loader.ClientLocation != null )
			return new google.maps.LatLng(google.loader.ClientLocation.latitude, google.loader.ClientLocation.longitude);

		return new google.maps.LatLng(<?php echo $start_pos[0]?>, <?php echo $start_pos[1]?>);
	}

});

function swapView(lat, lon, zoom) {
	if (lat && lon)
		$('#map_canvas').gmap('option', 'center', new google.maps.LatLng(lat, lon));

	if (zoom)
		$('#map_canvas').gmap('option', 'zoom', zoom);
}

function storeSwap(lat, lon, zoom, h2id, h2value, show, parent, type) {
	if (h2id && h2value)
		$(h2id).html(h2value);

	$('#store_country').hide();

	if (show)
		$(show).show();

	if (type == 'country') {
		$('.hide_town').hide();
		$('.store_town_selector_' + parent).show();
	}
	if (type == 'town') {
		$('.hide_store').hide();
		$('.store_store_selector_' + parent).show();
	}

	swapView(lat, lon, zoom);
}
</script>
<?php
$specific->showStoresList();
$specific->showStoresFooterButtons();
?>
