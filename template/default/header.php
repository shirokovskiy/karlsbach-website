
<!DOCTYPE html>
<html>

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <link rel="stylesheet" type="text/css" href="<?php echo DIR?>css/styles.css" media="screen" />
  <link rel="shortcut icon" type="image/x-icon" href="<?php echo DIR?>favicon.ico" />
<?php echo $this->page_info()?>

<!--[if IE]>
  <link rel="stylesheet" type="text/css" href="<?php echo DIR?>css/styles_ie.css" media="screen" /><![endif]-->
<!--[if lt IE 8]>
  <link rel="stylesheet" type="text/css" href="<?php echo DIR?>css/styles_ie7.css" media="screen" /><![endif]-->
<!--[if lt IE 7]>
  <link rel="stylesheet" type="text/css" href="<?php echo DIR?>css/styles_ie6.css" media="screen" /><![endif]-->
  <link rel="stylesheet" type="text/css" href="<?php echo DIR?>css/jquery.lightbox-0.5.css" media="screen" />
  <link rel="stylesheet" type="text/css" href="<?php echo DIR?>css/jquery.jcarousel.css" media="screen" />
  <link rel="stylesheet" type="text/css" href="<?php echo DIR?>css/background.php" media="screen" />

  <script type="text/javascript" src="<?php echo URL?>/js/common.js"></script>
  <script type="text/javascript" src="<?php echo URL?>/js/browser.detect.js"></script>
  <script type="text/javascript" src="http://code.jquery.com/jquery-latest.pack.js"></script>
  <script type="text/javascript" src="<?php echo URL?>/js/jquery.lightbox-0.5.pack.js"></script>
  <script type="text/javascript" src="<?php echo URL?>/js/jquery.cycle.min.js"></script>
  <script type="text/javascript" src="<?php echo URL?>/js/jquery.jcarousel.min.js"></script>
  <script type="text/javascript">
$(function() {
   $(".opengallery").lightBox({
	overlayBgColor: "#000",
	overlayOpacity: 0.6,
	imageLoading: "<?php echo DIR?>images/lightbox-loading.png",
	imageBtnClose: "<?php echo DIR?>images/lightbox-close.png",
	imageBtnPrev: "<?php echo DIR?>images/lightbox-prev.png",
	imageBtnNext: "<?php echo DIR?>images/lightbox-next.png",
	containerResizeSpeed: 350,
	txtImage: "",
	txtOf: "/"
   });
});
</script>

<script type="text/javascript">
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-29909707-1']);
  _gaq.push(['_addOrganic', 'Neti', 'query']);
  _gaq.push(['_addOrganic', 'www.ee', 'query']);
  _gaq.push(['_addOrganic', 'Delfi', 'q']);
  _gaq.push(['_trackPageview']);
  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
</head>

<body>
<a name="top"></a>
<div class="container">

<div class="header">
  <h1><a href="<?php echo URL?>">d e m o</a></h1>
</div>
<?php echo $this->outPut($this->display_menu(array('style' => 'ul')))?>

