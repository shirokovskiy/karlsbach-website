<?php
/**
 * Читай заметки в README.txt
 */
umask(0);
set_time_limit (1200);
header('Content-Type: text/html; charset=utf-8');

/**
 * Преобразовать PDF в картинки
 *
 * @param $pdf
 */
function generatePDFimages($pdf) {
    $pdfDir = dirname(__FILE__).'/upload/pdf/'.str_replace('.pdf', '', $pdf).'/';
    if (!is_dir($pdfDir))
    {
        mkdir($pdfDir, 0775, true);
        if (is_dir("/home/dmitry"))
            chgrp($pdfDir, "dmitry");
    }

	$im = new imagick('upload/pdf/'.$pdf);
    $pages = $im->getNumberImages();

	for ($i = 0; $i < $pages; $i++) {
        /**
         * Старая реализация
         */
//        echo '>'.$url.'<br /><img src="/pdf2image.php?file='.$pdf.'&img='.$i.'" style="height: 100px" />';
//        file_put_contents($pdfDir.$fileName.'.jpeg', file_get_contents($url));

        $im->clear();

        $im->setResolution(67, 67);
        $im->readImage('upload/pdf/'.$pdf.'['.$i.']');
        $im->setImageFormat('jpeg');
        $im->setImageCompressionQuality(67);

        $fileName = 'pdf-'.str_pad($i, 6, '0', STR_PAD_LEFT).'.jpg';
        $im->writeimage($pdfDir.$fileName);
        if (is_dir("/home/dmitry"))
            chgrp($pdfDir.$fileName, "dmitry");
        chmod($pdfDir.$fileName, 0664);
	}
}

function getFileList($dir) {
	$list = glob($dir . "*.pdf");

	return $list;
}



if (!empty($_GET['file'])) {
    $pdf = str_replace('upload/pdf/' , '', $_GET['file']);

    echo '<h4>Конвертируемый файл</h4>';
    echo '<p><a href="/map4.php">список файлов</a></p>';
    echo 'Start. ', date('H:i:s'), '<hr />';
    if ($pdf)
    {
        echo '<p>'. $pdf .'</p>';
        generatePDFimages($pdf);
        echo '<hr />The End. ', date('H:i:s');
    }
} else {
    echo '<h3>Файлы PDF для конвертации в картинки</h3>';
    $list = getFileList('upload/pdf/');
    for ($i = 0; $i < count($list); $i++) {
        echo '> <a href="map4.php?file='.$list[$i].'">'.$list[$i].'</a><br />';
    }
}


echo '<br />';
