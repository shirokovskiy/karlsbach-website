<?php
class Template extends DMS {

function Template() {
	$this->getTheme();
	$this->language();
}

function outPut($content, $site = false) {
    //clear the scene
	if ($site)
		ob_end_clean();

    //output itself
	echo $content;
}

function useTemplate($filename) {
	if (file_exists(TEMPLATE_DIR.$this->theme.'/'.$filename.'.php')) {
		include(TEMPLATE_DIR.$this->theme.'/'.$filename.'.php');
	} else {
		$output= 'ERROR on template <b>"'.$filename.'"</b><br />';
	}

	return $output;
}

function getTheme() {
    //db query here to get the theme
    $theme = 'dooker';

	if (!$theme) {
		$default = dms::defaultVal('default');
		$theme = $default['theme'];
	}
echo $theme;
	if ($theme && is_dir(TEMPLATE_DIR.$theme)) {
//use selected theme
		$this->theme = $theme;
	} else {
echo 'error, no theme';
//die with error
	}
}

function language() {
	$list = $this->getLanguageList();

	foreach ($list as $item)
		$langlist[] = $item['name'];

	if ($_GET['lang'] && in_array($_GET['lang'], $langlist))
		$this->lang = $_GET['lang'];

	if (!$this->lang)
		$this->lang = $list[0]['name'];
}

function getLanguageList() {
	$param = array(
		'single'	=> false,
		'table'		=> 'lang',
		'where'		=> 'active="yes"',
		'order'		=> 'jrk',
	);

	$langs = frontend::getSQL($param);

	return $langs;
}

function getLanguage() {
	return $this->lang;
}

function fetchKey($input) {
echo '>'.$this->lang;
	foreach ($input as $row)
		$result[$row['name']] = $row[$this->lang];

	return $result;
}

}
?>
