<?php
class ftp extends DMS {

    function __construct() {
    }

//manage ftp user details
    function ftp_userDetails($user_id = '') {
        global $dms;

        $type = 'add';
        if ($_GET['act'] == 'users' && intval($_GET['id'])) {
            $user = $dms->getSQL(array('table' => 'ftp_user', 'where' => 'id="'.($user_id ? $user_id : intval($_GET['id'])).'"'));
            $type = 'update';
        }

        $output= '
<form method="post" action="'.URL.'/'.DMS.'?page='.$_GET['page'].'&act='.$type.'_item'.($type == 'update' ? '&id='.($user_id ? $user_id : intval($_GET['id'])) : '').'" class="transform">
<label for="username">'.$dms->trans('dms_level_2').':</label>
<input type="text" name="username" id="username" class="inputfield" value="'.stripslashes($user['username']).'"'.($_GET['page'] == 'profile' ? ' disabled' : '').' />
<label for="password">'.$dms->trans('dms_password').':</label>
<input type="password" name="password" id="password" class="inputfield" value="" />
<label for="password2">'.$dms->trans('dms_password2').':</label>
<input type="password" name="password2" id="password2" class="inputfield" value="" />
<input type="hidden" name="act" value="lisa" />
<input type="submit" value="'.$dms->trans('button_'.($type == 'add' ? 'add' : 'change').'_plain').'" name="button" class="inputbutton" />
'.($type == 'update' && $_GET['page'] != 'profile' ? '<input type="button" value="'.$dms->trans('button_back').'" name="button" class="inputbutton nomargin" onclick="parent.location=\''.URL.'/dms/?page='.$_GET['page'].'&act='.$_GET['act'].'\';" />' : '').'
<input type="hidden" name="redirect" value="http://'.$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'].'" />
</form>';

        $dms->outPut($output);
    }


//show user list in DMS
    function ftp_userList() {
        global $dms;

        $lang = $dms->lang;
        $lang_list = $dms->returnLangList();

        if (!$_GET['cur_lang'])
            $_GET['cur_lang'] = $lang_list[0]['name'];

        $output= '
<div class="editorheader">
  <div class="editorheadertext">'.$dms->trans('dms_menu_users').'</div>
</div>';


        $sql_param = array(
            'single' => false,
            'table' => 'ftp_user'
        );
        $res = $dms->getSQL($sql_param);

        for ($i = 0; $i < count($res); $i++) {
            $row = $res[$i];

            $output.= '
<div class="langdatarow">
  <div class="datarow">
    <div class="text">'.stripslashes($row['username']).'</div>
    <div class="values">';

            $output.= '<a href="javascript: if (confirm(\''.$dms->trans('msg_delete').'\')) document.location=\''.URL.'/'.DMS.'?page='.$_GET['page'].'&act=delete_item&&id='.$row['id'].'&redirect=_self\'"><img src="'.DIR.'images/edit/'.$lang.'/delete.gif" title="'.$dms->trans('msg_delete').'" /></a>';
            $output.= '<a href="'.URL.'/dms/?page='.$_GET['page'].'&act=users&id='.$row['id'].'"><img src="'.DIR.'images/edit/'.$lang.'/edit.gif" title="'.$dms->trans('button_change').'" /></a>';

            $output.= '</div>
  </div>
</div>';
        }

        $dms->outPut($output);
    }


    function ftp_access() {
        global $dms, $db;

        if ($_GET['user_id']) {
            $param = array(
                'field'		=> 'access',
                'table'		=> 'ftp_access',
                'where'		=> 'user_id="'.intval($_GET['user_id']).'"',
            );
        }
        $access = $dms->getSQL($param);
        $access_list = array();
        $access_list = explode(';', $access['access']);

        $userlist = $this->getuserlist();

        $output = '
<form method="post" id="mainlist" class="ftp_access" action="'.URL.'/'.DMS.'?page='.$_GET['page'].'&act=save_user_access&cur_lang='.$_GET['cur_lang'].$dms->showP().($_GET['act'] == 'files' ? '&id='.$_GET['id'] : '').'" enctype="multipart/form-data">
<input type="hidden" name="new_lang" id="new_lang" value="'.$_GET['cur_lang'].'" />
<input type="hidden" name="redirect" value="http://'.$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'].'" />

<div class="ftp_text">'.$dms->trans('filelist_selector_1').'</div>

<select name="user" id="user">
  <option value="none">'.$dms->trans('filelist_selector_ftp').'</option>';

        for ($i = 0; $i < count($userlist); $i++) {
            $item = $userlist[$i];

            $output.= '
  <option value="'.$item['id'].'"'.($item['id'] == $_GET['user_id'] ? ' selected="selected"' : '').'>'.$item['username'].'</option>';
        }

        $output.= '
</select>

<div class="ftp_buttons">
<input type="button" value="'.$dms->trans('editor_selectall').'" id="switcher_on" />
<input type="button" value="'.$dms->trans('editor_unselectall').'" id="switcher_off" />
</div>

<ul class="ftp_access_list">';

        $dir = '../ftp/';
        $filelist = glob($dir . "*");

        for ($i = 0; $i < count($filelist); $i++) {
            $name = str_replace('../ftp/', '', $filelist[$i]);

            $output.= '
  <li><input type="checkbox" name="files[]" value="'.$name.'"'.(in_array(stripslashes($name), $access_list) ? ' checked="checked"' : '').' />'.stripslashes($name).'</li>';
        }

        $output.= '
</ul>

<div><input type="button" id="submit_1" value="'.$dms->trans('button_save').'" class="inputbutton" /></div>

</form>

<script>
$("#switcher_on").click(function() {
	$(this).hide();
	$("#switcher_off").show();
	$(".ftp_access_list INPUT[type=\'checkbox\']").attr("checked", true);
});
$("#switcher_off").click(function() {
	$(this).hide();
	$("#switcher_on").show();
	$(".ftp_access_list INPUT[type=\'checkbox\']").attr("checked", false);
});
$("#user").change(function() {
	window.location.href = "'.URL.'/dms/?page=ftp&act=access&user_id=" + $(this).val();
});
$("#submit_1").click(function() {
	var check = $("#user").val();

	if (isNaN(parseFloat(check))) {
	} else {
		currentForm = $(this).closest("form");
		currentForm.submit();
	}
});
</script>
';

        if (1) {

            $output.= '
<form method="post" id="mainlist" class="ftp_access ftp_access_2" action="'.URL.'/'.DMS.'?page='.$_GET['page'].'&act=save_file_access&cur_lang='.$_GET['cur_lang'].$dms->showP().($_GET['act'] == 'files' ? '&id='.$_GET['id'] : '').'" enctype="multipart/form-data">
<input type="hidden" name="new_lang" id="new_lang" value="'.$_GET['cur_lang'].'" />
<input type="hidden" name="redirect" value="http://'.$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'].'" />

<div class="ftp_text">'.$dms->trans('filelist_selector_2').'</div>

<div class="ftp_buttons ftp_buttons2">
<input type="button" value="'.$dms->trans('editor_selectall').'" id="switcher_on2" />
<input type="button" value="'.$dms->trans('editor_unselectall').'" id="switcher_off2" />
</div>

<ul class="ftp_file_list">';

            $access_list = array();

            $sql = 'SELECT user.*, access.access
			FROM '.SQL_PREFIX.'ftp_user as user
			LEFT JOIN '.SQL_PREFIX.'ftp_access as access
				ON access.user_id=user.id';
            $res = $db->query($sql);
            $n = $res->getRowCount();

            for ($i = 0; $i < $n; $i++) {
                $user = $res->getRow();

                $list[] = $user['access'];
            }

            $dir = '../ftp/';
            $filelist = glob($dir . "*");

            for ($i = 0; $i < count($filelist); $i++) {
                $name = str_replace('../ftp/', '', $filelist[$i]);

                $active = true;
                for ($j = 0; $j < $n; $j++) {
                    $temp = explode(';', $list[$j]);

                    if (!in_array($name, $temp))
                        $active = false;
                }

                $output.= '
  <li><input type="checkbox" name="files[]" value="'.$name.'"'.($active ? ' checked="checked"' : '').' />'.stripslashes($name).'</li>';
            }

            $output.= '
</ul>

<div><input type="button" id="submit_2" value="'.$dms->trans('button_save').'" class="inputbutton" /></div>

</form>

<script>
$("#switcher_on2").click(function() {
	$(this).hide();
	$("#switcher_off2").show();
	$(".ftp_file_list INPUT[type=\'checkbox\']").attr("checked", true);
});
$("#switcher_off2").click(function() {
	$(this).hide();
	$("#switcher_on2").show();
	$(".ftp_file_list INPUT[type=\'checkbox\']").attr("checked", false);
});
$("#submit_2").click(function() {
	currentForm = $(this).closest("form");
	currentForm.submit();
});
</script>
';

        }

        $dms->outPut($output);
    }


    function getuserlist() {
        global $dms;

        $param = array(
            'single'	=> false,
            'fields'	=> 'id, username',
            'table'		=> 'ftp_user'
        );
        $list = $dms->getSQL($param);

        return $list;
    }


    function renamer() {
        global $dms;

        $dir = '../ftp/';
        $dir2 = 'ftp/file/server/php/thumbnails/';
        $filelist = glob($dir . "*");

        for ($i = 0; $i < count($filelist); $i++) {
            $name = str_replace('../ftp/', '', $filelist[$i]);

            if ($name != str_replace(' ', '', $name)) {
                @rename($dir.$name, $dir.str_replace(' ', '', $name));
                @rename($dir2.$name, $dir2.str_replace(' ', '', $name));
            }
        }

        $dms->outPut('');
    }


    function ftp_schedule() {
        global $dms;

        $param = array(
            'single'	=> false,
            'fields'	=> 'filename, date',
            'table'		=> 'ftp_schedule',
        );
        $list = $dms->getSQL($param);

        for ($i = 0; $i < count($list); $i++) {
            $name = $list[$i]['filename'];
            $lpos = strrpos($name, '_');
            $name = substr($name, 0, $lpos).'.'.substr($name, $lpos + 1);

            $db[$name] = $list[$i]['date'];
        }

        $output= '
<form method="post" id="mainlist" action="'.URL.'/'.DMS.'?page='.$_GET['page'].'&act=save_schedule&cur_lang='.$_GET['cur_lang'].$dms->showP().($_GET['act'] == 'files' ? '&id='.$_GET['id'] : '').'" enctype="multipart/form-data">
<input type="hidden" name="new_lang" id="new_lang" value="'.$_GET['cur_lang'].'" />
<input type="hidden" name="redirect" value="http://'.$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'].'" />
<ul class="ftp_access_list">';

        $dir = '../ftp/';
        $filelist = glob($dir . "*");

        for ($i = 0; $i < count($filelist); $i++) {
            $name = str_replace('../ftp/', '', $filelist[$i]);

            $output.= '
  <li><input type="text" name="'.stripslashes($name).'" class="inputfieldshort datepicker" value="'.($db[stripslashes($name)] ? $db[stripslashes($name)] : date("m/d/Y",strtotime("+1 months"))).'" /> '.stripslashes($name).'</li>';
        }

        $output.= '
</ul>

<div><input type="submit" value="'.$dms->trans('button_save').'" class="inputbutton" /></div>

</form>';

        $dms->outPut($output);
    }
}
