<?php //dms4b1karlsbach_edition ?>
<?php
class DMS {
    public function __construct() {
        global $dmsdefault,$setup;

        $this->defaults = $dmsdefault;
        $this->langlist = $this->getLanguageList();

        //TODO>make default language selection better
        $this->defaultlang = $this->langlist[0]['name'];
        $this->checkLogin();
        $this->lang = $this->getLanguage();
        $this->translations = $this->getSetup();
        $this->configuration = $this->getSetup('setup');
        $this->theme = $this->getTheme();

        if (isset($this->user['level']) && $this->user['level'] == 3) {
            $this->defaults['allowed_templates'] = array('profile', 'ftp');
        }
        // usable directories
        $this->path = array(
            'banner'		=> 'upload/banner/'
            , 'header'		=> 'upload/header/'
            , 'images'		=> 'template/'.$this->theme.'/images/'
        );

        //editor fields
        $this->setup = $setup;

        //page specific class
        if (file_exists($_SERVER['DOCUMENT_ROOT'].'template/'.$this->theme.'/specific.php')) {
            require_once $_SERVER['DOCUMENT_ROOT'].'template/'.$this->theme.'/specific.php';
            $specific = new Specific();
        }

        if (isset($_SESSION['displaysomemessage'])) {
            $this->message = $_SESSION['displaysomemessage'];
        }
        if (isset($_SESSION['post'])) {
            $this->form_info = $_SESSION['post'];
        }
        //in case of form submit
        if (isset($_POST['act']) && substr($_POST['act'], 0, 5) == 'form_') {
            global $arr;
            if (method_exists($this, $_POST['act'])) {
                call_user_func_array(array($this, $_POST['act']), (array) $arr);
            }
        }

        $param = $this->defaults['headerimage'];
        $this->headerimages = $this->getSQL($param);

        $this->page = $this->getPage($this->getPageId());
        $this->page['show_title'] = $this->page[$this->lang];
        $this->parentpage = $this->getParentPage();

        //in case of portfolio (list type) reload page info
        if (isset($this->page['template']) && $this->page['template'] == 'portfolio' && intval($_GET['id'])) {
            $this->parentpage = $this->page;
            $this->page = $this->getPage(intval($_GET['id']), $this->page['template']);
            $this->page['show_title'] = $this->page[$this->lang.'_topic'];
        } else
            if (isset($this->page['parent']) && $this->page['parent'] != 'parent' && $this->page['template'] == 'news') {
                die("<hr /><p style='color:red'><b>" . __FILE__ . "</b>:<i>" . __LINE__ . "</i></p>");

                $this->parentpage = $this->getPage($this->page['parent']);
            }

        if (isset($this->page['coming_soon']) && $this->page['coming_soon']== 'yes'){
            $this->page['template'] = 'error';
        }
    }

    /*
    main function to display page
    */
    public function showPageContent() {
        $output = '';
        $args = $this->defaults['default'];

        if ($this->parentpage['template'] == 'news')
            $output.= '
<div class="newslist">';

        if ($this->page[$this->lang.'_topic'])
            $output.= '
<h1 class="topic'.($this->parentpage['template'] == 'news' ? ' newstopic' : '').'">'.stripslashes($this->page[$this->lang.'_topic']).'</h1>';

        if ($this->parentpage['template'] == 'news')
            $output.= '
    <div class="newsdate">'.date($args['blog_date_format'], $this->page['timestamp']).'</div>';
        /*
            if ($this->page['gallery'] == 'before')
                $output.= $this->showGallery($this->page['id']);
        */
        if ($this->page[$this->lang.'_lead'])
            $output.= '
<div class="'.($this->parentpage['template'] == 'news' ? 'newsbody' : 'content').'">'.stripslashes($this->page[$this->lang.'_lead']).'</div>';

        if ($this->page[$this->lang.'_text'])
            $output.= '
<div class="'.($this->parentpage['template'] == 'news' ? 'newsbody' : 'content').'">'.stripslashes($this->page[$this->lang.'_text']).'</div>';
        /*
            if ($this->page['gallery'] == 'after')
                $output.= $this->showGallery($this->page['id']);
        */
        if ($this->parentpage['template'] == 'news')
            $output.= '
</div>';

        $this->outPut($output);
    }


    //share on FB button
    function shareOnFB() {
        $output = '
<div class="shareonfb">
<a name="fb_share" type="icon_link" share_url="'.$this->SEOLink($this->page['id']).($this->parentpage['template'] == 'portfolio' ? $this->title_slug($this->page[$this->lang.'_topic']).'/' : '').'">'.$this->trans('frontend_share_on_FB').'</a>
<script src="http://static.ak.fbcdn.net/connect.php/js/FB.Share" type="text/javascript"></script>
</div>';

        $this->outPut($output);
    }


    /*
    show images as gallery
    */
    function showGallery($id = 0, $table = 'images') {
        $overimage = 'gallery';
        $output = '';
        if ($this->parentpage['template'] == 'portfolio') {
            $table = 'images_portfolio';
            $overimage = 'portfolio';
        }

        if ($id) {
            $param = array(
                'single'		=> false,
                'table'			=> $table,
                'where'			=> 'parent="'.$id.'"',
                'order'			=> 'jrk',
            );
            $images = $this->getSQL($param);

            if (count($images)) {
                $output.= '
<ul class="gallery'.($this->parentpage['template'] == 'portfolio' ? ' portfoliogallery' : ($this->parentpage['template'] == 'news' ? ' newsgallery' : '')).'">';

                for ($i = 0; $i < count($images); $i++) {
                    $row = $images[$i];
                    $max = $row['path'];
                    $dir = $row['path'].'tnb/';

                    if ($this->parentpage['template'] == 'portfolio') {
                        $dir = 'upload/portfolio/250/';
                        $max = 'upload/portfolio/800/';
                    }

                    if ($row['filename'] != '' and file_exists($dir.$row['filename'])) {
                        $output.= '
  <li class="itemimage"><a rel="gallery" href="'.URL.'/'.$max.$row['filename'].'" title="'.stripslashes($row[$this->lang]).'" class="opengallery" style="background-image:url(\''.URL.'/'.$dir.$row['filename'].'\');">'.($this->defaults[$overimage]['galleryimage'] ? '<img src="'.DIR.'images/'.$this->defaults[$overimage]['galleryimage'].'" title="" />' : '').'</a></li>';
                    }
                }

                $output.= '
</ul>';
            }
        }

        return $output;
    }

    /*
    main function to display news list
    */
    function showNewsList() {
        $args = $this->defaults['default'];
        $start = (intval($_GET['start']) - 1) * $args['blog_items_per_page'];
        if (!$start)
            $start = 0;
        if ($start < 0)
            $start = 0;

        $param = array(
            'fields'	=> 'COUNT(id)',
            'where'		=> 'hidden="no"
						AND parent="'.$this->page['id'].'"
						AND page="list"',
        );
        $total = $this->getSQL($param);

        $prev = $start - $args['blog_items_per_page'];
        if ($prev < 0)
            $prev = 0;
        $next = $start + $args['blog_items_per_page'];
        if ($next > $total[0] - 1)
            $next = $total[0] - 1 - $args['blog_items_per_page'];

        $param = array(
            'single'	=> false,
            'where'		=> 'hidden="no"
						AND parent="'.$this->page['id'].'"
						AND page="list"',
            'order'		=> 'jrk DESC',
            'limit'		=> ''.$start.', '.$args['blog_items_per_page'],
        );
        $news = $this->getSQL($param);
        $output = '';

        if (count($news)) {
            $output.= $this->pagination($total[0]);

            $output.= '
<ul class="newslist">';

            for ($i = 0; $i < count($news); $i++) {
                $row = $news[$i];

                $output.= '
  <li>
    <h1 class="newstopic"><a href="'.$this->SEOLink($row['id']).'">'.stripslashes($row[$this->lang.'_topic'] ? $row[$this->lang.'_topic'] : '&nbsp;').'</a></h1>
    <div class="newsdate">'.date($args['blog_date_format'], $row['timestamp']).'</div>';

                if ($row['gallery'] == 'before')
                    $output.= $this->showGallery($row['id']);

                $output.= '
    <div class="newsbody">'.stripslashes($row[$this->lang.'_'.($args['blog_lead_show'] ? 'lead' : 'text')]).'</div>';

                if ($row['gallery'] == 'after')
                    $output.= $this->showGallery($row['id']);

                $output.= '
  </li>';
            }

            $output.= '
</ul>';

            $output.= $this->pagination($total[0]);
        }

        $this->outPut($output);
    }

    //display site & page header info
    //TODO>FB share meta doesnt work!
    function page_info() {
        $title = stripslashes($this->page['show_title']).($this->page['show_title'] && $this->configuration['page_title'] ? ' - ' : '').$this->configuration['page_title'];

        if ($this->checkForDMS()) {
            $title = $this->configuration['page_title'] . ' [DMS]';
        }
        $output = '<title>'.$title.'</title>';

        if (isset($this->page[$this->lang.'_meta_desc']) && $this->page[$this->lang.'_meta_desc']) {
            $output.= '<meta name="description" content="'.$this->page[$this->lang.'_meta_desc'].'" />';
        }

        if (isset($this->page[$this->lang.'_meta_keys'])) {
            $output.= '<meta name="keywords" content="'.$this->page[$this->lang.'_meta_keys'].'" />';
        }

        $output.= '<meta property="og:title" content="'.$title.'" />'
        .'<meta property="og:description" content="'.(isset($this->page[$this->lang.'_meta_desc'])?$this->page[$this->lang.'_meta_desc']:'').'" />'
        .'<meta property="og:image" content="'.DIR.'images/logo.gif" />'
        .'<meta property="og:url" content="'.(isset($this->page['id'])?$this->SEOLink($this->page['id']):'').($this->parentpage['template'] == 'portfolio' ? $this->title_slug($this->page[$this->lang.'_topic']).'/' : '').'" />';

        return $output;
    }

    /*
    default functions
    fetch & display language list
    */
    function language() {
        $args = $this->defaults['lang'];
        $list = $this->langlist;
        $output = '';

        if (count($list) > 1) {
            $output.= '
  <ul'.($args['mainclass'] ? ' class="'.$args['mainclass'].'"' : '').'>';

            for ($i = 0; $i < count($list); $i++) {
                $row = $list[$i];

                $url = stripslashes($row['display']);

                $class = ' class="';
                if ($i == 0)
                    $class.= ' first';
                if ($i == count($list) - 1)
                    $class.= ' last';
                if ($this->lang == $row['name'])
                    $class.= ' active';
                $class.= '"';

                $output.= '
    <li'.$class.'><a href="'.$this->SEOlink($this->page['id'], $row['name']).'"'.$class.'>';

                if ($args['display'] == 'text') {
                    $output.= ucwords(stripslashes($row['display']));
                } else {
                    $output.= '<img src="'.DIR.'images/lang/'.$row['name'].($this->lang == $url ? $args['hover_image'] : '').$args['filetype'].'" title="'.stripslashes($row['display']).'"';

                    if ($args['hover'] && $this->lang != $url)
                        $output.= ' onmouseover="this.src=\''.DIR.'images/lang/'.$row['name'].$args['hover_image'].$args['filetype'].'\'; " onmouseout="this.src=\''.DIR.'images/lang/'.$row['name'].$args['filetype'].'\'; "';

                    $output.= ' />';
                }

                $output.= '</a></li>';
            }

            $output.= '
  </ul>';
        }

        $this->outPut($output);
    }

    /*
    banner
    */
    //TODO>check if works!!!
    function banner() {
        global $uploaddir;
        $param = array(
            'single'	=> false,
            'table'		=> 'banner',
            'order'		=> 'jrk',
        );
        $banners = $this->getSQL($param);
        $output = '';

        if (count($banners)) {
            $output.= '<div class="banner">';

            for ($i = 0; $i < count($banners); $i++) {
                $row = $banners[$i];

                if (file_exists($this->path['banner'].$row['filename'])) {
                    $temp = explode('.', $uploaddir.$row['filename']);
                    $filetype = $temp[count($temp) - 1];

                    if ($filetype == 'swf')
                        $output.= '
    <div class="banneritem'.($i == 0 ? ' first' : '').'"><div>
      <object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=7,0,19,0" width="'.$row['x'].'" height="'.$row['y'].'">
        <param name="movie" value="'.URL.'/'.$this->path['banner'].$row['filename'].'" />
        <param name="quality" value="high" />
        <param name="wmode" value="transparent">
        <param name="flashvars" value="clickTARGET=_blank&clickTAG='.$row['url'].'">
        <embed src="'.URL.'/'.$this->path['banner'].$row['filename'].'" wmode="transparent" quality="high" pluginspage="http://www.macromedia.com/go/getflashplayer" type="application/x-shockwave-flash" width="'.$row['x'].'" height="'.$row['y'].'"></embed>
      </object><script type="text/javascript" src="'.URL.'/ieupdate.js"></script></div></div>';
                    else
                        $output.= '
    <div class="banneritem'.($i == 0 ? ' first' : '').'"><div>'.($row['url'] && $row['url'] != 'http://' ? '<a href="'.$row['url'].'" target="_blank">' : '').'<img src="'.URL.'/'.$this->path['banner'].$row['filename'].'" title="'.$row['url'].'" />'.($row['url'] && $row['url'] != 'http://' ? '</a>' : '').'</div></div>';
                }
            }

            $output.= '
  </div>';
        }

        $this->outPut($output);
    }

    /*
    header images
    */
    //TODO>loose the $global
    function headerimages($limit = 'all', $type = '') {
        global $global;
        $lang = $this->lang;
        $param = $this->defaults['headerimage'];
        $output = '';

        if ($type == 'local') {
            if (file_exists($this->path['header'].$global['page']['filename']) && $global['page']['filename'])
                $output.= '
      <img src="'.URL.'/'.$this->path['header'].$global['page']['filename'].'" title="'.$this->page[$lang].'" />';
        } else {
            $array = $this->headerimages;

            for ($i = 0; $i < count($array); $i++) {
                $row = $array[$i];

                if (file_exists($this->path['header'].$row['filename'])) {
                    $size = getimagesize($this->path['header'].$row['filename']);
                    $temp = explode('.', $this->path['header'].$row['filename']);
                    $filetype = $temp[count($temp) - 1];

                    if ($filetype == 'swf')
                        $output.= '<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=7,0,19,0" width="905" height="228">
        <param name="movie" value="'.URL.'/'.$this->path['header'].$row['filename'].'" />
        <param name="quality" value="high" />
        <param name="wmode" value="transparent">
        <embed src="'.URL.'/'.$this->path['header'].$row['filename'].'" wmode="transparent" quality="high" pluginspage="http://www.macromedia.com/go/getflashplayer" type="application/x-shockwave-flash" width="905" height="228"></embed>
      </object><script type="text/javascript" src="'.URL.'ieupdate.js"></script>';
                    else
                        $output.= '
      <img src="'.URL.'/'.$this->path['header'].$row['filename'].'" title="'.$this->page[$lang].'"'.($i == 0 ? ' class="show"' : '').' />';
//TODO>headerbanners, allow link
                    /*
                                        $output.= '
                          <a href="'.URL.'/"><img src="'.URL.'/'.$this->path['header'].$row['filename'].'" title="'.$this->page['page'][$this->lang].'" /></a>';
                    */
                }
            }
        }

        if (!$output)
            $output.= '<img src="'.DIR.'images/'.$param['default'].'" title="'.$global['page'][$lang].'" />';

        $this->outPut($output);
    }

    /*
    menu display function
    must include: table display for horiz. menu
                  ul-li display for vert & horiz. menu
    */
    //TODO>parenttree doesnt give the parent!
    public function display_menu($attr) {
        $output = '';
        $args = $this->parameters($attr, 'menu');

        //in case of portfolio we use parent page as menu marker
        //TODO>we must select location better than this hardcoded
        if ($this->parentpage['parent'] && $args['location'] == 'main' && $this->parentpage['parent'] != 'parent' && $args['parent'] != 'main')
            $args['sqlparent'] = $this->parentpage['parent'];

        $param = array(
            'single'	=> false,
            'where'		=> 'parent="'.$args['sqlparent'].'"
						AND '.$this->lang.'!=""
						AND hidden="no"
						AND page="site"
						AND template!="footer"
						AND menu="'.$args['location'].'"',
            'order'		=> 'jrk',
        );
        $array = $this->getSQL($param);
        $n = count($array);

        if (count($array)) {
            if ($args['style'] == 'table')
                $output.= '
<table class="'.$args['parent'].'menu" cellpadding="0" cellspacing="0">
<tr>';
            else
                $output.= '
<ul class="'.$args['parent'].'menu">';

            for ($i = 0; $i < $n; $i++) {
                $row = $array[$i];

                $parenttree = explode('/', $this->getParentTree($this->page['id'], $this->lang, '', true));
                $class='';

                if ($this->page['id'] == $row['id'] || in_array($row['id'], $parenttree))
                    $class = 'active';

                if ($n == 1) {
                    $class.= ($class ? ' ' : '').'single';
                } else {
                    if ($i == 0)
                        $class.= ($class ? ' ' : '').'first';
                    if ($i == $n - 1)
                        $class.= ($class ? ' ' : '').'last';
                }

                if ($class)
                    $class = ' class="'.$class.'"';

//($args == 'main' ? '<img src="'.URL.'/images/mainmenuleft.png" title="" /><a style="width: '.(intval(986 / $n) - 8).'px;"
//link begin
                if ($args['style'] == 'table') {
                    $output .= '<td' . $class . '>';
                } else {
                    $output .= '<li' . $class . '>';
                }
//link
                if ($row['template'] == 'whitebox') {
                    $output .= '<a' . $class . ' href="#" onclick="openWhiteBox(' . $row['id'] . ')">' . $args['add_before'] . stripslashes($row[$this->lang]) . $args['add_after'] . '</a>';
                }else {
                    $output .= '<a' . $class . ' href="' . ($row[$this->lang . '_redirect_url'] && $row[$this->lang . '_redirect_url'] != 'http://' ? $row[$this->lang . '_redirect_url'] : $this->SEOlink($row['id'])) . '">' . $args['add_before'] . stripslashes($row[$this->lang]) . $args['add_after'] . '</a>';
                }

//link end
                if ($args['style'] == 'table') {
                    $output .= '</td>';
                } else {
                    $output .= '</li>';
                }

//TODO>limited to 1 sublevel now
                if ($args['sqlparent'] == 'parent' && $args['parent'] == 'sub' && $_GET['page'] == $row['id']) {
                    $output.= '<li>'.$this->display_menu(array('parent' => 'sub', 'style' => 'ul', 'sqlparent' => $row['id'])).'</li>';
                }
            }

            if ($args['style'] == 'table')
                $output.= '
</tr>
</table>';
            else
                $output.= '
</ul>';
        }

        return $output;
    }

    //defaults value returner
    function defaults($target) {
        $temp = $this->defaults;
        $temp2 = $temp[$target];

        return $temp2;
    }

    //output translation text for selected language from translations
    function trans($code) {
        $value = stripslashes($this->translations[$code]);
        if (!$value)
            $value = '['.$code.']';

        return $value;
    }

    //get default values from default.php
    //multiple accessing?
    //2011.12.09 - deprecated?
    function defaultVal($array, $path = 'defaults.php') {
        include($path);

        return $$array;
    }

    function returnLangList() {
        return $this->langlist;
    }


    //dms language list
    function langlist() {
        global $db;

        $lang = $this->lang;
        $lang_list = $this->returnLangList();

        if (!$_GET['cur_lang'])
            $_GET['cur_lang'] = $lang_list[0]['name'];

        $output= '
<div class="editorheader">
  <div class="editorheadertext">'.$this->trans('dms_menu_languages').'</div>
</div>';

        $output.= '
<form method="post" id="mainlist" action="'.URL.'/'.DMS.'?page='.$_GET['page'].'&act=update_list" enctype="multipart/form-data">
<div>
  <input type="submit" value="'.$this->trans('button_savelist').'" class="inputbutton" />
  <input type="submit" value="'.$this->trans('button_change_plain').'" class="inputbutton" onclick="$(\'#bypass\').val(1)" />
</div>
<ul class="mainlist orderlist">';

        $langlist = $this->getSQL(array('single' => false, 'table' => 'lang', 'order' => 'jrk'));

        for ($i = 0; $i < count($langlist); $i++) {
            $row = $langlist[$i];

            $output.= '
<div class="langdatarow">
  <div class="datarow">
    <div class="text" style="width: 100px; ">'.stripslashes($row['name']).'</div>
    <div class="values" style="float: left; width: 670px; ">
      <div class="userdetails">
        <input type="text" class="inputfieldlong" name="input[]" value="'.stripslashes($row['display']).'" />
        <input type="hidden" class="inputfieldlong" name="marker[]" value="'.$row['id'].'" />
      </div>
      <div class="adminbuttons">
        <input type="hidden" name="item[]" value="'.$row['id'].'" />';

//		$output.= '<a href="javascript: if (confirm(\''.$this->trans('msg_delete').'\')) document.location=\''.URL.'/'.DMS.'?page='.$_GET['page'].'&act=delete_item&&id='.$row['id'].'&redirect=_self\'"><img src="'.DIR.'images/edit/'.$lang.'/delete.gif" title="'.$this->trans('msg_delete').'" /></a>';
//		$output.= '<a href="'.URL.'/dms/?page='.$_GET['page'].'&act=update_item&id='.$row['id'].'"><img src="'.DIR.'images/edit/'.$lang.'/edit.gif" title="'.$this->trans('button_change').'" /></a>';
            if  ($row['active'] == 'no')
                $output.= '<a href="'.URL.'/'.DMS.'?page='.$_GET['page'].'&act=toggle_lang&val=yes&id='.$row['id'].'&redirect=_self"><img src="'.DIR.'images/edit/'.$lang.'/1.gif" title="'.$this->trans('button_show').'" /></a>';
            else
                $output.= '<a href="'.URL.'/'.DMS.'?page='.$_GET['page'].'&act=toggle_lang&val=no&id='.$row['id'].'&redirect=_self"><img src="'.DIR.'images/edit/'.$lang.'/0.gif" title="'.$this->trans('button_hide').'" /></a>';

            $output.= '
      </div>
    </div>
  </div>
</div>';
        }

        $output.= '
</ul>
<div>
  <input type="submit" value="'.$this->trans('button_savelist').'" class="inputbutton" />
  <input type="submit" value="'.$this->trans('button_change_plain').'" class="inputbutton" onclick="$(\'#bypass\').val(1)" />
</div>
<input type="hidden" name="redirect" value="http://'.$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'].'" />
<input type="hidden" name="bypass" id="bypass" value="0" />
</form>';

        $this->outPut($output);
    }


    function parameters($attr, $array, $path = 'defaults.php') {
        $default = $this->defaults[$array];

        if (is_array($attr) && !empty($attr)) {
            $nu_param = array();

            foreach ($attr as $key => $val){
                $nu_param[$key] = $val;
            }
        }

        $param = array();
        foreach ($default as $key => $val) {
            $param[$key] = (isset($nu_param[$key]) ? $nu_param[$key] : $val);
        }
        return $param;
    }


    //TODO>remove echo debug
    function getSQL($attr, $debug = false) {
        global $db;

        $attr = dms::parameters($attr, 'sql');

        $sql = 'SELECT '.$attr['fields'].'
			FROM '.SQL_PREFIX.$attr['table'].'
			'.($attr['where'] ? 'WHERE '.$attr['where'] : '').'
			'.($attr['order'] ? 'ORDER BY '.$attr['order'] : '').'
			'.($attr['limit'] ? 'LIMIT '.$attr['limit'] : '');
        $res = $db->query($sql);

        if ($debug) {
            echo $sql . '<br />';
        }
        if ($attr['obj'] === true)
            return $res;
        else {
            if ($attr['single'] === true) {
                return $res->getRow(0);
            } else {
                $n = $res->getRowCount();
                $item = array();
                for ($i = 0; $i < $n; $i++) {
                    $item[$i] = $res->getRow(0);
                }
                return $item;
            }
        }
    }

    function outPut($content, $site = false) {
        //clear the scene
        if ($site) {
            ob_end_clean();
        }
        //output itself
        echo $content;
    }

    public function useTemplate($filename) {
        global $specific;
        if (file_exists(TEMPLATE_DIR.$this->theme.'/'.$filename.'.php')) {
            include(TEMPLATE_DIR.$this->theme.'/'.$filename.'.php');
        } else {
//		$this->outPut('<div class="main">ERROR, no template found!');
            $this->outPut('<div class="main">');
        }
    }

//check weather use index template or just first page
    function getPageId() {
        if (isset($_GET['page'])) {
            return intval($_GET['page']);
        } else {
            $res = $this->getSQL(array('fields' => $this->lang, 'table' => 'setup', 'where' => 'code="index_id"'));

            if (!$res[$this->lang])
                $res = $this->getSQL(array('fields' => 'id as '.$this->lang, 'where' => 'parent="parent" AND menu="main" AND hidden="no"', 'order' => 'jrk', 'limit' => '1'));

            return $res[$this->lang];
        }
    }

//create page
    public function site() {
        $this->useTemplate('header');
        if ($this->checkForDMS()) {
            if (isset($this->user['level']) && $this->user['level'] == 3) {
                $this->dms_mainmenu();
                $this->outPut('<div class="dmscontainer">');
                $this->dms_menu();
                $this->outPut('<div class="main">');
                $this->dms_content();
            } else {
                $this->useTemplate('index');
            }
        } else {
            $this->useTemplate($this->page['template']);
        }
        $this->useTemplate('footer');
    }

//return true if dms
    function checkForDMS() {
        if (isset($_GET['dms']) && $_GET['dms'] == 'true') {
            return true;
        }
        return false;
    }

    /**
     * Get web-site Theme
     *
     * @return string
     * @author Dimitry Shirokovskiy
     */
    function getTheme() {
        // TODO: db query here to get the theme
        $theme = $this->configuration['theme'];

        if (!$theme)
            $theme = $this->defaults['default']['theme'];

        if ($this->checkForDMS())
            $theme = 'dms';

        if ($theme && is_dir(TEMPLATE_DIR.$theme)) {
            //use selected theme
        } else {
            $theme = $this->defaults['default']['theme'];
        }

        $this->theme = $theme;

        //in case we have DMS
        if (isset($_GET['dms']) && $_GET['dms'] == 'true') {
            if (!is_dir(TEMPLATE_DIR.'dms'))
                $this->die2('error, <strong>dms</strong> template not found');

            $this->theme = 'dms';
        }

        return $this->theme;
    }

    function setlanguage() {
        $list = $this->getLanguageList();

        foreach ($list as $item)
            $langlist[] = $item['name'];

        if ($_GET['lang'] && in_array($_GET['lang'], $langlist))
            $this->lang = $_GET['lang'];

        if (!$this->lang)
            $this->lang = $list[0]['name'];
    }

    /*
    get available languages
    */
    function getLanguageList() {
        $param = array(
            'single'	=> false,
            'table'		=> 'lang',
            'order'		=> 'jrk',
        );
        if (!$this->checkForDMS())
            $param['where'] = 'active="yes"';

        $langs = $this->getSQL($param);

        return $langs;
    }

    /*
    gives language var to child class
    */
    function getLanguage() {
//	$this->getlanguage();
        //check lang list here?
        if (isset($_GET['lang']))
            $this->lang = $_GET['lang'];
        else
            $this->lang = $this->langlist[0]['name'];

        if ($this->checkForDMS()) {
            if (isset( $this->user['dms_lang'])) {
                $this->lang = $this->user['dms_lang'];
            }
        }


        if (!$this->lang)
            $this->lang = $this->defaultlang;

        return $this->lang;
    }

    /*
    returns theme
    */
    function showTheme() {
        return $this->theme;
    }


    function fetchKey($input) {
        if (is_array($input))
            foreach ($input as $row)
                $result[$row['name']] = $row[$this->lang];

        return $result;
    }

    /*
    SEOLink
    displays SOE friendly link
    */
    function SEOlink($input, $lang = '') {
        $attr = $this->defaults['seo'];

        if (!$lang)
            $lang = $this->lang;
//need some add values here? table selection?
//$table needed?
        if (SEO)
            return URL.'/'.($attr['allow_language'] ? ($this->lang == $this->defaultlang && !$lang ? '' : $lang.'/').($input ? $this->getParentTree($input, $lang) : '') : $this->title_slug($input).'/');
        else
            return '?page='.$input;
    }

    /*
    getParentTree
    gives back breadcrub to parent
    TODO: getParentTree seems to have multiple parameters on rest of the code

    * 2012.02.16 - WE NEED ADDITIONAL PARAMETERS for news for example

    */
    function getParentTree($id, $lang, $table = '', $showid = '') {
        $attr = $this->defaults['tree'];

        if ($showid)
            $attr['showid'] = $showid;


//need some add values here? table selection?

        $param = array(
            'table'		=> ($table ? $table : $attr['table']),
            'where'		=> 'id="'.$id.'"',
        );
        $row = dms::getSQL($param);

        if ($row['page'] == 'list')
            $attr['display_add'] = '_topic';

        $display = stripslashes($row[$lang.$attr['display_add']]);

        $tree = ($attr['showid'] ? $row['id'] : ($attr['show_real_name'] ? $display : $this->title_slug($display))).'/';

        if ($row['parent'] != 'parent' && $row['parent']) {
            $tree = dms::getParentTree($row['parent'], $lang, $table, $showid) . $tree;
        }
        if ($tree[0] == '/') {
            $tree = substr($tree, 1);
        }
        return $tree;
    }


    /*
    title_slug
    get the title proper for SEO link
    //2012.07.10 fixed cyrillic usage + added lower case estonian special chars
    */
    function title_slug($title) {
        $title = dms::cyrillicSEO($title);

        $title = str_replace('Ä', 'a', $title);
        $title = str_replace('Ö', 'o', $title);
        $title = str_replace('Ü', 'u', $title);
        $title = str_replace('Õ', 'o', $title);
        $title = str_replace('ä', 'a', $title);
        $title = str_replace('ö', 'o', $title);
        $title = str_replace('ü', 'u', $title);
        $title = str_replace('õ', 'o', $title);

        $a = array('/(à|á|â|ã|ä|å|æ)/','/(è|é|ê|ë)/','/(ì|í|î|ï)/','/(ð|ò|ó|ô|õ|ö|ø|œ)/','/(ù|ú|û|ü)/','/ç/','/þ/','/ñ/','/ß/','/(ý|ÿ)/','/(=|\+|\/|\\\|\.|\'|\_|\\n| |\(|\))/','/[^a-z0-9_ -]/s','/-{2,}/s');
        $b = array('a','e','i','o','u','c','d','n','ss','y','-','','-');

        return trim(preg_replace($a, $b, mb_strtolower($title)),'-');
    }

    function u8($win,$h,$t) {
        global $w8;

        $w8[chr($h).chr($t)] = $win;
    }

    function cyrillicSEO($text) {
        $this->u8("i",208,185); $this->u8("ch",209,134); $this->u8("u",209,131);
        $this->u8("k",208,186); $this->u8("je",208,181); $this->u8("n",208,189);
        $this->u8("g",208,179); $this->u8("sh",209,136); $this->u8("zh",209,137);
        $this->u8("z",208,183); $this->u8("h",209,133); $this->u8("",209,138);
        $this->u8("f",209,132); $this->u8("o",209,139); $this->u8("v",208,178);
        $this->u8("a",208,176); $this->u8("p",208,191); $this->u8("r",209,128);
        $this->u8("o",208,190); $this->u8("l",208,187); $this->u8("d",208,180);
        $this->u8("zh",208,182); $this->u8("e",209,141); $this->u8("ja",209,143);
        $this->u8("tsh",209,135); $this->u8("s",209,129); $this->u8("m",208,188);
        $this->u8("i",208,184); $this->u8("t",209,130); $this->u8("",209,140);
        $this->u8("",208,177); $this->u8("ju",209,142); $this->u8("I",208,153);
        $this->u8("CH",208,166); $this->u8("U",208,163); $this->u8("K",208,154);
        $this->u8("JE",208,149); $this->u8("N",208,157); $this->u8("G",208,147);
        $this->u8("SH",208,168); $this->u8("ZH",208,169); $this->u8("Z",208,151);
        $this->u8("X",208,165); $this->u8("",208,170); $this->u8("F",208,164);
        $this->u8("O",208,171); $this->u8("V",208,146); $this->u8("A",208,144);
        $this->u8("P",208,159); $this->u8("R",208,160); $this->u8("O",208,158);
        $this->u8("L",208,155); $this->u8("D",208,148); $this->u8("ZH",208,150);
        $this->u8("E",208,173); $this->u8("JA",208,175); $this->u8("TSH",208,167);
        $this->u8("S",208,161); $this->u8("M",208,156); $this->u8("I",208,152);
        $this->u8("T",208,162); $this->u8("",208,172); $this->u8("",208,145);
        $this->u8("JU",208,174); $this->u8("E",209,145); $this->u8("E",208,129);

        return $this->utf2win($text);
    }

    function utf2win ($text) {
        global $w8, $lc;

        $c1 = chr(208);
        $c2 = chr(209);
        $u = false;
        $temp = "";

        for($i = 0; $i < strlen($text); $i++) {
            $c = substr($text,$i,1);

            if ($u) {
                $c = $w8[$lc.$c];
                $temp .= isset($c)?$c:"?";
                $u = false;
            } else if ($c==$c1 || $c==$c2) {
                $u = true;
                $lc = $c;
            } else
                $temp .= $c;
        }

        return $temp;
    }

    function str_encode($string, $to = "iso-8859-9", $from = "utf8") {
        if ($to == "iso-8859-9" && $from == "utf8") {
            $str_array = array(
                chr(196).chr(177) => chr(253),
                chr(196).chr(176) => chr(221),
                chr(195).chr(182) => chr(246),
                chr(195).chr(150) => chr(214),
                chr(195).chr(167) => chr(231),
                chr(195).chr(135) => chr(199),
                chr(197).chr(159) => chr(254),
                chr(197).chr(158) => chr(222),
                chr(196).chr(159) => chr(240),
                chr(196).chr(158) => chr(208),
                chr(195).chr(188) => chr(252),
                chr(195).chr(156) => chr(220),
                chr(195).chr(149) => chr(213),
                chr(195).chr(181) => chr(245),
                chr(195).chr(164) => chr(228),
            );

            return str_replace(array_keys($str_array), array_values($str_array), $string);
        }

        return $string;
    }

    /*
    getPage - get page
    */
    function getPage($id, $table = 'page') {
        $param = array(
            'table'		=> $table,
            'where'		=> 'id="'.intval($id).'"',
        );
        $result = $this->getSQL($param);

        //TODO: better redirect check?
        if ($result[$this->lang.'_redirect_url'] && $result[$this->lang.'_redirect_url'] != 'http://') {
            header('Location: '.$result[$this->lang.'_redirect_url']);
            exit;
        }
        return $result;
    }

    /**
     * @return array|bool|QueryResult
     * @author Dimitry Shirokovskiy
     */
    public function getParentPage()
    {
        $result = false;
        if (isset($this->page) && intval($this->page['parent']) > 0) {
            $param = array(
                'table'		=> 'page',
                'where'		=> 'id='.intval($this->page['parent']),
            );
            $result = $this->getSQL($param);
        }

        return $result;
    }

    /*
    die2 - nicer die output
    */
    function die2($content) {
        $this->outPut($content);

        exit;
    }

    /*
    getSetup
    */
    function getSetup($type = 'trans') {
        $array = $this->getSQL(array('table' => $type, 'single' => false));

//detect language
//dms need to show separate ones:
//		for translations we show user selected language
//		for setup we need to show currently selected language, if not supported then default language
        $lang = $this->lang;
        if ($type == 'setup')
            if (isset($_GET['cur_lang']))
                $lang = substr($_GET['cur_lang'], 0, 3);
            elseif (isset($_GET['lang']))
                $lang = substr($_GET['lang'], 0, 3);
            else
                $lang = $this->defaultlang;

        $return = array();
        //value array (code as key) with needed language values
        for ($i = 0; $i < count($array); $i++) {
            $return[$array[$i]['code']] = $array[$i][$lang];
        }

        return $return;
    }

    function backend() {
        $this->checkLogin();
    }

    //version
    //should be calculated from files
    function version() {
        $version = '4 build 1 <strong>ALFA</strong>';

        return $version;
    }

//dms login
    function loginForm() {
        $output = '
<div class="loginform">
<img src="'.DIR.'images/logo.gif" title="dooker.eu DMS" />';

        if (isset($_SESSION['gmessage']) && $_SESSION['gmessage'])
            $output.= '
  <div id="gmessage" class="note">'.$_SESSION['gmessage'].'</div>';

        $output.= '
  <form method="post" action="'.URL.'/login_buffer.php" name="login">
  <label for="auth_username">'.$this->trans('dms_username').'</label>
  <input type="text" name="auth_username" id="auth_username" />
  <label for="auth_username">'.$this->trans('dms_password').'</label>
  <input type="password" name="auth_password" />
  <input type="submit" name="button" value="'.$this->trans('dms_login').'" class="loginbutton" />
  <input type="hidden" name="redirect" value="'.('http://'.$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'] == URL.'/dms/' ? URL.'/dms/' : 'http://'.$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI']).'" />
  </form>
</div>';

        $this->outPut($output);
    }


    //footer output
    function footer() {
        $output = '';
        if ($this->checkForDMS()) {
            if ($this->isLogged())
                $output = '<div class="footer">DMS '.$this->version().' [<a href="http://www.dooker.eu" target="_blank">www.dooker.eu</a>]</div>';
        } else {
            $param = array('where' => 'template="footer"');
            $footer = $this->getSQL($param);
            $output = stripslashes($footer[$this->lang.'_text']);
        }

        $this->outPut($output);
    }

    //user level vs menuitem
    function menuItemReturner($key) {
        $user = $this->userinfo();
        $default = $this->defaults[$key];
        $return = '';
        if (is_array($default)) {
            foreach($default as $key) {
                if ($user['level'] <= $key['level'])
                    $return.= '<li><a href="'.URL.'/dms/?page='.$key['name'].'">'.($key['icon'] ? '<img src="'.$key['icon'].'" title="'.$this->trans($key['translation']).'" />' : $key['display']).'</a></li>';
            }
        }

        return $return;
    }

//dms main menu
//hardcoded menu for general functions
//must include conf vs user access level
    function dms_mainmenu() {
        $user = $this->userinfo();

        $output = '<ul class="mainmenu">
  <li><a href="'.URL.'/dms/?page=profile"><img src="http://www.karlsbach.eu/template/dms/images/icon/profile.jpg" title="'.$this->trans('dms_menu_profile').'" /></a></li>
  <li><a href="'.URL.'/dms/logout/"><img src="http://www.karlsbach.eu/template/dms/images/icon/logout.jpg" title="'.$this->trans('dms_menu_logout').'" /></a></li>
  <li class="separator"></li>';

        $output.= $this->menuItemReturner('mainmenu');

        if ($this->user['level'] == 3)
            $output.= '<li><a href="http://www.karlsbach.eu/dms/?page=ftp"><img src="/template/dms/images/icon/ftp_icon.png" title="[dms_menu_ftp]" /></a></li>';

        $output.= '</ul>';

        $this->outPut($output);
    }

    //displays list templates in DMS menu
    function showCustomList() {
        $attr = array(
            'single'	=> false,
            'fields'	=> 'id, '.$this->lang.', template',
            'where' 	=> 'template="list" || template="news" || template="calendar"',
            'order' 	=> 'jrk'
        );
        $res = $this->getSQL($attr);
        $output = '';

        for ($i = 0; $i < count($res); $i++) {
            $row = $res[$i];

            $output.= '
  <li><a href="'.URL.'/dms/?page='.($row['template'] == 'news' ? 'list' : $row['template']).'&parent='.$row['id'].'" class="header">'.$row[$this->lang].'</a></li>
  <li><a href="'.URL.'/dms/?page='.($row['template'] == 'news' ? 'list' : $row['template']).'&parent='.$row['id'].'&act=add_item">'.$this->trans('dms_menu_addnew').'</a></li>';
        }

        return $output;
    }

//dms menu
//should already include links from conf
    function dms_menu() {
        $output = '';
        if ($this->user['level'] == 3) {
            $output.= '
<ul class="sidemenu">
</ul>';
        } else {

            $output.= '
<ul class="sidemenu">
  <li><a href="'.URL.'/dms/?page=site" class="header">'.$this->trans('dms_menu_mainsite').'</a></li>';
//TODO:will need configuration implement

            $menu = $this->enum_select(SQL_PREFIX.'page', 'menu');

            for ($i = 0; $i < count($menu); $i++) {
                $output.= '
  <li><a href="'.URL.'/dms/?page=site&p='.$menu[$i].'" class="header">'.$this->trans('dms_menu_'.$menu[$i]).'</a></li>
  <li><a href="'.URL.'/dms/?page=site&act=add_item&parent=parent&p='.$menu[$i].'">'.$this->trans('dms_menu_addnew').'</a></li>';
            }

//store countries & towns
            $output.= '
  <li><a href="'.URL.'/dms/?page=stores" class="header">'.$this->trans('dms_menu_stores').'</a></li>
  <li><a href="'.URL.'/dms/?page=stores">'.$this->trans('dms_menu_lists').'</a></li>';

            /*
                $output.= '
              <li class="headerfake">'.$this->trans('dms_menu_lists').'</li>';

                $output.= $this->showCustomList();
            */
            $output.= '
  <li class="headerfake" style="cursor: pointer;" onclick="window.open(\'http://'.$_SERVER['SERVER_NAME'].'/file/index.php\', \'file_uploader\', \'width=1024px,height=400px,scrollbars=1\');">'.$this->trans('dms_menu_pdfupload').'</li>
</ul>';

        }

        $this->outPut($output);

    }



//shows page main area content
    function dms_content() {
        $output = '';
        $default = $this->defaults['allowed_templates'];

        if (isset($_GET['page']) && $_GET['page'] && in_array($_GET['page'], $default)) {
            $output.= $this->useTemplate($_GET['page']);
        }

        $this->outPut($output);
    }


    // menu locator link
    //CAN NOT be first parameter
    function showP() {
        //TODO>check the "menu" values and compare them
        if (isset($_GET['p']))
            return '&p='.$_GET['p'];
    }


    //menulist, u know the old one from admin.php, the main shit
    function showList($attr, $type = 'mainlist') {
        $lang = $this->lang;
        $parent = $output = '';
        $res = $this->getSQL($attr);
        $n = $res->getRowCount();

        if (!$parent)
            $parent = 'parent';
        if (isset($_GET['page']) && ($_GET['page'] == 'list' || $_GET['page'] == 'portfolio'))
            $parent = intval($_GET['parent']);

        //TODO: check for header
        if ($attr['level'] == 0) {
            $output.= '
<form method="post" id="mainlist" action="'.URL.'/'.DMS.'?page='.$_GET['page'].'&act=update_list'.$this->showP().'" enctype="multipart/form-data">
<div class="editorheader">
  <div class="editorheaderbuttons">';

            if ($this->setup[$_GET['page']]['adminlist']['mainadd'])
                $output.= '<a href="?page='.$_GET['page'].'&act=add_item&parent='.$parent.$this->showP().'"><img src="'.DIR.'images/edit/'.$lang.'/new.gif" title="'.$this->trans('button_addnew').'" /></a>';

            $output.= '</div></div><input type="hidden" name="imageupload_delete" id="imageupload_delete" value="0" />';

            if ($n > 0)
                $output.= '
<div class="imageuploadbutton">
  <input type="submit" value="'.$this->trans('button_savelist').'" class="inputbutton" />
  <input type="submit" onclick="$(\'#imageupload_delete\').val(\'1\');" value="'.$this->trans('button_delete').'" class="inputbutton" />
</div>
<div class="imageuploadbutton">
  <div class="deletecol deletecolbutton selectall">'.$this->trans('editor_selectall').'</div>
  <div class="deletecol deletecolbutton unselectall">'.$this->trans('editor_unselectall').'</div>
</div>';

            $output.= '<ul class="mainlist orderlist">';
        }

        for ($i = 0; $i < $n; $i++) {
            $row = $res->getRow();

            if ($row) {
                $display = stripslashes($row[$lang]);

                if ($attr['level'] > 0)
                    $parent = $row['parent'];
                if ($_GET['page'] == 'list' || $_GET['page'] == 'portfolio')
                    $display = stripslashes($row[$lang.'_topic']);

                $_GET['parent'] = isset($_GET['parent']) ? $_GET['parent'] : '';
                $param = array('where' => 'id="'.intval($_GET['parent']).'"');
                $parentpage = $this->getSQL($param);

                //show hidden pages differently
                //TODO>better configuration for showing line
                if ($parentpage['template'] == 'contact')
                    $display = $display.' ['.$row['id'].']';
                if (isset($row['hidden']) && $row['hidden'] == 'yes')
                    $display = '<em>'.$display.'</em> [<strong>'.$this->trans('editor_hidden').'</strong>]';

                $output.= '
                    <li class="level'.$attr['level'].'">
                        <input type="hidden" name="item[]" value="'.$row['id'].'" />
                        <div class="itemname"><a href="?page='.$_GET['page'].'&act=update_item&id='.$row['id'].'&parent='.$parent.$this->showP().'">'.$display.'</a></div>
                        <div class="itembuttons">';

                //TODO: list from config
                if (isset($row['template']) && $row['template'] == 'products' && $row['parent'] == 'parent')
                    $output.= '<a href="?page='.$_GET['page'].'&act=files&id='.$row['id'].'&parent='.$parent.$this->showP().'"><img src="'.DIR.'images/edit/'.$lang.'/files.gif" title="'.$this->trans('button_files').'" /></a>';
                if (isset($row['template']) && ($row['template'] == 'catalog' || $row['template'] == 'pricelist')) {
                    $size = getimagesize(DIR.'images/edit/'.$lang.'/files.gif');

                    $output.= '<a style="padding-right: '.$size[0].'px; " href="?page='.$_GET['page'].'&act=files&id='.$row['id'].'&parent='.$parent.$this->showP().'"><img src="'.DIR.'images/edit/'.$lang.'/files.gif" title="'.$this->trans('button_files').'" /></a>';
                }
                if (isset($row['template']) && ($row['template'] == 'products' || $row['template'] == 'sale-bonus' || $row['template'] == 'contact'))
                    $output.= '<a href="?page=list&parent='.$row['id'].'"><img src="'.DIR.'images/edit/'.$lang.'/list.gif" title="'.$this->trans('button_list').'" /></a>';

                if (isset($row['template']) && $row['template'] == 'news') {
                    $output.= '<a href="?page=list&parent='.$row['id'].'"><img src="'.DIR.'images/edit/'.$lang.'/list.gif" title="'.$this->trans('button_list').'" /></a>';
                    $output.= '<a href="?page='.$_GET['page'].'&act=gallery&id='.$row['id'].'&parent='.$parent.$this->showP().'"><img src="'.DIR.'images/edit/'.$lang.'/images.gif" title="'.$this->trans('button_gallery').'" /></a>';
                } elseif (isset($row['template']) && $row['template'] == 'calendar') {
                    $output.= '<a href="?page=calendar"><img src="'.DIR.'images/edit/'.$lang.'/list.gif" title="'.$this->trans('button_list').'" /></a>';
                } elseif (isset($row['template']) && $row['template'] == 'portfolio') {
                    $output.= '<a href="?page=portfolio&parent='.$row['id'].'"><img src="'.DIR.'images/edit/'.$lang.'/list.gif" title="'.$this->trans('button_list').'" /></a>';
                } elseif (isset($row['template']) && $row['template'] != 'list' && $row['template'] != 'footer' && $_GET['page'] != 'stores' && $parentpage['template'] != 'products' && $parentpage['template'] != 'contact') {
                    $output.= '<a href="?page='.$_GET['page'].'&act=gallery&id='.$row['id'].'&parent='.$parent.$this->showP().'"><img src="'.DIR.'images/edit/'.$lang.'/images.gif" title="'.$this->trans('button_gallery').'" /></a>';
                }

                $output.= '<a href="?page='.$_GET['page'].'&act=update_item&id='.$row['id'].'&parent='.$parent.$this->showP().'"><img src="'.DIR.'images/edit/'.$lang.'/edit.gif" title="'.$this->trans('button_change').'" /></a>';
                $output.= '<div class="deletecol"><input type="checkbox" name="deleteitem[]" class="abu" value="'.$row['id'].'" /></div>';

                if ($attr['level'] < $this->setup[$_GET['page']]['adminlist']['level'] && $_GET['page'] != 'list' && isset($row['template']) && $row['template'] != 'footer') {
                    if ($row['template'] == 'calendar')
                        $link = '?page=2&act=add_item&parent=parent';
                    else
                        $link = '?page='.$_GET['page'].'&act=add_item&parent='.$row['id'].$this->showP();

                    $output.= '<a href="'.$link.'"><img src="'.DIR.'images/edit/'.$lang.'/new.gif" title="'.$this->trans('button_addnew').'" /></a>';
                } else {
                    if ($_GET['page'] != 'portfolio' && $_GET['page'] != 'list') {
                        $size = getimagesize(DIR.'images/edit/'.$lang.'/files.gif');

                        $output.= '<div style="width: '.$size[0].'px; height: '.$size[1].'px; float: right; "></div>';
                    }
                }

                $output.=  '</div>';

                if ($attr['recursive'] && $attr['level'] < $this->setup[$_GET['page']]['adminlist']['level'] + 1 && $_GET['page'] != 'list') {
                    $attr['level']++;
                    $attr['where'] = 'parent="'.$row['id'].'"'.(isset($attr['where_add'])?$attr['where_add']:'');

                    $output.= '
  <ul class="sublist orderlist">'.$this->showList($attr, $type).'</ul>';
                    $attr['level']--;
                }

                $output.=  '</li>';
            }
        }

        if ($attr['level'] == 0) {
            $output.= '
</ul>';

            if ($n > 0)
                $output.= '
<div class="imageuploadbutton">
  <div class="deletecol deletecolbutton selectall">'.$this->trans('editor_selectall').'</div>
  <div class="deletecol deletecolbutton unselectall">'.$this->trans('editor_unselectall').'</div>
</div>
<div class="imageuploadbutton">
  <input type="submit" onclick="$(\'#imageupload_delete\').val(\'1\');" value="'.$this->trans('button_delete').'" class="inputbutton" />
  <input type="submit" value="'.$this->trans('button_savelist').'" class="inputbutton" />
</div>';

            $output.= '
<input type="hidden" name="redirect" value="http://'.$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'].'" />
</form>';
        }

        return $output;
    }



//image uploader for gallery
    function imageUploader($showlang = true) {
        $lang = $this->lang;
        $lang_list = $this->returnLangList();

        if (!$_GET['cur_lang'])
            $_GET['cur_lang'] = $lang_list[0]['name'];

        global $db;

        $param = array('where' => 'id="'.intval($_GET['id']).'"');
        $parent = $this->getSQL($param);

        $output = '<div class="editorheader"><div class="editorheaderbuttons">';
        if ($_GET['page'] != 'headerimage') {
            if (($parent['template'] == 'products' && $parent['parent'] == 'parent') || $parent['template'] == 'catalog' || $parent['template'] == 'pricelist')
                $output.= '<a href="?page='.$_GET['page'].'&act=files&id='.$_GET['id'].'&parent='.$_GET['parent'].$this->showP().'"><img src="'.DIR.'images/edit/'.$lang.'/files.gif" title="'.$this->trans('button_files').'" /></a>';
            $output.= '<a href="?page='.$_GET['page'].'&act=update_item&id='.$_GET['id'].'&parent='.$_GET['parent'].$this->showP().'"><img src="'.DIR.'images/edit/'.$lang.'/text.gif" title="'.$this->trans('button_text').'" /></a>';
            $output.= '<a href="?page='.$_GET['page'].($_GET['parent'] ? '&parent='.$_GET['parent'] : '').$this->showP().'"><img src="'.DIR.'images/edit/'.$lang.'/list.gif" title="'.$this->trans('button_list').'" /></a>';
        }

        $output.= '
  </div>
</div>

<div class="note">'.$this->trans('note_uploader_allowedtypes').'</div>

<div id="content">
  <form id="form1" action="upload.php" method="post" enctype="multipart/form-data">
    <div class="fieldset flash" id="fsUploadProgress">
    <span class="legend">'.$this->trans('note_uploader_legendtopic').'</span>
  </div>
  <div id="divStatus">'.$this->trans('note_uploader_filecounter').'</div>
  <div>
    <span id="spanButtonPlaceHolder"></span>
    <input id="btnCancel" type="button" value="'.$this->trans('button_cancelbutton').'" onclick="swfu.cancelQueue();" disabled="disabled" />
  </div>
  </form>
</div>

<form method="post" id="mainlist" action="'.URL.'/'.DMS.'?page='.$_GET['page'].'&act=update_list&type=gallery&cur_lang='.$_GET['cur_lang'].$this->showP().'" enctype="multipart/form-data" class="uploadimagelist">

<input type="hidden" name="new_lang" id="new_lang" value="'.$_GET['cur_lang'].'" />

<div class="editorheader">
  <div class="editorheadertext">'.$this->trans('editor_chooseland').'</div>
</div>';

        if ($showlang)
            $output.= $this->showLangTabs();

        $output.= '
<input type="hidden" name="imageupload_delete" id="imageupload_delete" value="0" />
<div><input type="submit" value="'.$this->trans('button_savelist').'" class="inputbutton" /></div>
<div><input type="submit" onclick="$(\'#imageupload_delete\').val(\'1\');" value="'.$this->trans('button_delete').'" class="inputbutton" /></div>
<div class="imageuploadbutton">
  <div class="deletecol deletecolbutton selectall">'.$this->trans('editor_selectall').'</div>
  <div class="deletecol deletecolbutton unselectall">'.$this->trans('editor_unselectall').'</div>
</div>
<div class="editorheader">
  <div class="editorheadertext">'.$this->trans('editor_uploadedimages').'</div>
</div>';

        if ($parent['template'] == 'sale-bonus' && $_GET['parent'] == 'parent')
            $output.= '
<div class="editorheader">
  <img src="'.DIR.'images/dms_sales-bonus_help.gif" title="helper" />
</div>';

        $output.= '
<ul class="mainlist orderlist">';

        $res = $this->getSQL(array('single' => false, 'table' => ($_GET['page'] == 'headerimage' ? 'header' : 'images'.($_GET['page'] == 'portfolio' ? '_portfolio' : '')), 'where' => ($_GET['page'] == 'headerimage' ? '' : 'parent="'.intval($_GET['id']).'"'), 'order' => 'jrk'));

        $_SESSION['return_url'] = $_SERVER['REQUEST_URI'];

        for ($i = 0; $i < count($res); $i++) {
            @$row = $res[$i];
            $dir = $row['path'].'tnb/';
            if ($_GET['page'] == 'portfolio')
                $dir = 'upload/portfolio/150/';
            elseif ($_GET['page'] == 'headerimage')
                $dir = 'upload/header/';

//TODO: must get bg image size from conf, same as "upload.php"
            if ($row['filename'] != '' and file_exists($dir.$row['filename'])) {
                $output.= '
  <li>
    <input type="hidden" name="item[]" value="'.$row['id'].'" />
    <div class="itemimage">
      <img src="'.URL.'/'.$dir.$row['filename'].'?'.rand().'" title=""'.($_GET['page'] == 'headerimage' ? ' width="780"' : '').' />
	  <div class="imagehoverbuttons">
        <a href="'.URL.'/dms.php?act=image_rotate&dir=right&id='.$row['id'].'"><img src="'.DIR.'images/edit/rotate_right.png" title="" /></a>
        <a href="'.URL.'/dms.php?act=image_rotate&dir=left&id='.$row['id'].'"><img src="'.DIR.'images/edit/rotate_left.png" title="" /></a>
      </div>
    </div>
    <div class="itembuttons">';

                if ($_GET['page'] != 'headerimage')
                    $output.= '
  <input type="text" name="textitem[]" value="'.stripslashes($row[$_GET['cur_lang']]).'" class="inputfieldlong" />';
//			$output.= '<a href="?page='.$_GET['page'].'&act=muudapildid&id='.$_GET['id'].'&target='.$row['id'].'&parent='.$_GET['parent'].'&p='.$_GET['p'].'"><img src="'.DIR.'images/edit/'.$lang.'/edit.gif" title="Muuda" border="0" align="absmiddle"></a>';
                $output.= '<div class="deletecol"><input type="checkbox" name="deleteitem[]" class="abu" value="'.$row['id'].'" /></div>';
                $output.= '</div>
  </li>';
            }
        }

        $output.= '
</ul>
<div class="imageuploadbutton">
  <div class="deletecol deletecolbutton selectall">'.$this->trans('editor_selectall').'</div>
  <div class="deletecol deletecolbutton unselectall">'.$this->trans('editor_unselectall').'</div>
</div>
<div><input type="submit" onclick="$(\'#imageupload_delete\').val(\'1\');" value="'.$this->trans('button_delete').'" class="inputbutton" /></div>
<div><input type="submit" value="'.$this->trans('button_savelist').'" class="inputbutton" /></div>
<input type="hidden" name="redirect" value="http://'.$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'].'" />
</form>';

        $this->outPut($output);
    }






    // image uploader for gallery
    function fileUploader($showlang = true) {
        $lang = $this->lang;
        $lang_list = $this->returnLangList();

        if (!$_GET['cur_lang'])
            $_GET['cur_lang'] = $lang_list[0]['name'];

        global $db;

        $output = '<div class="editorheader"><div class="editorheaderbuttons">';

        if ($_GET['page'] != 'headerimage') {
            $output.= '<a href="?page='.$_GET['page'].'&act=gallery&id='.$_GET['id'].'&parent='.$_GET['parent'].$this->showP().'"><img src="'.DIR.'images/edit/'.$lang.'/images.gif" title="'.$this->trans('button_gallery').'" /></a>';
            $output.= '<a href="?page='.$_GET['page'].'&act=update_item&id='.$_GET['id'].'&parent='.$_GET['parent'].$this->showP().'"><img src="'.DIR.'images/edit/'.$lang.'/text.gif" title="'.$this->trans('button_text').'" /></a>';
            $output.= '<a href="?page='.$_GET['page'].($_GET['parent'] ? '&parent='.$_GET['parent'] : '').$this->showP().'"><img src="'.DIR.'images/edit/'.$lang.'/list.gif" title="'.$this->trans('button_list').'" /></a>';
        }

        $output.= '</div></div>';

        /*
            $output.= '
        <div class="note">'.$this->trans('note_uploader_allowedtypes_pdf').'</div>

        <div id="content">
          <form id="form1" action="upload.php" method="post" enctype="multipart/form-data">
            <div class="fieldset flash" id="fsUploadProgress">
            <span class="legend">'.$this->trans('note_uploader_legendtopic_files').'</span>
          </div>
          <div id="divStatus">'.$this->trans('note_uploader_filecounter').'</div>
          <div>
            <span id="spanButtonPlaceHolder"></span>
            <input id="btnCancel" type="button" value="'.$this->trans('button_cancelbutton').'" onclick="swfu.cancelQueue();" disabled="disabled" />
          </div>
          </form>
        </div>

        <form method="post" id="mainlist" action="'.URL.'/'.DMS.'?page='.$_GET['page'].'&act=update_list&type=fileuploader&cur_lang='.$_GET['cur_lang'].$this->showP().'" enctype="multipart/form-data" class="uploadimagelist">

        <input type="hidden" name="new_lang" id="new_lang" value="'.$_GET['cur_lang'].'" />
        ';
        */
        /*
        <div class="editorheader">
          <div class="editorheadertext">'.$this->trans('editor_chooseland').'</div>
        </div>';
        */
        /*
            $output.= '
        <input type="hidden" name="fileupload_delete" id="fileupload_delete" value="0" />
        <div><input type="submit" onclick="$(\'#fileupload_delete\').val(\'1\');" value="'.$this->trans('button_delete').'" class="inputbutton" /></div>
        <div class="imageuploadbutton">
          <div class="deletecol deletecolbutton selectall">'.$this->trans('editor_selectall').'</div>
          <div class="deletecol deletecolbutton unselectall">'.$this->trans('editor_unselectall').'</div>
        </div>
        <div class="editorheader">
          <div class="editorheadertext">'.$this->trans('editor_uploadedimages').'</div>
        </div>
        <ul class="mainlist">';

            $res = $this->getSQL(array('single' => false, 'table' => 'files', 'where' => 'parent="'.intval($_GET['id']).'"', 'order' => 'jrk'));

            for ($i = 0; $i < count($res); $i++) {
                @$row = $res[$i];
                 $dir = $row['path'];

                if ($row['filename'] != '' and file_exists($dir.$row['filename'])) {
                    $output.= '
          <li>
            <input type="hidden" name="item[]" value="'.$row['id'].'" />
            <div class="itemimage">'.$dir.$row['filename'].'</div>
            <div class="itembuttons">';

                    $output.= '<div class="deletecol"><input type="checkbox" name="deleteitem[]" class="abu" value="'.$row['id'].'" /></div>';
                    $output.= '</div>
          </li>';
                }
              }

            $output.= '
        </ul>
        <div class="imageuploadbutton">
          <div class="deletecol deletecolbutton selectall">'.$this->trans('editor_selectall').'</div>
          <div class="deletecol deletecolbutton unselectall">'.$this->trans('editor_unselectall').'</div>
        </div>
        <div><input type="submit" onclick="$(\'#fileupload_delete\').val(\'1\');" value="'.$this->trans('button_delete').'" class="inputbutton" /></div>
        <input type="hidden" name="redirect" value="http://'.$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'].'" />
        </form>';
        */

        $dir = 'upload/pdf/';
        $filelist = $this->getFileList($dir);

        $this->checkPDFtnbs($filelist);
        $this->clean_up_pdf($filelist);

        $param = array(
            'table'		=> 'files',
            'where'		=> 'id="'.intval($_GET['id']).'"',
        );
        $current_file = $this->getSQL($param);

        $param = array(
            'table'		=> 'pdf',
            'where'		=> 'file_id="'.intval($_GET['id']).'"',
        );
        $pdf_type = $this->getSQL($param);

        $output.= '
<form method="post" id="mainlist" action="'.URL.'/'.DMS.'?page='.$_GET['page'].'&act=update_list&type=fileuploader&cur_lang='.$_GET['cur_lang'].$this->showP().($_GET['act'] == 'files' ? '&id='.$_GET['id'] : '').'" enctype="multipart/form-data" class="uploadimagelist">
<input type="hidden" name="new_lang" id="new_lang" value="'.$_GET['cur_lang'].'" />
<input type="hidden" name="redirect" value="http://'.$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'].'" />

<select name="file">
  <option value="none">'.$this->trans('filelist_selector_default').'</option>';

        for ($i = 0; $i < count($filelist); $i++)
            $output.= '
  <option value="'.$filelist[$i].'"'.($current_file['filename'] == $filelist[$i] ? ' selected' : '').'>'.$filelist[$i].'</option>';

        $output.= '
</select>

<div><input type="submit" value="'.$this->trans('button_change_plain').'" class="inputbutton" /></div>

<div class="editorrow">
  '.$this->trans('editor_pdf_type').':<br />
  <input type="radio" name="pdf_type" value="portrait"'.($pdf_type['type'] == 'portrait' || !$pdf_type ? ' checked' : '').' /> '.$this->trans('editor_pdf_type_portrait').'
  <input type="radio" name="pdf_type" value="landscape"'.($pdf_type['type'] == 'landscape' ? ' checked' : '').' /> '.$this->trans('editor_pdf_type_landscape').'
</div>

</form>';

        $this->outPut($output);
    }


    function getFileList($dir) {
        $list = glob($dir . "*.pdf");

        return $list;
    }


//delete not needed directorys
//no pdf => deleted => must remove dir
    function clean_up_pdf() {
        $dir = "upload/pdf/";
        $dh  = opendir($dir);
        while (false !== ($filename = readdir($dh))) {
            if (!in_array($filename, array(".",".."))) {
                if (is_dir('upload/pdf/'.$filename))
                    $dirs[] = $filename;
                else
                    $file[] = str_replace('.pdf', '', $this->checkPDFname($filename, false));
            }
        }

        $diff = array_diff($dirs, $file);
        $diff = array_values($diff);

        for ($i = 0; $i < count($diff); $i++)
            @system("rm -rf ".escapeshellarg($dir.$diff[$i]));
    }


    function checkPDFtnbs($list) {
        for ($i = 0; $i < count($list); $i++) {
            $dirname = $this->dirnamereturner($list[$i]);
            $pdfname = str_replace('upload/pdf/', '', $list[$i]);

            $pdfname = $this->checkPDFname($pdfname);

            $exists[] = $dirname;

            if (!is_dir('upload/pdf/'.$dirname)) {
                mkdir('upload/pdf/'.$dirname);
                $this->generatePDFimages($pdfname);
            }
        }
    }


    function checkPDFname($pdf, $rename = true) {
        $nu_pdf = str_replace(' ', '_', mb_strtolower($this->title_slug(str_replace('.pdf', '', $pdf)), 'UTF-8')).'.pdf'; // remove .pdf and add - somewhere in cyrillic decryption it gets lost

        if ($nu_pdf != $pdf && $rename)
            rename('upload/pdf/'.$pdf, 'upload/pdf/'.$nu_pdf);

        return $nu_pdf;
    }


    function replacespacer($i) {
        $spacer = '000000';

        $new = substr($spacer, 0, strlen($spacer) - strlen($i));
        $new = $new.$i;

        return $new;
    }


    function generatePDFimages($pdf) {
        try {
            $im = new imagick($_SERVER['DOCUMENT_ROOT'].'upload/pdf/'.$pdf);
            $im->setImageFormat('jpg');
        } catch (Exception $e) {
            file_put_contents($_SERVER['DOCUMENT_ROOT'].'.jimmy.log', $e->getTraceAsString()."\n");
        }

        for ($i = 0; $i < $im->getNumberImages(); $i++) {
            $url = 'http://'.$_SERVER['HTTP_HOST'].'/pdf2image.php?file='.$pdf.'&img='.$i.'';
            $img = $_SERVER['DOCUMENT_ROOT'].'upload/pdf/'.$this->dirnamereturner($pdf).'/pdf-'.$this->replacespacer($i).'.jpg';
            file_put_contents($img, file_get_contents($url));
        }
    }


    function dirnamereturner($name) {
        $dirname = substr($name, 0, strlen($name) - 4);
        $dirname = str_replace('[^A-Za-z0-9]', '', $dirname);
        $dirname = mb_strtolower(trim($dirname), 'UTF-8');
        $dirname = str_replace('upload/pdf/', '', $dirname);

        return $dirname;
    }



    function getValues($table, $field, $parent = 'parent', $limit = '', $recursive = false, $level = 0) {
        $return = array();
        if ($table != 'stores')
            if ($level == 0)
                $return = array(0 => array('id' => '1', $this->lang => 'parent'));

        $res = $this->getSQL(array('single' => false, 'fields' => $field, 'table' => $table, 'where' => 'parent="'.$parent.'"'.($limit ? ' AND '.$limit : '').($table == 'stores' ? '' : ' AND template!="footer"').'', 'order' => ($table == 'stores' ? '' : 'jrk')));

        for ($i = 0; $i < count($res); $i++) {
            $row = $res[$i];

            $space = '';
            for ($j = 0; $j < $level; $j++)
                $space = $space.'&nbsp;&nbsp;&nbsp;&nbsp;';

            $row[$this->lang] = $space.$row[$this->lang];
            $return[] = $row;

            if ($recursive)
                $newarray = $this->getValues($table, $field, $row['id'], $limit, true, ($level + 1));

            for ($j = 0; $j < count($newarray); $j++) {
                if ($newarray[$j])
                    $return[] = $newarray[$j];
            }
        }

        return $return;
    }

    function enum_select($table, $field) {
        $query = " SHOW COLUMNS FROM `$table` LIKE '$field' ";
        $result = mysql_query( $query ) or die( 'error getting enum field ' . mysql_error() );
        $row = mysql_fetch_array( $result , MYSQL_NUM );

#extract the values
#the values are enclosed in single quotes
#and separated by commas
        $regex = "/'(.*?)'/";
        preg_match_all( $regex , $row[1], $enum_array );
        $enum_fields = $enum_array[1];

        return( $enum_fields );
    }

    function showLangTabs() {
        $lang = $this->lang;
        $lang_list = $this->returnLangList();
        $output = '';

        if (!$_GET['cur_lang'])
            $_GET['cur_lang'] = $lang_list[0]['name'];

        if (count($this->langlist) > 1) {
            $output.= '<div class="langtabs">';

            for ($i = 0; $i < count($this->langlist); $i++)
                $output.= '<div class="tabs'.($_GET['cur_lang'] == $this->langlist[$i]['name'] ? ' active' : '').($_GET['act'] == 'add_item' ? ' disabled' : '').'"'.($_GET['cur_lang'] != $this->langlist[$i]['name'] ? ' onclick="$(\'#new_lang\').val(\''.$this->langlist[$i]['name'].'\'); $(\'form:last\').submit();"' : '').'>'.$this->langlist[$i]['display'].'</div>';

            $output.= '</div>';
        }

        return $output;
    }

    //editor buttons
    function editorButtons() {
        $redirect = array(
            'site'		=> 'site',
            'list'		=> 'list&parent='.$_GET['parent'],
            'portfolio'	=> 'portfolio&parent='.$_GET['parent'],
            'stores'	=> 'stores',
        );
        $output = '';

        if ($_GET['act'] == 'add_item') {
            $output.= '<div><input type="submit" value="'.$this->trans('button_add_plain').'" class="inputbutton" /></div>';
        } else {
            $output.= '<div><input type="submit" value="'.($_GET['act'] == 'update_item' ? $this->trans('button_change') : $this->trans('button_add')).'" class="inputbutton" /></div>
<div><input type="submit" onclick="$(\'#redirect\').val(\''.URL.'/dms/?page='.$redirect[$_GET['page']].$this->showP().'\'); $(\'#new_lang_disable\').val(\'1\');" value="'.($_GET['act'] == 'update_item' ? $this->trans('button_change_list') : $this->trans('button_add_list')).'" class="inputbutton" /></div>';
        }

        return $output;
    }


    function getTemplateNames() {
        $param = array(
            'single'	=> false,
            'table' 	=> 'template',
        );
        $names = $this->getSQL($param);

        for ($i = 0; $i < count($names); $i++)
            $list[$names[$i]['template']] = $names[$i]['display'];

        return $list;
    }


    //TODO>pealkirjad LABEL tagi sisse
    //TODO>conf check for fields
    function showeditor() {
        $lang = $this->lang;
        $lang_list = $this->returnLangList();

        if (!isset($_GET['cur_lang']))
            $_GET['cur_lang'] = $lang_list[0]['name'];

        if ($_GET['act'] == 'update_item') {
            $param['where'] = 'id="'.intval($_GET['id']).'"';

            if ($_GET['page'] == 'portfolio')
                $param['table'] = 'portfolio';

            $row = $this->getSQL($param);
        }

        $param = array('where' => 'id="'.intval($_GET['parent']).'"');
        $parent = $this->getSQL($param);

        $output = '<div class="editorheader"><div class="editorheaderbuttons">';
        if ($row['template'] != 'footer' && $_GET['act'] != 'add_item' && $parent['template'] != 'products' && $parent['template'] != 'contact')
            $output.= '<a href="?page='.$_GET['page'].'&act=gallery&id='.$_GET['id'].'&parent='.$_GET['parent'].$this->showP().'"><img src="'.DIR.'images/edit/'.$lang.'/images.gif" title="'.$this->trans('button_gallery').'" /></a>';
        if (($row['template'] == 'products' || $row['template'] == 'catalog') && $row['parent'] == 'parent')
            $output.= '<a href="?page='.$_GET['page'].'&act=files&id='.$_GET['id'].'&parent='.$_GET['parent'].$this->showP().'"><img src="'.DIR.'images/edit/'.$lang.'/files.gif" title="'.$this->trans('button_files').'" /></a>';
        $output.= '<a href="?page='.$_GET['page'].($_GET['parent'] ? '&parent='.$_GET['parent'] : '').$this->showP().'"><img src="'.DIR.'images/edit/'.$lang.'/list.gif" title="'.$this->trans('button_list').'" /></a>';
        $output.= '
  </div>
</div>

<form method="post" action="'.URL.'/'.DMS.'?page='.$_GET['page'].'&act='.$_GET['act'].($_GET['act'] == 'update_item' ? '&id='.$_GET['id'] : '').'&parent='.$_GET['parent'].(isset($_GET['p']) ? '&p='.$_GET['p'] : '').'&cur_lang='.$_GET['cur_lang'].$this->showP().'" enctype="multipart/form-data" name="info">

<input type="hidden" name="template" value="'.($row['template'] ? $row['template'] : 'text').'" />
<input type="hidden" name="new_lang" id="new_lang" value="'.$_GET['cur_lang'].'" />
<input type="hidden" name="new_lang_disable" id="new_lang_disable" value="" />
<input type="hidden" name="afterSQL" id="afterSQL" value="list" />
<input type="hidden" name="post" value="'.$row['page'].'" />
<input type="hidden" id="key" value="0" />';

//TODO>conf check here!
//TODO>fix the getparenttree language
//link to show real SEO
        /*
                if ($_GET['page'] == 3)
                    $link = getParentTree($row['parent'], $lang).getParentTree($row['id'], $lang);
                else
        */
        if ($this->setup[$_GET['page']]['showSEOlink'] && $parent['template'] != 'contact') {
            $link = $this->getParentTree($row['id'], $_GET['cur_lang']);

            if ($row['id'] && $link != '/')
                $output.= '
<div class="editorrow">
      <div><i>link</i>:</div>
		<a href="'.URL.'/'.$_GET['cur_lang'].'/'.$link.'" target="_dooker_eu_preview">'.URL.'/'.$_GET['cur_lang'].'/'.$link.'</a>
</div>';
        }

        if ($this->setup[$_GET['page']]['parentedit']) {
            $output.= '
<div class="editorrow">
  <div>'.$this->trans('editor_parent').':</div>
  <select name="parent">';

            $parent_list = $this->getValues('page', 'id, '.$this->lang, 'parent', 'id!="'.$row['id'].'" AND page="site"', true);

            for ($i = 0; $i < count($parent_list); $i++)
                $output.= '
    <option value="'.($i == 0 ? 'parent' : $parent_list[$i]['id']).'"'.($row['template'] == $parent_list[$i]['id'] || $parent_list[$i]['id'] == $row['parent'] || ($parent_list[$i]['id'] == $_GET['parent'] && $_GET['act'] == 'add_item') ? ' selected' : '').'>'.$parent_list[$i][$this->lang].'</option>';

            $output.= '</select></div>';
        }

        if ($this->setup[$_GET['page']]['templateedit']) {
            $output.= '
<div class="editorrow">
  <div>'.$this->trans('editor_template').':</div>
  <select name="template">';

            $templates = $this->enum_select(SQL_PREFIX.'page', 'template');

            $display_list = $this->getTemplateNames();

            for ($i = 0; $i < count($templates); $i++)
                $output.= '
  <option value="'.$templates[$i].'"'.($row['template'] == $templates[$i] ? ' selected' : '').'>'.$display_list[$templates[$i]].'</option>';

            $output.= '</select></div>';
        }

        if ($this->setup[$_GET['page']]['locationedit']) {
            $output.= '
<div class="editorrow">
  <div>'.$this->trans('editor_location').':</div>
  <input type="hidden" name="old_menu" value="'.$row['menu'].'" />
  <select name="menu">';

            $menu = $this->enum_select(SQL_PREFIX.'page', 'menu');

            for ($i = 0; $i < count($menu); $i++)
                $output.= '
  <option value="'.$menu[$i].'"'.($row['menu'] == $menu[$i] || ($_GET['act'] == 'add_item' && $_GET['p'] == $menu[$i]) ? ' selected' : '').'>'.$menu[$i].'</option>';

            $output.= '</select></div>';
        }

        if ($_GET['parent'] != 'parent' && $parent['template'] != 'news') {
            $image_param['uploaddir'] = 'upload/theme/';
            $_SESSION['return_url'] = $_SERVER['REQUEST_URI'];

            $output.= '
<div class="editorrow">
  <div>'.($this->setup[$_GET['page']]['specpicresize'] ? $this->trans('editor_pic_resize') : $this->trans('editor_pic').' '.$this->setup[$_GET['page']]['specpicsize'].'px: ').'</div>';

            if ($row['filename'] && file_exists($image_param['uploaddir'].$row['filename']))
                $output.= '
  <div><a href="javascript: if (confirm(\''.$this->trans('msg_delete').'?\')) document.location=\''.URL.'/dms.php?page='.$_GET['page'].'&act=delete_image&id='.$_GET['id'].'&parent='.$_GET['parent'].'&cur_lang='.$_GET['cur_lang'].'\'"><img src="'.URL.'/'.$image_param['uploaddir'].$row['filename'].'" title="" /></a></div>';

            $output.= '
  <div><input type="file" name="uploadfile" class="InputField" style="height: 20px; "></div>
</div>';
        }

        if ($this->setup[$_GET['page']]['hiddenedit']) {
            $output.= '
<div class="editorrow">
  '.$this->trans('editor_hidden').':<br />
  <input type="checkbox" name="hidden"'.($row['hidden'] == 'yes' ? ' checked' : '').' />
</div>';
        }

//coming soon
        if ($parent['template'] != 'contact' && $parent['template'] != 'news') {
            $output.= '
<div class="editorrow">
  '.$this->trans('editor_coming_soon').':<br />
  <input type="checkbox" name="coming_soon"'.($row['coming_soon'] == 'yes' ? ' checked' : '').' />
</div>';
        }

//karlsbach hack
        if (($row['template'] == 'text' || $row['template'] == 'catalog') && $parent['template'] != 'news') {
            $param = array(
                'table'		=> 'color',
                'where'		=> 'page_id="'.intval($_GET['id']).'"',
            );
            $color = $this->getSQL($param);

            $output.= '
<div class="editorrow">
  '.$this->trans('editor_text_color').':<br />
  <input type="radio" name="text_color" value="fff"'.($color['text'] == 'fff' ? ' checked' : '').' />'.$this->trans('editor_color_white').'
  <input type="radio" name="text_color" value="000"'.($color['text'] == '000' ? ' checked' : '').' />'.$this->trans('editor_color_black').'
  <input type="radio" name="text_color" value="ccc"'.($color['text'] == 'ccc' ? ' checked' : '').' />'.$this->trans('editor_color_grey').'
  <input type="radio" name="text_color" value="947839"'.($color['text'] == '947839' ? ' checked' : '').' />'.$this->trans('editor_color_gold').'
</div>';

            $output.= '
<div class="editorrow">
  '.$this->trans('editor_bg_color').':<br />
  <input type="radio" name="bg_color" value="fff"'.($color['bg'] == 'fff' ? ' checked' : '').' />'.$this->trans('editor_color_white').'
  <input type="radio" name="bg_color" value="000"'.($color['bg'] == '000' ? ' checked' : '').' />'.$this->trans('editor_color_black').'
  <input type="radio" name="bg_color" value="ccc"'.($color['bg'] == 'ccc' ? ' checked' : '').' />'.$this->trans('editor_color_grey').'
</div>';
        }

        $output.= $this->editorButtons();

        $output.= '
<div class="editorheader">
  <div class="editorheadertext">'.$this->trans('editor_content').'</div>
</div>';

        $output.= $this->showLangTabs();

//contact hack
        if ($parent['template'] == 'contact') {
            if ($this->setup[$_GET['page']]['topicedit']) {
                $output.= '
<div class="editorrow">
  '.$this->trans('editor_topic').':<br />
  <input type="text" name="'.$_GET['cur_lang'].'_topic" value="'.stripslashes($row[$_GET['cur_lang'].'_topic']).'" class="inputfieldlong" />
</div>';
            }
//products sub hack
        } elseif ($parent['template'] == 'products') {
            $output.= '
<div class="editorrow">
  '.$this->trans('editor_product_topic').':<br />
  <input type="text" name="'.$_GET['cur_lang'].'_topic" value="'.stripslashes($row[$_GET['cur_lang'].'_topic']).'" class="inputfieldlong" />
</div>';

            $output.= '
<div class="editorrow">
  '.$this->trans('editor_product_desc').':<br />
  <input type="text" name="'.$_GET['cur_lang'].'_text" value="'.stripslashes($row[$_GET['cur_lang'].'_text']).'" class="inputfieldlong" />
</div>';

            $output.= '
<div class="editorrow">
  '.$this->trans('editor_product_page').':<br />
  <input type="text" name="'.$_GET['cur_lang'].'_lead" value="'.stripslashes($row[$_GET['cur_lang'].'_lead']).'" class="inputfieldlong" />
</div>';

            $output.= '
<div class="editorrow">
  '.$this->trans('editor_product_image').':<br />';

            $row['filename'] = $row['gallery'];
            $dir = 'upload/products/groups/';

            if ($row['filename'] && file_exists($dir.$row['filename']))
                $output.= '
  <div><a href="javascript: if (confirm(\''.$this->trans('button_delete').'?\')) document.location=\'?page='.$_GET['page'].'&act=db_kustutapilt&type=header&id='.$_GET['id'].'&target='.$row['id'].'&parent='.$_GET['parent'].'&cur_lang='.$_GET['cur_lang'].'\'"><img src="'.URL.'/'.$dir.$row['filename'].'" title="'.$this->trans('button_delete').'" /></a></div>';

            $output.= '
  <div><input type="file" name="uploadfile" class="InputField" style="height: 20px; "></div>
</div>

<input type="hidden" name="hidden_product" value="true" />';

        } else {

            if ($this->setup[$_GET['page']]['redirectedit']) {
                $output.= '
<div class="editorrow">
  '.$this->trans('editor_redirect_url').':<br />

  <input type="text" name="'.$_GET['cur_lang'].'_redirect_url" value="'.($row[$_GET['cur_lang'].'_redirect_url'] ? stripslashes($row[$_GET['cur_lang'].'_redirect_url']) : 'http://').'" class="inputfieldlong" />
</div>';
            }

            if ($this->setup[$_GET['page']]['metadescedit']) {
                $output.= '
<div class="editorrow">
  '.$this->trans('editor_meta_desc').':<br />
  <input type="text" name="'.$_GET['cur_lang'].'_meta_desc" value="'.stripslashes($row[$_GET['cur_lang'].'_meta_desc']).'" class="inputfieldlong" />
</div>';
            }

            if ($this->setup[$_GET['page']]['metakeyedit']) {
                $output.= '
<div class="editorrow">
  '.$this->trans('editor_meta_key').':<br />
  <input type="text" name="'.$_GET['cur_lang'].'_meta_key" value="'.stripslashes($row[$_GET['cur_lang'].'_meta_key']).'" class="inputfieldlong" />
</div>';
            }

            if ($this->setup[$_GET['page']]['menutopicedit']) {
                $output.= '
<div class="editorrow">
  '.$this->trans('editor_menutopic').':<br />
  <input type="text" name="'.$_GET['cur_lang'].'" value="'.stripslashes($row[$_GET['cur_lang']]).'" class="inputfieldlong" />
</div>';
            }

            if (isset($this->setup[$_GET['page']]['portfoliolink'])) {
                $output.= '
<div class="editorrow">
  '.$this->trans('editor_link').':<br />
  <input type="text" name="'.$_GET['cur_lang'].'" value="'.($row[$_GET['cur_lang']] ? stripslashes($row[$_GET['cur_lang']]) : 'http://').'" class="inputfieldlong" />
</div>';
            }

            if ($this->setup[$_GET['page']]['topicedit']) {
                $output.= '
<div class="editorrow">
  '.$this->trans('editor_topic').':<br />
  <input type="text" name="'.$_GET['cur_lang'].'_topic" value="'.stripslashes($row[$_GET['cur_lang'].'_topic']).'" class="inputfieldlong" />
</div>';
            }

            if ($this->setup[$_GET['page']]['dateedit']) {
                if ($parent['template'] != 'sale-bonus')
                    $output.= '
<div class="editorrow">
  '.$this->trans('editor_date').':<br />
  <input type="text" name="date" class="inputfieldshort datepicker" value="'.date('m/d/Y', ($row['timestamp'] ? $row['timestamp'] : mktime())).'" />
</div>';
            }

            if ($this->setup[$_GET['page']]['leadedit']) {
                $output.= '
<div class="editorrow">
  '.$this->trans('editor_lead').':<br />
  <textarea class="ckeditor" name="'.$_GET['cur_lang'].'_lead">'.stripslashes($row[$_GET['cur_lang'].'_lead']).'</textarea>
<script type="text/javascript">//<![CDATA[
	CKEDITOR.replace(\''.$_GET['cur_lang'].'_lead\', { "filebrowserBrowseUrl": "'.URL.'\/ckfinder\/ckfinder.html", "filebrowserImageBrowseUrl": "'.URL.'\/ckfinder\/ckfinder.html?type=Images", "filebrowserFlashBrowseUrl": "'.URL.'\/ckfinder\/ckfinder.html?type=Flash", "filebrowserUploadUrl": "'.URL.'\/ckfinder\/core\/connector\/php\/connector.php?command=QuickUpload&type=Files", "filebrowserImageUploadUrl": "'.URL.'\/ckfinder\/core\/connector\/php\/connector.php?command=QuickUpload&type=Images", "filebrowserFlashUploadUrl": "'.URL.'\/ckfinder\/core\/connector\/php\/connector.php?command=QuickUpload&type=Flash" });
//]]></script>
</div>';
            }
            /*
            //gallery imagelist
                if ($parent['template'] != 'sale-bonus') {
                    $output.= '
            <div id="dgallerycontrol">
              <input type="button" id="inserDGallery" class="inputbutton" value="'.$this->trans('editor_dgallery_insert').'" />
              <input type="button" id="closeDGallery" class="inputbutton" value="'.$this->trans('editor_dgallery_close').'!" />
            </div>
            <div class="demo" id="galleryBox">

            <input type="hidden" id="dgalleryURL" value="'.URL.'" />
            <span id="select-result"></span>

            <ol id="selectable">';

                    $res = $this->getSQL(array('single' => false, 'table' => 'images'.($_GET['page'] == 'portfolio' ? '_portfolio' : ''), 'where' => 'parent="'.intval($_GET['id']).'"', 'order' => 'jrk'));

                    for ($i = 0; $i < count($res); $i++) {
                        $image = $res[$i];
                        $dir = $image['path'].'tnb/';
                        if ($_GET['page'] == 'portfolio') {
                            $image['path'] = 'upload/portfolio/250/';
                            $dir = $image['path'];
                        }

            //TODO: must get bg image size from conf, same as "upload.php"
                        if ($image['filename'] != '' and file_exists($dir.$image['filename'])) {
                            $output.= '
              <li class="ui-state-default">
                <input type="hidden" id="item'.$i.'" value="'.$image['filename'].'" />
                <input type="hidden" id="dir'.$i.'" value="'.$image['path'].'" />
                <input type="hidden" id="add'.$i.'" value="'.($_GET['page'] == 'portfolio' ? '' : 'tnb/').'" />
                <div class="itemimage" style="background-image:url(\''.URL.'/'.$dir.$image['filename'].'\');"></div>
              </li>';
                        }
                      }

                    $output.= '
            </ol>

            </div>';
                }
            */
            if ($this->setup[$_GET['page']]['textedit']) {
//gallery link
                /*
                        if ($parent['template'] != 'sale-bonus')
                            $output.= '
                <div class="dGalleryBox">
                  <div style="" onclick="insertDGallery()">'.$this->trans('editor_dgallery_open').'</div>
                '.$this->trans('editor_dgallery_or').'<br />
                '.$this->trans('editor_dgallery_addgallery').'
                  <select name="dgallery">
                    <option value="none">'.$this->trans('editor_dgallery_choose').'</option>
                    <option value="before"'.($row['gallery'] == 'before' ? ' selected' : '').'>'.$this->trans('editor_dgallery_before').'</option>
                    <option value="after"'.($row['gallery'] == 'after' ? ' selected' : '').'>'.$this->trans('editor_dgallery_after').'</option>
                  </select>
                </div>';
                */
                $output.= '
<div class="editorrow">
  '.$this->trans('editor_content').':<br />
  <textarea class="ckeditor" name="'.$_GET['cur_lang'].'_text" id="editorid">'.stripslashes($row[$_GET['cur_lang'].'_text']).'</textarea>

<script type="text/javascript">//<![CDATA[
	CKEDITOR.replace(\''.$_GET['cur_lang'].'_text\', { "filebrowserBrowseUrl": "'.URL.'\/ckfinder\/ckfinder.html", "filebrowserImageBrowseUrl": "'.URL.'\/ckfinder\/ckfinder.html?type=Images", "filebrowserFlashBrowseUrl": "'.URL.'\/ckfinder\/ckfinder.html?type=Flash", "filebrowserUploadUrl": "'.URL.'\/ckfinder\/core\/connector\/php\/connector.php?command=QuickUpload&type=Files", "filebrowserImageUploadUrl": "'.URL.'\/ckfinder\/core\/connector\/php\/connector.php?command=QuickUpload&type=Images", "filebrowserFlashUploadUrl": "'.URL.'\/ckfinder\/core\/connector\/php\/connector.php?command=QuickUpload&type=Flash" });
//]]></script>

</div>';
            }

            if ($parent['template'] == 'sale-bonus') {
                $price = explode('/', $row['gallery']);

                $output.= '
<div class="editorrow">
  '.$this->trans('editor_bonus_price').':<br />
  <input type="text" name="'.$_GET['cur_lang'].'_price" value="'.$price[0].'" class="inputfield" id="bonus_oldprice" />
</div>
<div class="editorrow">
  '.$this->trans('editor_bonus_price_new').':<br />
  <input type="text" name="'.$_GET['cur_lang'].'_newprice" value="'.$price[1].'" class="inputfield" id="bonus_price" />
</div>
<div class="editorrow">
  '.$this->trans('editor_bonus_percent').':<br />
  <input type="text" name="'.$_GET['cur_lang'].'_percent" value="'.$price[2].'" class="inputfield" id="bonus_percent" />
</div>';
            }
        }

        $output.= $this->editorButtons();

        $output.= '
<input type="hidden" name="redirect" id="redirect" value="http://'.$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'].'" />
</form>';

        $this->outPut($output);
    }

    function user() {
        $this->checkLogin();
        $this->isLogged();
    }

    function checkLogin() {
        $this->user = array();
        if (isset($_POST['auth_username'])) {
            $_POST['auth_username'] = trim($_POST['auth_username']);
        }
        if (isset($_POST['auth_password'])) {
            $_POST['auth_password'] = trim($_POST['auth_password']);
        }

        $auth_username = $auth_password = '';
        $attempt = false;

        if (isset($_POST['auth_username']) || isset($_POST['auth_password'])) {
            $cpass = md5($_POST['auth_password'].$_POST['auth_username']);
            $_SESSION['auth_username'] = $_POST['auth_username'];
            $_SESSION['auth_password'] = $cpass;
            $auth_username = $_POST['auth_username'];
            $auth_password = $cpass;
            $attempt = true;
        } elseif (isset($_SESSION['auth_username']) && isset($_SESSION['auth_password'])) {
            $auth_username = $_SESSION['auth_username'];
            $auth_password = $_SESSION['auth_password'];
        }

        $param = array(
            'table'		=> 'user',
            'where'		=> 'username="'.$auth_username.'"',
        );
        @$user = $this->getSQL($param);

        if ($user['username'] == $auth_username && $user['password'] == $auth_password && $user['username']) {
            $this->user = $user;
            $this->logged = true;
            $_SESSION['user_logged_on'] = true;
        } else {
            if ($attempt) {
                $_SESSION['gmessage'] = $this->trans('msg_login_fail');
            }
        }
    }

    /**
     * check if we have logged in situation
     * @return bool
     */
    public function isLogged() {
        if (isset($this->logged) && $this->logged)
            return true;
        else
            return false;
    }

    /**
     * returns user info
     * @return mixed
     */
    public function userinfo() {
        return $this->user;
    }

//reset temp info
    function clearBuffer() {
        unset($_SESSION['gmessage']);
        unset($_SESSION['displaysomemessage']);
        unset($_SESSION['post']);
    }

//TODO>user level check
    function paramAdder($type = 'trans') {
        $lang = $this->lang;
        $lang_list = $this->returnLangList();

        if (!$_GET['cur_lang'])
            $_GET['cur_lang'] = $lang_list[0]['name'];

        $output = '
<form method="post" action="'.URL.'/'.DMS.'?page='.$type.'&act=add_item&cur_lang='.$_GET['cur_lang'].'" class="transform">
  <label for="code">'.$this->trans('editor_'.$type.'code').':</label>
  <input type="text" name="code" id="code" class="inputfield" />
  <label for="param">'.$this->trans('editor_'.$type.'_field').':</label>
  <input type="text" name="'.$_GET['cur_lang'].'" id="param" class="inputfield" />';

        if ($type == 'trans') {
            $output.= '
  <label for="group_id">'.$this->trans('editor_groupid').':</label>
  <select name="group_id" id="group_id">
    <option></option>';

            $res = $this->getSQL(array('single' => false, 'table' => 'trans', 'where' => 'group_id="0"', 'order' => 'group_id'));
            for ($i = 0; $i < count($res); $i++) {
                $row = $res[$i];

                $output.= '
    <option value="'.$i.'">'.$this->trans('group_'.$i).'</option>';
            }

            $output.= '</select>';
        }

        $output.= '
  <input type="submit" value="'.$this->trans('button_add_plain').'" class="inputbutton" />';

        $output.= '
  <input type="hidden" name="cur_lang" id="cur_lang" value="'.$_GET['cur_lang'].'" />
  <input type="hidden" name="redirect" value="http://'.$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'].'" />
</form>';

        $this->outPut($output);
    }


//setup dropdowns - index
    function paramDropdownsDefault($type) {
        $output = '';
        if ($type) {
            $lang = $this->lang;
            $lang_list = $this->returnLangList();

            if (!$_GET['cur_lang'])
                $_GET['cur_lang'] = $lang_list[0]['name'];

            $output.= '
<div class="editorheader dropdownheader">
  <div class="editorheadertext">'.$this->trans('editor_dropdown_'.$type).'</div>
</div>

<div class="editorrow">
  <select name="'.$type.'_id">
    <option value="0"'.(!$this->configuration[$type.'_id'] || $this->configuration[$type.'_id'] == 0 ? ' selected' : '').'>'.$this->trans('editor_dropdown_default').'</option>';

            unset($param);
            $this->testlist = array();
            if ($type == 'index') {
                $this->getDropdownIndexList();
                $pages_list = $this->testlist;
            } else
                $pages_list = $this->getDropdownList($type);

            for ($i = 0; $i < count($pages_list); $i++) {
                $row = $pages_list[$i];

                $output.= '
    <option value="'.$row['id'].'"'.($row['id'] == $this->configuration[$type.'_id'] ? ' selected' : '').'>'.($row['parent'] != 'parent' && $row['parent'] ? '&nbsp;&nbsp;&nbsp;' : '').$row[$_GET['cur_lang']].'</option>';
            }

            $output.= '
  </select>
</div>';

            $this->outPut($output);
        }
    }


//setup dropdowns - Nivo slider effect type
    function paramDropdownsNivo() {
        $effect_list = array(
            'random',
            'sliceDown',
            'sliceDownLeft',
            'sliceUp',
            'sliceUpLeft',
            'sliceUpDown',
            'sliceUpDownLeft',
            'fold',
            'fade',
            'slideInRight',
            'slideInLeft',
            'boxRandom',
            'boxRain',
            'boxRainReverse',
            'boxRainGrow',
            'boxRainGrowReverse',
        );

        $output = '
<div class="editorheader dropdownheader">
  <div class="editorheadertext">'.$this->trans('editor_dropdown_nivo_effect').'</div>
</div>

<div class="editorrow">
  <select name="index_page_effect">';

        for ($i = 0; $i < count($effect_list); $i++)
            $output.= '
    <option value="'.$effect_list[$i].'"'.($this->configuration['index_page_effect'] == $effect_list[$i] ? ' selected' : '').'>'.$effect_list[$i].'</option>';

        $output.= '</select></div>';

        $this->outPut($output);
    }


    function getDropdownIndexList() {
        $param = array(
            'single' 	=> false,
            'where' 	=> $_GET['cur_lang'].'!="" && parent="parent" && template!="footer"',
            'order' 	=> 'jrk',
        );
        $parentlist = $this->getSQL($param);

        for ($i = 0; $i < count($parentlist); $i++) {
            $row = $parentlist[$i];

            $this->testlist[] = $row;

//we do not allow some blog post to be the index page
            if ($row['template'] != 'news')
                $this->getDropdownIndexListLoop($row['id']);
        }
    }


//
    function getDropdownIndexListLoop($parent) {
        $param = array(
            'single' 	=> false,
            'where' 	=> $_GET['cur_lang'].'!="" && parent="'.$parent.'"',
            'order' 	=> 'jrk',
        );
        $sublist = $this->getSQL($param);

        for ($i = 0; $i < count($sublist); $i++) {
            $row = $sublist[$i];

            $this->testlist[] = $row;

            $this->getDropdownIndexListLoop($row['id']);
        }
    }

//
//2012.05.18 - used no more for index
    function getDropdownList($type) {
        if ($type == 'index')
            return $this->getSQL(array('single' => false, 'where' => $_GET['cur_lang'].'!=""', 'order' => 'jrk, parent'));
        elseif ($type == 'lang')
            return $this->getSQL(array('single' => false, 'fields' => 'id, display as '.$_GET['cur_lang'], 'table' => 'lang', 'where' => 'active="yes"', 'order' => 'jrk'));
    }



//setup dropdowns - theme
    function paramDropdownsTheme() {
        $lang = $this->lang;
        $lang_list = $this->returnLangList();

        if (!$_GET['cur_lang'])
            $_GET['cur_lang'] = $lang_list[0]['name'];

        $row = $this->getSQL(array('table' => 'setup', 'where' => 'code="theme"'));

        if ($handle = opendir('template/')) {
            while (false !== ($entry = readdir($handle))) {
                if ($entry != '.' && $entry != '..' && $entry != 'dms')
                    $list[] = $entry;
            }

            closedir($handle);
        }

        $output = '
<div class="editorheader dropdownheader">
  <div class="editorheadertext">'.$this->trans('editor_themedropdown').'</div>
</div>

<div class="editorrow">';

        for ($i = 0; $i < count($list); $i++) {
            $output.= '
<div class="themeitem'.($i == 0 ? ' themefirst' : '').'">
  <input type="radio" name="input['.$row['id'].'_'.$_GET['cur_lang'].']" value="'.$list[$i].'"'.($row[$_GET['cur_lang']] == $list[$i] ? ' checked' : '').' />'.$list[$i].(file_exists('template/'.$list[$i].'/'.$list[$i].'.jpg') ? '<br /><img src="'.URL.'/template/'.$list[$i].'/'.$list[$i].'.jpg" title="'.$list[$i].'" class="themepreviewimage" />' : '').'
</div>';
        }

        $output.= '
</div>';

        $this->outPut($output);
    }


    //TODO> we dont need LANG in the INPUT code, we can use global CUR_LANG instead
    //TODO> DIV transgroupX should hide trans if we have everything translated
    function paramList($type = 'trans') {
        $lang = $this->lang;
        $lang_list = $this->returnLangList();

        if (!$_GET['cur_lang'])
            $_GET['cur_lang'] = $lang_list[0]['name'];

        $output = '
<input type="hidden" name="redirect" value="http://'.$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'].'" />
<div class="editorheader">
  <div class="editorheadertext">'.$this->trans('editor_'.$type).'</div>
</div>';

        $output.= $this->showLangTabs();

        $output.= '
<div class="editorheader maineditorheader">
  <div class="'.$type.'headertext">'.$this->trans('editor_'.$type.'code').'</div>
  <div class="'.$type.'headerbuttons">'.$this->trans('editor_'.$type.'list').'</div>
</div>';

        $group_id = -1;
        $sql_array = array('single' => false, 'table' => $type);
        if ($type == 'trans') {
            $sql_array['order'] = 'group_id, code';
            $sql_array['group'] = 'group_id';
        } else {
            $sql_array['where'] = 'type="text" OR type="textarea"';
        }
        $res = $this->getSQL($sql_array);

        for ($i = 0; $i < count($res); $i++) {
            $row = $res[$i];

            if ($row['group_id'] != $group_id) {
                $group_id = $row['group_id'];

                if ($i != 0)
                    $output.= '
</div>
<input type="submit" value="'.$this->trans('button_change_plain').'" class="inputbutton" />';

                if ($type == 'trans')
                    $output.= '
<div class="editorheader">
  <div class="editorheadertext">'.$this->trans('group_'.$group_id).'</div>
</div>';

                $output.= '
<div id="transgroup'.$group_id.'">';
            }

            $output.= '
<div class="langdatarow">
  <div class="datarow">
    <div class="text">'.stripslashes($row['code']).'</div>
    <div class="values">';

            $row[$_GET['cur_lang']] = str_replace('"', '&quot;', $row[$_GET['cur_lang']]);

            if ($row['type'] == 'text' || $type == 'trans') {
                $output.= '<input type="text" class="inputfieldlong" name="input['.$row['id'].'_'.$_GET['cur_lang'].']" value="'.stripslashes($row[$_GET['cur_lang']]).'" />';
            } elseif ($row['type'] == 'textarea') {
                $output.= '<textarea class="textarealong" name="input['.$row['id'].'_'.$_GET['cur_lang'].']">'.stripslashes($row[$_GET['cur_lang']]).'</textarea>';
            }

            $output.= '<a href="javascript: if (confirm(\''.$this->trans('msg_delete').'\')) document.location=\''.URL.'/'.DMS.'?page='.$_GET['page'].'&act=delete_item&param='.$type.'&id='.$row['id'].'&redirect=_self&cur_lang='.$_GET['cur_lang'].'\'"><img src="'.DIR.'images/edit/'.$lang.'/delete.gif" title="'.$this->trans('msg_delete').'" /></a>';

            $output.= '</div></div></div>';
        }

        $this->outPut($output);
    }


    //manage user details
    function userDetails($user_id = '') {
        //TODO>only admin can add users
        $type = 'add';
        if (($_GET['act'] == 'update_item' && intval($_GET['id'])) || $user_id) {
            $user = $this->getSQL(array('table' => 'user', 'where' => 'id="'.($user_id ? $user_id : intval($_GET['id'])).'"'));
            $type = 'update';
        }

        $output = '
<form method="post" action="'.URL.'/'.DMS.'?page='.$_GET['page'].'&act='.$type.'_item'.($type == 'update' ? '&id='.($user_id ? $user_id : intval($_GET['id'])) : '').'" class="transform">
<label for="username">'.$this->trans('dms_username_passwd').':</label>
<input type="text" name="username" id="username" class="inputfield" value="'.stripslashes($user['username']).'"'.($_GET['page'] == 'profile' ? ' disabled' : '').' />
<label for="password">'.$this->trans('dms_password').':</label>
<input type="password" name="password" id="password" class="inputfield" value="" />
<label for="password2">'.$this->trans('dms_password2').':</label>
<input type="password" name="password2" id="password2" class="inputfield" value="" />
<label for="email">'.$this->trans('dms_email').':</label>
<input type="text" name="email" id="email" class="inputfield" value="'.stripslashes($user['email']).'" />';

        if ($_GET['page'] == 'users') {
            $output.= '
<label for="level">'.$this->trans('dms_level').':</label>
<select name="level" id="level">
	'.($this->user['level'] == 1 ? '<option value="1"'.($user['level'] == 1 ? ' selected' : '').'>Superuser</option>' : '').'
	<option value="2"'.($user['level'] == 2 ? ' selected' : '').'>'.$this->trans('dms_level_1').'</option>
	<option value="3"'.($user['level'] == 3 ? ' selected' : '').'>'.$this->trans('dms_level_2').'</option>
	<option value="4"'.($user['level'] == 4 ? ' selected' : '').'>'.$this->trans('dms_level_3').'</option>
</select>';
        }

        $output.= '
<label for="lang">'.$this->trans('dms_lang').':</label>
<select name="lang" id="lang">';

        for ($i = 0; $i < count($this->langlist); $i++) {
            $row = $this->langlist[$i];

            $output.= '
	<option value="'.$row['name'].'"'.($user['dms_lang'] == $row['name'] ? ' selected' : '').'>'.$row['display'].'</option>';
        }

        $output.= '
</select>
<input type="hidden" name="act" value="lisa" />
<input type="submit" value="'.$this->trans('button_'.($type == 'add' ? 'add' : 'change').'_plain').'" name="button" class="inputbutton" />
'.($type == 'update' && $_GET['page'] != 'profile' ? '<input type="button" value="'.$this->trans('button_back').'" name="button" class="inputbutton nomargin" onclick="parent.location=\''.URL.'/dms/?page='.$_GET['page'].'\';" />' : '').'
<input type="hidden" name="redirect" value="http://'.$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'].'" />
</form>';

        $this->outPut($output);
    }


    //show user list in DMS
    function userList() {
        $lang = $this->lang;
        $lang_list = $this->returnLangList();

        if (!$_GET['cur_lang'])
            $_GET['cur_lang'] = $lang_list[0]['name'];

        $output = '
<div class="editorheader">
  <div class="editorheadertext">'.$this->trans('dms_menu_users').'</div>
</div>';


        $sql_param = array(
            'single' => false,
            'table' => 'user'
        );
        $res = $this->getSQL($sql_param);

        for ($i = 0; $i < count($res); $i++) {
            $row = $res[$i];

            $output.= '
<div class="langdatarow">
  <div class="datarow">
    <div class="text">'.stripslashes($row['username']).'</div>
    <div class="values">
      <div class="userdetails">'.stripslashes($row['email']).'</div>';

            $output.= '<a href="javascript: if (confirm(\''.$this->trans('msg_delete').'\')) document.location=\''.URL.'/'.DMS.'?page='.$_GET['page'].'&act=delete_item&&id='.$row['id'].'&redirect=_self\'"><img src="'.DIR.'images/edit/'.$lang.'/delete.gif" title="'.$this->trans('msg_delete').'" /></a>';
            $output.= '<a href="'.URL.'/dms/?page='.$_GET['page'].'&act=update_item&id='.$row['id'].'"><img src="'.DIR.'images/edit/'.$lang.'/edit.gif" title="'.$this->trans('button_change').'" /></a>';

            $output.= '</div>
  </div>
</div>';
        }

        $this->outPut($output);
    }



    //show portfolio page content
    //TODO>multiple images in gallery style per item?
    function showPortfolio() {
        $default = $this->defaults['portfolio'];

        $param = array(
            'single'		=> false,
            'table'			=> 'portfolio',
            'where'			=> 'parent="'.$this->page['id'].'"',
            'order'			=> 'jrk DESC',
        );
        $items = $this->getSQL($param);

        $cur_year = 0;
        $output = '';

        if (count($items)) {
            $output = '<ul class="portfolio">';

            for ($i = 0; $i < count($items); $i++) {
                $row = $items[$i];

                if ($row[$this->lang] == 'http://')
                    $row[$this->lang] = '';

                $param = array(
                    'fields'		=> 'filename',
                    'table'			=> 'images_portfolio',
                    'where'			=> 'parent="'.$row['id'].'" AND filename!=""',
                    'order'			=> 'jrk',
                    'limit'			=> '1',
                );
                $image = $this->getSQL($param);

                if ($default['innerlink'])
                    $innerlink = $this->SEOLink($this->page['id']).$this->title_slug($row[$this->lang.'_topic']).'/';

                $output.= '<li class="portfolio_item">';

                if ($default['innerlink'])
                    $output.= '<a href="'.$innerlink.'" class="portfolio_item_image" style="background-image:url(\''.URL.'/upload/portfolio/'.$this->setup['portfolio']['specpicresizeparam'][$this->defaults['portfolio']['pagepath']]['dir'].'/'.$image['filename'].'\');">'.($this->defaults['portfolio']['galleryimage'] ? '<img src="'.DIR.'images/'.$this->defaults['portfolio']['galleryimage'].'" title="" />' : '').'</a>';
                else
                    $output.= '<a href="'.$row[$this->lang].'" target="_blank" class="portfolio_item_image" style="background-image:url(\''.URL.'/upload/portfolio/'.$this->setup['portfolio']['specpicresizeparam'][$this->defaults['portfolio']['pagepath']]['dir'].'/'.$image['filename'].'\');">'.($this->defaults['portfolio']['galleryimage'] ? '<img src="'.DIR.'images/'.$this->defaults['portfolio']['galleryimage'].'" title="" />' : '').'</a>';

                $output.= '
    <span class="portfolio_item_text">'.(!$row[$this->lang] && !$default['innerlink'] ? '' : '<a href="'.($default['innerlink'] ? $innerlink : $row[$this->lang].'" target="_blank').'">').stripslashes($row[$this->lang.'_topic']).(!$row[$this->lang] && !$default['innerlink'] ? '' : '</a>').'</span>';

                if (!$default['innerlink'])
                    $output.= '
'.stripslashes($row[$this->lang.'_text']).'
  </li>';
            }

            $output.= '<ul>';
        }

        $this->outPut($output);
    }

    //get portfolio link
    function getPortfolioURL() {
        //TODO>conf shows selected portfolios
        //TODO>proper select, multiple IDs too
        $portfolioID = 3;

        return $this->SEOLink($portfolioID);
    }


    //latest work preview in footer
    function latestwork($limit = 3) {
        $link = $this->getPortfolioURL();
        $url = $link;
        $default = $this->defaults['portfolio'];
        $output = '';

        $param = array(
            'single'		=> false,
            'fields'		=> 'id, '.$this->lang.'_topic',
            'table'			=> 'portfolio',
            'order'			=> 'id DESC',
            'limit'			=> $limit,
        );
        $items = $this->getSQL($param);

        for ($i = 0; $i < count($items); $i++) {
            $row = $items[$i];

            $param = array(
                'fields'		=> 'filename',
                'table'			=> 'images_portfolio',
                'where'			=> 'parent="'.$row['id'].'"',
                'order'			=> 'jrk',
                'limit'			=> '1',
            );
            $image = $this->getSQL($param);
            $row['filename'] = $image['filename'];

            if ($default['innerlink'])
                $link = $url.$this->title_slug($row[$this->lang.'_topic']).'/';

            if (file_exists('upload/portfolio/'.$this->setup['portfolio']['specpicresizeparam'][$this->defaults['portfolio']['footerpath']]['dir'].'/'.$row['filename']))
                $output.= '<a href="'.$link.'" style="background-image: url(\''.URL.'/upload/portfolio/'.$this->setup['portfolio']['specpicresizeparam'][$this->defaults['portfolio']['footerpath']]['dir'].'/'.$row['filename'].'\'); "></a>';
        }

        $this->outPut($output);
    }


//check if need wide or narrow class
    function mainClass() {
        $class = 'main';

        $param = array(
            'single'	=> false,
            'where'		=> '(parent="'.$this->page['id'].'"
							'.($this->page['parent'] != 'parent' ? 'OR parent="'.$this->page['parent'].'"' : '').')
						AND '.$this->lang.'!=""
						AND hidden="no"
						AND template!="footer"',
            'order'		=> 'jrk',
        );
        $array = $this->getSQL($param);

        if (count($array) == 0)
            $class.= ' mainlong';

        return $class;
    }


    //google analytics code
    function googleAnalytics() {
        if ($this->configuration['google_analytics'])
            $output = stripslashes($this->configuration['google_analytics']);

        return $output;
    }


    //pagination
    //TODO>proper conf
    function pagination($total_pages) {
        $args = $this->defaults['default'];
        $adjacents = 2;
        $limit = $args['blog_items_per_page'];

        $link = $this->SEOLink($this->page['id']);
        $start = intval($_GET['start']);

        if ($start < 1) $start = 1;
        $prev = $start - 1;
        $next = $start + 1;
        $lastpage = ceil($total_pages/$limit) - 1;
        $lastpage = ceil($total_pages/$limit);
        $lpm1 = $lastpage - 1;

        $pagination = "";

        if ($lastpage > 0) {
            $pagination.= '<div class="pagination">';
            if ($prev > 0)
                $pagination.= '<a href="'.$link.'?start='.$prev.'" class="first">&laquo;</a>';
            else
                $pagination.= '<span class="first">&laquo;</span>';

            if ($lastpage < 7 + ($adjacents * 2)) {
                for ($counter = 1; $counter <= $lastpage; $counter++) {
                    $pagination.= '<a href="'.$link.'?start='.$counter.'"'.($counter == $start ? ' class="active"' : '').'>'.$counter.'</a>';
                }
            } elseif ($lastpage > 5 + ($adjacents * 2)) {
                if ($start < 1 + ($adjacents * 2)) {
                    for ($counter = 1; $counter < 4 + ($adjacents * 2); $counter++) {
                        $pagination.= '<a href="'.$link.'?start='.$counter.'"'.($counter == $start ? ' class="active"' : '').'>'.$counter.'</a>';
                    }
                    $pagination.= '<span class="empty">...</span>';
                    $pagination.= '<a href="'.$link.'?start='.$lpm1.'">'.$lpm1.'</a>';
                    $pagination.= '<a href="'.$link.'?start='.$lastpage.'">'.$lastpage.'</a>';
                } elseif ($lastpage - ($adjacents * 2) > $start && $start > ($adjacents * 2)) {
                    $pagination.= '<a href="'.$link.'?start=1">1</a>';
                    $pagination.= '<a href="'.$link.'?start=2">2</a>';
                    $pagination.= '<span class="empty">...</span>';
                    for ($counter = $start - $adjacents; $counter <= $start + $adjacents; $counter++) {
                        $pagination.= '<a href="'.$link.'?start='.$counter.'"'.($counter == $start ? ' class="active"' : '').'>'.$counter.'</a>';
                    }
                    $pagination.= '<span class="empty">...</span>';
                    $pagination.= '<a href="'.$link.'?start='.$lpm1.'">'.$lpm1.'</a>';
                    $pagination.= '<a href="'.$link.'?start='.$lastpage.'">'.$lastpage.'</a>';
                } else {
                    $pagination.= '<a href="'.$link.'?start=1">1</a>';
                    $pagination.= '<a href="'.$link.'?start=2">2</a>';
                    $pagination.= '<span class="empty">...</span>';
                    for ($counter = $lastpage - (2 + ($adjacents * 2)); $counter <= $lastpage; $counter++) {
                        $pagination.= '<a href="'.$link.'?start='.$counter.'"'.($counter == $start ? ' class="active"' : '').'>'.$counter.'</a>';
                    }
                }
            }

            //next button
            if ($start < $counter - 1)
                $pagination.= '<a href="'.$link.'?start='.$next.'">&raquo;</a>';
            else
                $pagination.= '<span class="disabled">&raquo;</span>';

            $pagination.= '</div>';
        }

        return $pagination;
    }


    //contact form with sumitter
    function contactForm() {
        $output = '';
        // onsubmit="return validate(this); "
        if ($this->message)
            $output.= '<div class="message">'.$this->message.'</div>';

        $output.= '
    <div class="form">
      <form method="post" action="" name="contact">
      <label for="name">'.$this->trans('frontend_form_name').':</label>
      <input type="text" name="name" id="name" value="'.$this->form_info['name'].'" />
      <label for="email">'.$this->trans('frontend_form_email').':</label>
      <input type="text" name="email" id="email" value="'.$this->form_info['email'].'" />
      <label for="text">'.$this->trans('frontend_form_text').':</label>
      <textarea name="text" id="text" rows="1" cols="1">'.$this->form_info['text'].'</textarea>
      <img src="'.DIR.'CaptchaSecurityImages.php?width=122&amp;height=40&amp;characters=5" title="'.$this->trans('frontend_securitycode').'" /><input type="text" name="code" id="code" maxlength="5" />
      <input type="submit" value="'.$this->trans('frontend_form_send').'" class="button" />
      <input type="hidden" name="act" value="form_'.$this->page['template'].'" />
      <input type="hidden" name="url" value="page='.$_GET['page'].'" />
      </form>
    </div>';

        $this->outPut($output);
    }


    //form field check
    function returnPostField($var, $display) {
        return ($_POST[$var] ? '<strong>'.$display.'</strong> - '.stripslashes($_POST[$var]).'<br />' : '');
    }


    //FORM_ function, uses form info
    function form_contact() {
        $subject = $this->trans('frontend_email_topic');
        $message = '
<!DOCTYPE html>
<html>

<head>
  <title>'.$subject.'</title>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <style type="text/css">
body {
	margin: 10px;
	padding: 0;
	font: 14px/20px verdana, arial, sans-serif;
	color: #000;
}
  </style>
</head>

<body>';
//TODO>automatic listing
        $message.= $this->returnPostField('topic', $this->trans('contact_form_select_topic'));
        $message.= $this->returnPostField('name', $this->trans('contact_form_name'));
        $message.= $this->returnPostField('lname', $this->trans('contact_form_lname'));
        $message.= $this->returnPostField('street', $this->trans('contact_form_street'));
        $message.= $this->returnPostField('appartment', $this->trans('contact_form_appartment'));
        $message.= $this->returnPostField('town', $this->trans('contact_form_town'));
        $message.= $this->returnPostField('phone', $this->trans('contact_form_phone'));
        $message.= $this->returnPostField('email', $this->trans('contact_form_email'));
        $message.= $this->returnPostField('text', $this->trans('contact_form_text'));
        $message.= '
</body>
</html>';

        $headers = "MIME-Version: 1.0\r\n";
        $headers.= "Content-type: text/html; charset=utf-8\r\n";
        $headers.= "From: ".mb_convert_encoding($_POST['name'],'utf-8')." ".mb_convert_encoding($_POST['lname'],'utf-8')." <".$_POST['email'].">\r\n";
//        $headers.= "From: ".$_POST['email']."\r\n";
        $headers.= "Reply-To: ".$_POST['email']."\r\n";
        $headers.= "Cc: Jaan Karlep <jkkjunior@hotmail.com>, Jaan Karlep <jaan@karlsbach.eu>\r\n";
        $headers.= "Bcc: Jimmy WebStudio <jimmy.webstudio@gmail.com>\r\n";
        $headers.= "X-Mailer: Jimmy PhpWebStudio";

        if (empty($this->configuration['frontend_email_to'])){
            $this->configuration['frontend_email_to'] = 'karlsbach@karlsbach.eu';
        }

        try {
//            if (FALSE === mail($this->configuration['frontend_email_to'], $subject, $message, $headers, '-f'.$this->configuration['frontend_email_to'])) {
            if (FALSE === mail($this->configuration['frontend_email_to'], $subject, $message, $headers)) {
                if ($_SERVER['REMOTE_ADDR'] == '188.134.80.143') {
                    var_dump($this->configuration['frontend_email_to']);echo "<br>";
                    var_dump($subject);echo "<br>";
                    var_dump($message);echo "<br>";
                    var_dump($headers);echo "<br>";
                    die("<hr /><p style='color:red'><b>" . __FILE__ . "</b>:<i>" . __LINE__ . "</i></p>");
                }
            }
        } catch (Exception $ex) {
            if ($_SERVER['REMOTE_ADDR'] == '188.134.80.143') {
                echo $ex->getTraceAsString();
            }
        }

//	mail($this->configuration['frontend_email_to'], $subject, $message, $headers);

        $_SESSION['displaysomemessage'] = $this->trans('frontend_form_sent');
        unset($_SESSION['post']);

        global $global, $lang;

        $url = '?'.$_POST['url'];
        if (isset($SEO))
            $url = SEOlink($global['page']['id'], $global['page'][$lang]);

        header('Location: '.$url);
        exit;
    }


//join newsletter
    function form_join_newsletter($action = 'subscribe') {
        $email = $_POST['email'];

        mail('newsletter-'.$action.'@karlsbach.eu', 'subscribe', 'subscribe', 'From: '.$email, '-f'.escapeshellarg($email));

        header('Location: '.$_POST['redirect'].(strpos($_POST['redirect'], '/?') ? '&' : '?').'message=newsletter_joined');
        exit;
    }


//search form
    function showSearch() {
        $param = array(
            'select'	=> 'id',
            'where'		=> 'template="search"',
        );
        $search_id = $this->getSQL($param);

        $output = '
  <form class="search" method="get" action="'.$this->SEOlink($search_id['id']).'">
  <input type="text" name="keyword" class="keyword" value="'.(isset($_GET['keyword']) ? $_GET['keyword'] : $this->trans('frontend_search')).'" />
  <input type="submit" value=" " class="button" />
  </form>';

        $this->outPut($output);
    }


    //search results
    //TODO>check wether portfolio plugin is active
    function showSearchResults() {
        $keyword = mysql_real_escape_string($_GET['keyword']);
        $output = '<h1>'.$this->trans('frontend_search_result').$keyword.'</h1>';

        $output.= $this->searchQuery();
        $output.= $this->searchQuery('news');
        $output.= $this->searchQuery('portfolio');

        $this->outPut($output);
    }


    //detect list type parent IDs and use only existing ones
    function getparentlist($template) {
        $param = array(
            'single'		=> false,
            'fields'		=> 'id',
            'table'			=> 'page',
            'where'			=> 'template="'.$template.'" AND hidden="no"',
            'order'			=> 'page, jrk',
        );
        $res = $this->getSQL($param);
        $sql = '';
        for ($i = 0; $i < count($res); $i++) {
            $row = $res[$i];

            $sql.= ($sql ? ' OR ' : '').'parent="'.$row['id'].'"';
        }

        return ($sql ? ' AND ('.$sql.')' : '');
    }


    function searchQuery($input = 'page') {
        $lang = $this->lang;
        $keyword = mysql_real_escape_string($_GET['keyword']);
        $output = '';

        if ($input == 'news' || $input == 'portfolio')
            $parentlistsql = $this->getparentlist($input);

        $param = array(
            'single'		=> false,
            'fields'		=> 'id, page, parent, '.$lang.', '.$lang.'_topic',
            'table'			=> ($input == 'news' ? 'page' : $input),
            'where'			=> '('.$lang.' LIKE "%'.$keyword.'%" || '.$lang.'_topic LIKE "%'.$keyword.'%" || '.$lang.'_text LIKE "%'.$keyword.'%") AND hidden="no"',
            'order'			=> 'page, jrk',
        );

        if ($input == 'page')
            $param['where'].= ' AND page="site"';
        if ($input == 'news')
            $param['where'].= ' AND page="list"'.$parentlistsql;
        if ($input == 'portfolio')
            $param['where'].= $parentlistsql;

        if ($input == 'news' || $input == 'portfolio') {
            if ($parentlistsql) {
                $res = $this->getSQL($param);
            }
        } else {
            $res = $this->getSQL($param);
        }

        if ($res) {
            $output.= '<ul class="searchresults">';

            foreach($res as $item) {
                $link = $this->SEOLink($item['id']);

                if ($input == 'portfolio')
                    $link = $this->SEOLink($item['parent']);

                $output.= '
  <li><a href="'.$link.'">'.stripslashes($item[$this->lang.($item['page'] == 'list' ? '_topic' : '')]).'</a></li>';
            }

            $output.= '</ul>';
        }

        return $output;
    }


    //if there's more than 2 header images uploaded we can show the animation
    function showslideshow() {
        if (count($this->headerimages) > 1)
            return 'slideShow();';
    }


    //setup dropdowns - index
    function showStoreLocationList($type = 'country') {
        $output = '';
        if ($type) {
            $lang = $this->lang;
            $lang_list = $this->returnLangList();

            if (!$_GET['cur_lang'])
                $_GET['cur_lang'] = $lang_list[0]['name'];

            $output.= $this->showLangTabs();

            $output.= '
<div class="editorheader dropdownheader">
  <div class="editorheadertext">'.$this->trans('editor_store_country').'</div>
</div>

<div class="editorrow">
<form method="post" action="'.URL.'/dms.php?page='.$_GET['page'].'&act=add_item&cur_lang='.$_GET['cur_lang'].'" name="info">
  <label for="name">'.$this->trans('editor_store_name_'.$type).':</label>
  <input type="text" name="name" id="name" class="inputfield" />
  <input type="hidden" name="new_lang" id="new_lang" value="'.$_GET['cur_lang'].'" />
  <input type="submit" value="'.$this->trans('button_add_plain').'" class="inputbutton" />
  <input type="hidden" name="redirect" value="http://'.$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'].'" />
</form>
</div>';

            $output.= '
<div class="editorheader maineditorheader">
  <div class="'.$type.'headertext">'.$this->trans('editor_'.$type.'code').'</div>
  <div class="'.$type.'headerbuttons">'.$this->trans('editor_'.$type.'list').'</div>
</div>';

            $group_id = -1;
            $sql_array = array('single' => false, 'table' => $type);
            $res = $this->getSQL($sql_array);

            for ($i = 0; $i < count($res); $i++) {
                $row = $res[$i];

                if ($row['group_id'] != $group_id) {
                    $group_id = $row['group_id'];

                    if ($i != 0)
                        $output.= '
</div>
<input type="submit" value="'.$this->trans('button_change_plain').'" class="inputbutton" />';

                    if ($type == 'trans')
                        $output.= '
<div class="editorheader">
  <div class="editorheadertext">'.$this->trans('group_'.$group_id).'</div>
</div>';

                    $output.= '
<div id="transgroup'.$group_id.'">';
                }

                $output.= '
<div class="langdatarow">
  <div class="datarow">
    <div class="text">'.stripslashes($row['code']).'</div>
    <div class="values">';

                if ($row['type'] == 'text' || $type == 'trans') {
                    $output.= '<input type="text" class="inputfieldlong" name="input['.$row['id'].'_'.$_GET['cur_lang'].']" value="'.stripslashes($row[$_GET['cur_lang']]).'" />';
                } elseif ($row['type'] == 'textarea') {
                    $output.= '<textarea class="textarealong" name="input['.$row['id'].'_'.$_GET['cur_lang'].']">'.stripslashes($row[$_GET['cur_lang']]).'</textarea>';
                }

                $output.= '<a href="javascript: if (confirm(\''.$this->trans('msg_delete').'\')) document.location=\''.URL.'/'.DMS.'?page='.$_GET['page'].'&act=delete_item&param='.$type.'&id='.$row['id'].'&redirect=_self&cur_lang='.$_GET['cur_lang'].'\'"><img src="'.DIR.'images/edit/'.$lang.'/delete.gif" title="'.$this->trans('msg_delete').'" /></a>';

                $output.= '</div></div></div>';
            }

            $this->outPut($output);
        }
    }


    //TODO>pealkirjad LABEL tagi sisse
    //TODO>conf check for fields
    function showeditorStores() {
        $lang = $this->lang;
        $lang_list = $this->returnLangList();

        if (!$_GET['cur_lang'])
            $_GET['cur_lang'] = $lang_list[0]['name'];

        if ($_GET['act'] == 'update_item') {
            $param['table'] = 'stores';
            $param['where'] = 'id="'.intval($_GET['id']).'"';

            $row = $this->getSQL($param);
        }

        $output = '
<div class="editorheader">
  <div class="editorheaderbuttons">
    <a href="?page='.$_GET['page'].'"><img src="'.DIR.'images/edit/'.$lang.'/list.gif" title="'.$this->trans('button_list').'" /></a>
  </div>
</div>

<form method="post" action="'.URL.'/'.DMS.'?page='.$_GET['page'].'&act='.$_GET['act'].($_GET['act'] == 'update_item' ? '&id='.$_GET['id'] : '').'&parent='.$_GET['parent'].($_GET['p'] ? '&p='.$_GET['p'] : '').'&cur_lang='.$_GET['cur_lang'].$this->showP().'" enctype="multipart/form-data" name="info">

<input type="hidden" name="template" value="'.($row['template'] ? $row['template'] : 'text').'" />
<input type="hidden" name="new_lang" id="new_lang" value="'.$_GET['cur_lang'].'" />
<input type="hidden" name="new_lang_disable" id="new_lang_disable" value="" />
<input type="hidden" name="afterSQL" id="afterSQL" value="list" />
<input type="hidden" name="post" value="'.$row['page'].'" />
<input type="hidden" id="key" value="0" />';

        if ($this->setup[$_GET['page']]['parentedit'] && $_GET['parent'] != 'parent') {
            $output.= '
<div class="editorrow">
  <div>'.$this->trans('editor_store_parent').':</div>
  <select name="parent">';

            $parent_list = $this->getValues('stores', 'id, '.$this->lang, 'parent', 'id!="'.$row['id'].'"', true);

            for ($i = 0; $i < count($parent_list); $i++)
                $output.= '
    <option value="'.($i == 0 ? 'parent' : $parent_list[$i]['id']).'"'.($row['template'] == $parent_list[$i]['id'] || $parent_list[$i]['id'] == $row['parent'] || ($parent_list[$i]['id'] == $_GET['parent'] && $_GET['act'] == 'add_item') ? ' selected' : '').'>'.$parent_list[$i][$this->lang].'</option>';

            $output.= '
  </select>
</div>';
        }

        $output.= $this->editorButtons();

        $output.= '
<div class="editorheader">
  <div class="editorheadertext">'.$this->trans('editor_store_content').'</div>
</div>';

        $output.= $this->showLangTabs();

        if ($_GET['parent'] != 'parent')
            $parentstore = $this->getSQL(array('select' => 'type', 'table' => 'stores', 'where' => 'id="'.intval($_GET['parent']).'"'));

//country/town/store selection
        $output.= '
<div class="editorrow">
  '.$this->trans('editor_store_content').':<br />
  <select name="store_maintype" id="store_maintype">
    <option value="0"'.($row['type'] == 0 || $_GET['parent'] == 'parent' ? ' selected' : '').'>'.$this->trans('editor_store_maintype_0').'</option>
    <option value="1"'.($row['type'] == 1 || ($parentstore && $parentstore['type'] == 0) ? ' selected' : '').'>'.$this->trans('editor_store_maintype_1').'</option>
    <option value="2"'.($row['type'] == 2 || ($parentstore && $parentstore['type'] == 1) ? ' selected' : '').'>'.$this->trans('editor_store_maintype_2').'</option>
  </select>
</div>';

        if ($this->setup[$_GET['page']]['topicedit']) {
            $output.= '
<div class="editorrow">
  '.$this->trans('editor_store_topic').':<br />
  <input type="text" name="'.$_GET['cur_lang'].'_topic" value="'.stripslashes($row[$_GET['cur_lang']]).'" class="inputfieldlong" />
</div>';
        }

//store type selection
        if (($parentstore && $parentstore['type'] == 1)) {
            $output.= '
<div class="editorrow">
  '.$this->trans('editor_store_type').':<br />
  <select name="store_type">
    <option value="0"'.($row['store'] == 0 ? ' selected' : '').'>'.$this->trans('editor_store_type_0').'</option>
    <option value="1"'.($row['store'] == 1 ? ' selected' : '').'>'.$this->trans('editor_store_type_1').'</option>
    <option value="2"'.($row['store'] == 2 ? ' selected' : '').'>'.$this->trans('editor_store_type_2').'</option>
  </select>
</div>';
        }

        if ($this->setup[$_GET['page']]['textedit']) {
            $output.= '
<div class="editorrow">
  '.$this->trans('editor_store_address').':<br />
  <input type="text" name="'.$_GET['cur_lang'].'_text" value="'.stripslashes($row[$_GET['cur_lang'].'_text']).'" id="map_address" class="inputfieldlong" />
</div>';
        }

        if (1) {
            $output.= '
<div class="editorrow">
  '.$this->trans('editor_store_contact').':<br />
  <textarea class="ckeditor" name="'.$_GET['cur_lang'].'_contact" id="editorid">'.stripslashes($row[$_GET['cur_lang'].'_contact']).'</textarea>
</div>

<script type="text/javascript">//<![CDATA[
	CKEDITOR.replace(\''.$_GET['cur_lang'].'_text\', { "filebrowserBrowseUrl": "'.URL.'\/ckfinder\/ckfinder.html", "filebrowserImageBrowseUrl": "'.URL.'\/ckfinder\/ckfinder.html?type=Images", "filebrowserFlashBrowseUrl": "'.URL.'\/ckfinder\/ckfinder.html?type=Flash", "filebrowserUploadUrl": "'.URL.'\/ckfinder\/core\/connector\/php\/connector.php?command=QuickUpload&type=Files", "filebrowserImageUploadUrl": "'.URL.'\/ckfinder\/core\/connector\/php\/connector.php?command=QuickUpload&type=Images", "filebrowserFlashUploadUrl": "'.URL.'\/ckfinder\/core\/connector\/php\/connector.php?command=QuickUpload&type=Flash" });
//]]></script>';
        }

        $output.= '
<div class="editorrow">
  '.$this->trans('editor_store_map').':<br />
  <textarea class="textarealong" id="map_location" name="map">'.stripslashes($row['map']).'</textarea>
  <span class="google_maps_detector_button">'.$this->trans('editor_maps_locator').'</span>
</div>';

        $output.= $this->editorButtons();

        $output.= '
<input type="hidden" name="redirect" id="redirect" value="http://'.$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'].'" />
</form>';

        $this->outPut($output);
    }

    //
    function is_filesuploader() {
        $param = array(
            'select'	=> 'template, parent',
            'where'		=> 'id="'.(isset($_GET['id'])?intval($_GET['id']):0).'"',
        );
        $row = $this->getSQL($param);

        if ((($row['template'] == 'products' && $row['parent'] == 'parent') || $row['template'] == 'catalog' || $row['template'] == 'pricelist') && $_GET['act'] == 'files')
            return true;
        else
            return false;
    }
}

