<?php
class specific extends DMS {

    /*function __construct() {
    }*/

    /**
     * language dropdown selecion
     */
    function language() {
        $args = $this->defaults['lang'];
        $list = $this->langlist;

        $output = '';

        if (count($list) > 1) {
            $output.= '
            <img src="'.DIR.'images/langglobe.gif" title="" class="langglobe" /> '.$this->trans('frontend_language').' <img src="'.DIR.'images/langmainarrow.png" title="" class="langmainarrow" />';

            $output.= '
            <ul'.($args['mainclass'] ? ' class="'.$args['mainclass'].'"' : '').'>';

            for ($i = 0; $i < count($list); $i++) {
                $row = $list[$i];

                $url = stripslashes($row['display']);

                $class = ' class="';
                if ($i == 0)
                    $class.= ' first';
                if ($i == count($list) - 1)
                    $class.= ' last';
                if ($this->lang == $row['name'])
                    $class.= ' active';
                $class.= '"';

                $output.= '
                <li'.$class.'><a href="'.$this->SEOlink($this->page['id'], $row['name']).'"'.$class.'>';

                $output.= ucwords(stripslashes($row['display']));

                $output.= '</a></li>';
            }

            $output.= '</ul>';
        }

        $this->outPut($output);
    }

    /**
     * footer newsletter joiner
     */
    function newsletterjoin() {
        $output = '
<div class="newsletterjoin">
<a href="http://eepurl.com/S-eP5" target=_blank><span>'.$this->trans('frontend_newsletter_h1').'</span></a>
<form method="post" action="">
  <input type="text" name="email" class="email" value="'.$this->trans('frontend_newsletter_email').'" />
  <input type="submit" value=" " class="button" />
  <input type="hidden" name="act" value="form_join_newsletter" />
  <input type="hidden" name="redirect" value="http://'.$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'].'" />
</form>
</div>';

        $this->outPut($output);
    }

    /**
     * footer social media icons
     */
    function icons() {
        $param = array('single' => false, 'table' => 'social_icons');
        $icons = $this->getSQL($param);

        $output = '<div class="social_icons">';

        foreach ($icons as $icon) {
            $icon['url'] = $this->configuration['icon_'.$icon['code'].'_url'];

            if (in_array($icon['code'], ['twitter','gmail', 'youtube'])) {
                continue;
            }

            $output.= ($icon['url'] ? '<a href="'.$icon['url'].'" target="_blank">' : '').'<div class="'.$icon['code'].'"></div>'.($icon['url'] ? '</a>' : '');
        }
        // + Instagram
        $output.= '<a href="https://instagram.com/karlsbach.eu/" target="_blank"><div class="instagram"></div></a>';

        $output.= '</div>';

        $this->outPut($output);
    }

    /**
     * index page animations
     */
    function indexanimation() {
        global $specific;
        $output = '<div class="index_animations">';

        $param = array(
            'single'		=> false,
            'table'			=> 'images',
            'where'			=> 'parent="'.$this->page['id'].'"',
            'order'			=> 'jrk',
        );
        $images = $this->getSQL($param);

        if (count($images)) {
            $output.= '<div class="slider-wrapper theme-default"><div id="slider" class="nivoSlider">';

            for ($i = 0; $i < count($images); $i++) {
                $row = $images[$i];
                $max = $row['path'];
                $dir = $row['path'].'';

                if ($row['filename'] != '' and file_exists($dir.$row['filename']))
                    $output.= '
  '.($row[$this->lang] ? '<a href="'.stripslashes($row[$this->lang]).'">' : '').'<img src="'.URL.'/'.$dir.'_orig/'.$row['filename'].'" title="" />'.($row[$this->lang] ? '</a>' : '');
            }

            $output.= '</div></div>';
        }

        $output.= '</div>';

        $output.= $specific->indexanimationbottom($images);

        if (isset($_GET['message']) && $_GET['message'] == 'newsletter_joined') {
            $link = 'http://'.$_SERVER['SERVER_NAME'].substr($_SERVER['REQUEST_URI'], 0, strpos($_SERVER['REQUEST_URI'], '?')).($this->page['template'] == 'search' ? '?keyword='.$_GET['keyword'] : '');

            $output.= '
	<div class="overlay_message"></div>
	<div class="message_close">'.$this->trans('newsletter_joined').'<img src="'.DIR.'images/close.png" title="close" /></div>
	<input type="hidden" name="message_redirect" id="message_redirect" value="'.$link.'" />';
        }

        $this->outPut($output);
    }

    function indexanimationbottom($images) {
        $output = '<div class="index_animations_bottom">';
        /*
          <div class="jcarousel-control">';
            for ($i = 0; $i < count($images); $i++)
                $output.= '
            <a href="#">'.($i + 1).'</a>';
            $output.= '
          </div>
        */
        $output.= '</div>';

        return $output;
    }

    /**
     * background image for "about" pages
     */
    function background_image() {
        $param = array(
            'table'		=> 'images',
            'where'		=> 'parent="'.$this->page['id'].'"',
            'order'		=> 'jrk',
            'limit'		=> 1,
        );
        $image = $this->getSQL($param);

        $output = ' style="';

        if ($image['filename'] && file_exists($image['path'].'_orig/'.$image['filename']))
            $output.= 'background-image: url(\''.URL.'/'.$image['path'].'_orig/'.$image['filename'].'\');';

        $output.= '"';

        $this->outPut($output);
    }

    /**
     * displayes submenu
     * @param string $add_class
     */
    function submenu($add_class='') {
        global $specific;
        $showsubmenu = array('text', 'services', 'catalog', 'pricelist');

        if (in_array($this->page['template'], $showsubmenu)) {
            $param = array(
                'parent'	=> 'sub',
                'sqlparent'	=> ($this->page['parent'] != 'parent' ? $this->page['parent'] : $this->page['id']),
            );
            if ($this->page['template'] == 'gallery')
                $param['exeption'] = 'gallery';
            $submenu = $specific->display_menu($param);

            $topic = $this->page[$this->lang];

            if ($this->parentpage)
                $topic = $this->parentpage[$this->lang];

            $output = '
<div class="submenu '.$this->page['template'].'_submenu'.($add_class ? ' '.$add_class : '').'">';

            $output.= '
<h1>'.$topic.'</h1>
'.$submenu.'
</div>';

            if ($submenu)
                $this->outPut($output);
        }

//submenu for gallery, must always display 2 latest #2 level gallery links
        if ($this->page['template'] == 'gallery')
            $this->outPut($specific->submenuGallery());
    }

    /**
     * gallery submenu
     * @return string
     */
    function submenuGallery() {
        global $specific;
        $param = array(
            'parent'	=> 'sub',
            'sqlparent'	=> $this->page['parent'],
            'exeption'	=> 'gallery',
            'sub'		=> true,
        );
        $submenu = $specific->display_menu($param);

        $topic = $this->page[$this->lang.'_topic'];

        if ($this->parentpage)
            $topic = $this->parentpage[$this->lang.'_topic'];

        $submenu = str_replace('</li>
  <li', '</li>
  <li><span class="submenutopic">'.$topic.'</span></li>
  <li', $submenu);

        $output = '
<div class="submenu submenuGallery">
'.$submenu.'
</div>';

        return $output;
    }

    /**
     * modified menu display
     * @param $attr
     * @return string
     */
    function display_menu($attr) {
        global $specific;
        $args = $this->parameters($attr, 'menu');

        $output = '';

        if ($this->parentpage['parent'] && $args['location'] == 'main' && $this->parentpage['parent'] != 'parent' && $args['parent'] != 'main'){
            $args['sqlparent'] = $this->parentpage['parent'];
        }

        if ($this->page['template'] == 'catalog') {
            if ($args['parent'] == 'sub' && $this->page['parent'] != 'parent')
                $args['sqlparent'] = $this->page['parent'];
        }

        $param = array(
            'single'	=> false,
            'where'		=> 'parent="'.$args['sqlparent'].'"
						AND '.$this->lang.'!=""
						AND hidden="no"
						AND page="site"
						AND template!="footer"
						AND menu="'.$args['location'].'"',
            'order'		=> 'jrk',
        );

        if (isset($attr['exeption']) && ($attr['exeption'] == 'gallery' || $attr['exeption'] == 'expo')) {
            $param['limit'] = 2;
            $param['order'] = 'jrk DESC';
        }

        $array = $this->getSQL($param);
        $n = count($array);
        if (count($array)) {
            if ($args['style'] == 'table')
                $output.= '
<table class="'.$args['parent'].'menu" cellpadding="0" cellspacing="0">
<tr>';
            else
                $output.= '
<ul class="'.$args['parent'].'menu">';

            for ($i = 0; $i < $n; $i++) {
                $row = $array[$i];
                $class = '';

                $parenttree = explode('/', $this->getParentTree($this->page['id'], $this->lang, '', true));

                if ($this->page['id'] == $row['id'] || in_array($row['id'], $parenttree))
                    $class = 'active';

                if ($n == 1) {
                    $class.= ($class ? ' ' : '').'single';
                } else {
                    if ($i == 0)
                        $class.= ($class ? ' ' : '').'first';
                    if ($i == $n - 1)
                        $class.= ($class ? ' ' : '').'last';
                }

                if (isset($class))
                    $class = ' class="'.$class.'"';
                else
                    $class = '';

                if ($args['style'] == 'table')
                    $output.= '<td'.$class.' id="mainmenu'.$row['id'].'" nowrap>';
                else
                    $output.= '<li'.$class.'>';

                // modify gallery link to direct it to first gallery automatically
                if ($row['template'] == 'gallery' && !isset($attr['sub'])) {
                    $param = array(
                        'select'	=> 'id',
                        'where'		=> 'parent="'.$row['id'].'"',
                        'order'		=> 'jrk DESC',
                        'limit'		=> '1',
                    );
                    $gallery = $this->getSQL($param);

                    $row[$this->lang.'_redirect_url'] = $this->SEOlink($gallery['id']);
                }

                //link
                $link = ($row[$this->lang.'_redirect_url'] && $row[$this->lang.'_redirect_url'] != 'http://' ? $row[$this->lang.'_redirect_url'] : $this->SEOlink($row['id']));
                $output.= '<a'.$class.' href="'.$link.'">'.$args['add_before'].stripslashes($row[$this->lang]).$args['add_after'].($args['style'] == 'table' ? '' : ($row['filename'] ? '<input type="hidden" class="themeimage" value="'.$row['filename'].'" />' : '')).'</a>';

                //TODO>limited to 1 sublevel now
                if ($args['sqlparent'] == 'parent') {
                    $param = array(
                        'parent' => 'hoversub',
                        'style' => 'ul',
                        'sqlparent' => $row['id'],
                    );

                    if ($row['template'] == 'gallery') {
                        $param['exeption'] = $row['template'];
                        $param['sub'] = true;
                    }

                    if ($row['template'] != 'expo')
                        $output.= '<div class="hoversubmenu_container">'.$specific->display_menu($param).'</div>';
                }

//link end
                if ($args['style'] == 'table')
                    $output.= '
  </td>';
                else
                    $output.= '</li>';
            }

            if ($args['style'] == 'table') {
                $output.= '
</tr>
</table>';
            } else {
                $output.= '
</ul>';

                $output.= '<ul class="'.$args['parent'].'menu addimage"><img src="'.URL.'/images/submenu.jpg" title="" /></ul>';
            }
        }

        return $output;
    }

    /**
     * print out main menu IDs for hover menu selection
     * @return array|bool|QueryResult
     */
    function mainmenuids() {
        $param = array(
            'single'	=> false,
            'select'	=> 'id',
            'where'		=> 'parent="parent"
						AND '.$this->lang.'!=""
						AND hidden="no"
						AND page="site"
						AND template!="footer"
						AND menu="main"',
            'order'		=> 'jrk',
        );
        $array = $this->getSQL($param);

        return $array;
    }

    /**
     *
     */
    public function hoversubmenu_js() {
        global $specific;
        $array = $specific->mainmenuids();

        $output = '';

        for ($i = 0; $i < count($array); $i++) {
            $output.= '
$(\'#mainmenu'.$array[$i]['id'].'\').mouseover(function() {
    $(\'#mainmenu'.$array[$i]['id'].' div\').show();
});
$(\'#mainmenu'.$array[$i]['id'].'\').mouseout(function() {
    $(\'#mainmenu'.$array[$i]['id'].' div\').hide();
});
';

        }

        $output.= '
$(".mainmenu .hoversubmenu a").mouseover(function() {
	var image = ($(this).find("input").val());

    $(".mainmenu .addimage img").hide();

	if (image) {
		$(".mainmenu .addimage img").attr("src", "'.URL.'/upload/theme/" + image);
		$(".mainmenu .addimage img").show().css({"cursor":"pointer"}).click(function(){
            var Href = $(".mainmenu .hoversubmenu a").attr("href");
            window.location.href = Href;
		});
	}
});

$(".mainmenu td > a").mouseover(function(){
    $(".mainmenu .addimage img").hide();
});
';

        $this->outPut($output);
    }

    /**
     * show page content, removed H1 check
     */
    function showPageContent() {
        $args = $this->defaults['default'];

        $output = '
<div id="autoheight" class="pagecontentsmall">';

        if ($this->parentpage['template'] == 'news')
            $output.= '
<div class="newslist">';

        if ($this->page[$this->lang.'_topic'] && ($this->page['template'] != 'contact'))
            $output.= '
<h1 class="topic'.($this->parentpage['template'] == 'news' ? ' newstopic' : '').'">'.stripslashes($this->page[$this->lang.'_topic']).'</h1>';

        if ($this->parentpage['template'] == 'news')
            $output.= '
    <div class="newsdate">'.date($args['blog_date_format'], $this->page['timestamp']).'</div>';
        /*
            if ($this->page['gallery'] == 'before')
                $output.= $this->showGallery($this->page['id']);
        */
        if ($this->page[$this->lang.'_lead'])
            $output.= '
<div class="'.($this->parentpage['template'] == 'news' ? 'newsbody' : 'content').'">'.stripslashes($this->page[$this->lang.'_lead']).'</div>';

        if ($this->page[$this->lang.'_text'])
            $output.= '
<div class="'.($this->parentpage['template'] == 'news' ? 'newsbody' : 'content').'">'.stripslashes($this->page[$this->lang.'_text']).'</div>';
        /*
            if ($this->page['gallery'] == 'after')
                $output.= $this->showGallery($this->page['id']);
        */
        if ($this->parentpage['template'] == 'news')
            $output.= '
</div>';

        if (isset($_GET['message']) && $_GET['message'] == 'newsletter_joined') {
            $link = 'http://'.$_SERVER['SERVER_NAME'].substr($_SERVER['REQUEST_URI'], 0, strpos($_SERVER['REQUEST_URI'], '?')).($this->page['template'] == 'search' ? '?keyword='.$_GET['keyword'] : '');

            $output.= '
	<div class="overlay_message"></div>
	<div class="message_close">'.$this->trans('newsletter_joined').'<img src="'.DIR.'images/close.png" title="close" /></div>
	<input type="hidden" name="message_redirect" id="message_redirect" value="'.$link.'" />';
        }

        $output.= '
</div>';

        $this->outPut($output);
    }

    /**
     * gallery for services page with carousel
     */
    function servicesGallery() {
        $output = '';
        $param = array(
            'single'		=> false,
            'table'			=> 'images',
            'where'			=> 'parent="'.$this->page['id'].'"',
            'order'			=> 'jrk',
        );
        $images = $this->getSQL($param);

        if ($images) {
            $output.= '<div class="page_services_gallery">';

            while(count($images) < 3)
                $images = array_merge($images, $images);

            if (count($images)) {
                $output.= '<ul class="jcarousel-skin-tango">';

                for ($i = 0; $i < count($images); $i++) {
                    $row = $images[$i];
                    $max = $row['path'];
                    $dir = $row['path'].'475/';

                    if ($row['filename'] != '' and file_exists($dir.$row['filename']))
                        $output.= '
<li><a rel="gallery" href="'.URL.'/'.$max.$row['filename'].'" title="" class="opengallery" style="background-image:url(\''.URL.'/'.$dir.$row['filename'].'\');"></a></li>';
                }

                $output.= '</ul>';
            }

            $output.= '</div><div class="services_gallery_coverup"></div>';
        }

        $this->outPut($output);
    }

    /**
     * store list
     */
    function showStoresList() {
        $param = array(
            'single'		=> false,
            'table'			=> 'stores',
            'where'			=> 'allowed="yes"',
            'order'			=> 'jrk DESC',
        );
        $stores = $this->getSQL($param);

        foreach ($stores as $row)
            $display[$row['id']] = $row;

        $output = '
<div class="storelist">
  <h1>'.$this->trans('store_topic').'</h1>

  <div class="store_select_country">
    <h2 id="store_h2_country">'.$this->trans('store_topic_country').'</h2>
<input type="hidden" name="store_country_selected" id="store_country_selected" value="" />
	<ul class="store_select_country_list" id="store_country">';

        foreach ($stores as $row) {
            if ($row['type'] == 0) {
                $map = explode(',', $row['map']);

                if (!$map[2])
                    $map[2] = 14;

                $output.= '
      <li onclick="storeSwap('.$map[0].', '.$map[1].', '.$map[2].', \'#store_h2_country\', \''.$row[$this->lang].'\', \'#store_town_div\', '.$row['id'].', \'country\');">'.stripslashes($row[$this->lang]).'</li>';
            }
        }

        $output.= '
    </ul>
  </div>';

        $output.= '
  <div class="store_select_town" id="store_town_div">
    <h2 id="store_h2_town">'.$this->trans('store_topic_town').'</h2>
	<ul class="store_select_town_list" id="store_town">';

        foreach ($stores as $row) {
            if ($row['type'] == 1) {
                $map = explode(',', $row['map']);

                if (!$map[2])
                    $map[2] = 14;

                $output.= '
      <li class="hide_town store_town_selector_'.$row['parent'].'" onclick="storeSwap('.$map[0].', '.$map[1].', '.$map[2].', \'#store_h2_town\', \''.$row[$this->lang].'\', \'#store_store\', '.$row['id'].', \'town\');">'.stripslashes($row[$this->lang]).'</li>';
            }
        }

        $output.= '
    </ul>
  </div>';

        $output.= '
  <div class="store_showlist" id="store_store">';

        foreach ($stores as $row) {
            if ($row['type'] == 2) {
                $map = explode(',', $row['map']);

                if (!$map[2])
                    $map[2] = 16;

                $output.= '
    <div class="hide_store store_store_selector_'.$row['parent'].'" onclick="storeSwap('.$map[0].', '.$map[1].', '.$map[2].');"><strong>'.stripslashes($row[$this->lang]).'</strong><br />'.stripslashes($row[$this->lang.'_text']).'</div>';
            }
        }

        $output.= '
  </div>';

        $output.= '
</div>';

        $this->outPut($output);
    }

    /**
     * shows actual map
     */
    function showMap() {
        global $display;
        if ($_GET['country'] && $_GET['town'])
            $map = $display[$_GET['town']]['map'];
        elseif ($_GET['country'] && !$_GET['town'])
            $map = $display[$_GET['country']]['map'];
        else
            $map = $this->configuration['general_store_map'];

        $output = '
<div class="storemap">'.stripslashes($map).'</div>';

        $this->outPut($output);
    }

    /**
     * store page footer icons
     */
    function showStoresFooterButtons() {
        $pos = explode(',', $this->configuration['stores_show_all']);

        $output = '
<div class="storeicons">
  <a class="storeico_showall" href="#" onclick="swapView('.$pos[0].', '.$pos[1].', '.$pos[2].')">'.$this->trans('storeico_showall').'</a>
</div>';

        $this->outPut($output);
    }

    /**
     * for smartasses trying to access directly only gallery page we have additional redirection
     */
    function galleryReDirect() {
        if ($this->page['template'] == 'gallery' && $this->page['parent'] == 'parent') {
            $param = array(
                'select'	=> 'id',
                'where'		=> 'parent="'.$this->page['id'].'"',
                'order'		=> 'jrk',
                'limit'		=> '1',
            );
            $gallery = $this->getSQL($param);

            header('Location: '.$this->SEOlink($gallery['id']));
            exit;
        }
    }

    /**
     * gallery for gallery with carousel
     */
    function galleryGallery() {
        $output = '
<div class="page_gallery_gallery">';

        $param = array(
            'single'		=> false,
            'table'			=> 'images',
            'where'			=> 'parent="'.$this->page['id'].'"',
            'order'			=> 'jrk',
        );
        $images = $this->getSQL($param);

        while(count($images) < 7)
            $images = array_merge($images, $images);

        if (count($images)) {
            $output.= '
<ul class="jcarousel-skin-tango-gallery">';

            for ($i = count($images) - 3; $i < count($images); $i++) {
                $row = $images[$i];
                $max = $row['path'];
                $dir = $row['path'].'475/';

                $ssize='';
                $size = getimagesize($dir.$row['filename']);
                $ratio = $size[1] / $size[0];

                if ($size[0] < 356)
                    $ssize = ' background-size: 356px auto;';

                if ($row['filename'] != '' and file_exists($dir.$row['filename']))
                    $output.= '
  <li>
    <a rel="gallery" href="'.URL.'/'.$max.$row['filename'].'" title="" class="opengallery" style="background-image:url(\''.URL.'/'.$dir.$row['filename'].'\');'.$ssize.'">
      <div class="page_gallery_icoset">
        <span class="enlarge"></span>
      </div>
    </a>
  </li>';
//  <li><img src="'.URL.'/'.$dir.$row['filename'].'" width="356" height="475" title="" /><div class="gallery_full_link"><a href="#"></a></div></li>';
            }

            for ($i = 0; $i < count($images) - 3; $i++) {
                $row = $images[$i];
                $max = $row['path'];
                $dir = $row['path'].'475/';

                $ssize='';
                $size = getimagesize($dir.$row['filename']);
                $ratio = $size[1] / $size[0];

                if ($size[0] < 356)
                    $ssize = ' background-size: 356px auto;';

                if ($row['filename'] != '' and file_exists($dir.$row['filename']))
                    $output.= '
  <li>
    <a rel="gallery" href="'.URL.'/'.$max.$row['filename'].'" title="" class="opengallery" style="background-image:url(\''.URL.'/'.$dir.$row['filename'].'\');'.$ssize.'">
      <div class="page_gallery_icoset">
        <span class="enlarge"></span>
      </div>
    </a>
  </li>';
//  <li><img src="'.URL.'/'.$dir.$row['filename'].'" width="356" height="475" title="" /><div class="gallery_full_link"><a href="#"></a></div></li>';
            }

            $output.= '
</ul>';
        }

        $output.= '
  <div class="gallery_gallery_coverup_right"></div>
  <div class="gallery_gallery_coverup_left"></div>
</div>';

        $this->outPut($output);
    }

    /**
     * whitebox redirect
     */
    function whitebox_redirect() {
        header('Location: '.URL);
        exit;
    }

    /**
     * expo main
     */
    function expo() {
        global $specific;
        $expos = $specific->getExpos();
        $output = '';
        $allow = 0;

        for ($i = 0; $i < count($expos); $i++) {
            $row = $expos[$i];

            $param = array(
                'single'	=> false,
                'table'		=> 'images',
                'order'		=> 'jrk',
                'where' 	=> 'parent="'.$row['id'].'"',
            );

            $expo_images[$i] = $this->getSQL($param);

            if (count($expo_images[$i]) > 0)
                $allow++;
        }

        if ($allow == 1)
            $output.= '
<div class="expo_line_high">';

        $first = true;
        for ($i = 0; $i < count($expos); $i++) {
            $row = $expos[$i];
            $images = $expo_images[$i];

            if ($images) {
                while(count($images) < 7)
                    $images = array_merge($images, $images);

                array_unshift($images, $images[count($images) - 1]);
                array_pop($images);
                array_unshift($images, $images[count($images) - 1]);
                array_pop($images);
                array_unshift($images, $images[count($images) - 1]);
                array_pop($images);
                array_unshift($images, $images[count($images) - 1]);
                array_pop($images);
            }

            if (is_array($images)) {
                $output.= '
<div class="expo_line'.($first ? ' expo_line_first' : '').' expo_line_'.($first ? 1 : 2).'">
  <ul class="jcarousel-skin-tango-expo">';

                foreach($images as $item) {
                    $max = $item['path'];
                    $dir = $item['path'].'475/';

                    if ($item['filename'] != '' and file_exists($dir.$item['filename']))
                        $output.= '
    <li><a rel="gallery" href="'.URL.'/'.$max.$item['filename'].'" title="" class="opengallery" style="background-image:url(\''.URL.'/'.$dir.$item['filename'].'\');"></a></li>';
//    <li><img src="'.URL.'/'.$dir.$item['filename'].'" width="356" height="275" title="" /><div class="gallery_full_link"><a href="#"></a></div></li>';
                }

                $output.= '
  </ul>

  <div class="expo_navi_container">
<div class="expo_navi_bg_'.($first ? 1 : 2).'">
    <div class="expo_navi_'.($first ? 1 : 2).'">
      <a href="#" id="expo_line_prev_'.($first ? 1 : 2).'"><img src="'.DIR.'images/prev-small.png" title="prev" /></a>
      '.stripslashes($row[$this->lang]).'
      <a href="#" id="expo_line_next_'.($first ? 1 : 2).'"><img src="'.DIR.'images/next-small.png" title="next" /></a>
    </div>
</div>
  </div>

</div>';
            }

            $first = false;
        }

        if (count($expos) < 2)
            $output.= '
</div>';

        $this->outPut($output);
    }

    /**
     * expo main - test polygon
     */
    function expo_test() {
        global $specific;
        $expos = $specific->getExpos();
        $allow = 0;
        $output = '';

        for ($i = 0; $i < count($expos); $i++) {
            $row = $expos[$i];

            $param = array(
                'single'	=> false,
                'table'		=> 'images',
                'order'		=> 'jrk',
                'where' 	=> 'parent="'.$row['id'].'"',
            );

            $expo_images[$i] = $this->getSQL($param);

            if (count($expo_images[$i]) > 0)
                $allow++;
        }

        if ($allow == 1)
            $output.= '<div class="expo_line_high">';

        $first = true;
        $count = 0;
        for ($i = 0; $i < count($expos); $i++) {
            $row = $expos[$i];
            $images = $expo_images[$i];
            $count++;
            if ($count > 3)
                $count = 1;

            if ($images) {
                while(count($images) < 7)
                    $images = array_merge($images, $images);

                array_unshift($images, $images[count($images) - 1]);
                array_pop($images);
                array_unshift($images, $images[count($images) - 1]);
                array_pop($images);
                array_unshift($images, $images[count($images) - 1]);
                array_pop($images);
                array_unshift($images, $images[count($images) - 1]);
                array_pop($images);
            }

            if (is_array($images)) {
                $id = rand();

                $output.= '
<div class="expo_line'.($first ? ' expo_line_first' : '').' expo_line_'.$count.'" id="expo_liner_c_'.$id.'">
  <ul class="jcarousel-skin-tango-expo">';

                foreach($images as $item) {
                    $max = $item['path'];
                    $dir = $item['path'].'475/';

                    if ($item['filename'] != '' and file_exists($dir.$item['filename']))
                        $output.= '
    <li><a rel="gallery_'.$i.'" href="'.URL.'/'.$max.$item['filename'].'" title="" class="opengallery" style="background-image:url(\''.URL.'/'.$dir.$item['filename'].'\');"></a></li>';
//    <li><img src="'.URL.'/'.$dir.$item['filename'].'" width="356" height="275" title="" /><div class="gallery_full_link"><a href="#"></a></div></li>';
                }

                $output.= '
  </ul>

  <div class="expo_navi_container">
<div class="expo_navi_bg_'.$count.'" id="expo_liner_'.$id.'">
    <div class="expo_navi_'.$count.'">
      <a href="#" class="prev"><img src="'.DIR.'images/prev-small.png" title="prev" /></a>
      '.stripslashes($row[$this->lang]).'
      <a href="#" class="next"><img src="'.DIR.'images/next-small.png" title="next" /></a>
    </div>
</div>
  </div>

</div>

  <script type="text/javascript">
function expo_line_initCallback_'.$id.'(carousel) {
    jQuery("#expo_liner_'.$id.' .next").bind("click", function() { carousel.next(); return false; });
    jQuery("#expo_liner_'.$id.' .prev").bind("click", function() { carousel.prev(); return false; });
};
//expo page gallery carousel 1
$(function() {
	$("#expo_liner_c_'.$id.' ul").jcarousel({
		wrap: "circular",
		scroll: 1,
		initCallback: expo_line_initCallback_'.$id.',
		buttonNextHTML: null,
		buttonPrevHTML: null
	});


});
  </script>
';
            }

            $first = false;
        }

        if (count($expos) < 2)
            $output.= '
</div>';

        $this->outPut($output);
    }

    /**
     * get 2 latest expos
     * @return array|bool|QueryResult
     */
    function getExpos() {
        $param = array(
            'single'	=> false,
            'where'		=> 'parent="'.$this->page['id'].'" AND hidden="no"',
            'order'		=> 'jrk',
        );
//		'limit'		=> '2',

        $res = $this->getSQL($param);

        return $res;
    }

    /**
     * products header
     */
    function product_header() {
        $param = array(
            'single'	=> false,
            'table'		=> 'images',
            'where'		=> 'parent="'.($this->page['parent'] != 'parent' ? $this->page['parent'] : $this->page['id']).'"',
            'order'		=> 'jrk',
        );
        $images = $this->getSQL($param);
        $output = '';

        if (is_array($images) && !empty($images)) {
            while(count($images) < 13)
                $images = array_merge($images, $images);

            $output.= '<div class="products_line"><ul class="jcarousel-skin-tango-products">';

            foreach($images as $item) {
                $max = $item['path'];
                $dir = $item['path'].'475/';

                if ($item['filename'] != '' and file_exists($dir.$item['filename']))
                    $output.= '
    <li><a rel="gallery" href="'.URL.'/'.$max.$item['filename'].'" title="" class="opengallery" style="background-image:url(\''.URL.'/'.$dir.$item['filename'].'\');"></a></li>';
            }

            $output.= '
  </ul>

  <div class="products_navi_container">
    <div class="products_navi">
      <a href="#" id="products_line_prev"><img src="'.DIR.'images/prev-small.png" title="prev" /></a>
      '.$this->trans('products_gallery_topic').'
      <a href="#" id="products_line_next"><img src="'.DIR.'images/next-small.png" title="next" /></a>
    </div>
  </div>

</div>';
        }

        $this->outPut($output);
    }

    /**
     * displayes product submenu
     */
    function product_submenu() {
        global $specific;
        $param = array(
            'parent'	=> 'sub',
            'sqlparent'	=> ($this->page['parent'] != 'parent' ? $this->page['parent'] : $this->page['id']),
        );

        $submenu = $specific->display_menu($param);

        $topic = $this->page[$this->lang];

        if ($this->parentpage)
            $topic = $this->parentpage[$this->lang];

        $output = '
<div class="product_submenu">
<h1>'.$topic.'</h1>
'.$submenu.'
</div>';

        $this->outPut($output);
    }

    /**
     * product PDFs
     */
    function product_list() {
        if ($this->page['parent'] == 'parent') {
            $param = array(
                'single'	=> false,
                'where'		=> 'parent="'.$this->page['id'].'" AND page="site"',
                'order' 	=> 'jrk DESC',
            );
            $parents = $this->getSQL($param);

            $parent = '(';
            for ($i = 0; $i < count($parents); $i++)
                $parent.= 'parent="'.$parents[$i]['id'].'" OR ';

            $parent.= 'parent="'.$this->page['id'].'")';
        } else {
            $parent = 'parent="'.$this->page['id'].'"';
        }

        $param = array(
            'single'	=> false,
            'where'		=> $parent.' AND page="list"',
            'order' 	=> 'jrk DESC',
        );
        $groups = $this->getSQL($param);

        $output = '<div class="product_list"><ul>';

        if ($this->page['template'] == 'sale-bonus') {
            for ($i = 0; $i < count($groups); $i++) {
                $row = $groups[$i];

                $param = array(
                    'single'	=> false,
                    'table'		=> 'images',
                    'where'		=> 'parent="'.$row['id'].'"',
                    'order' 	=> 'jrk',
                    'limit'		=> '1',
                );
                $image = $this->getSQL($param);

                $output.= '
  <li onclick="openWhiteBox('.$row['id'].', \'bonus\')"><div class="product_image" style="background-image: url(\''.URL.'/'.$image[0]['path'].'200/'.$image[0]['filename'].'\');"></div><h1>'.stripslashes($row[$this->lang.'_topic']).'</h1><h2>'.$this->trans('sale-bonus_info').'</h2></li>';
            }
        } else {
            $param = array(
                'table'		=> 'files',
                'where'		=> 'id="'.($this->page['parent'] == 'parent' ? $this->page['id'] : $this->page['parent']).'"',
            );
            $file = $this->getSQL($param);

            for ($i = 0; $i < count($groups); $i++) {
                $row = $groups[$i];

                $output.= '
  <li>';
                if ($file['filename'] && file_exists($file['path'].$file['filename']))
                    $output.= '<a href="'.URL.'/'.$file['path'].$file['filename'].($row[$this->lang.'_lead'] ? '?#page='.$row[$this->lang.'_lead'] : '').'">';

                $output.= '<div class="product_image" style="background-image: url(\''.URL.'/upload/products/groups/'.$row['gallery'].'\');"></div><h1>'.stripslashes($row[$this->lang.'_topic']).'</h1><h2>'.stripslashes($row[$this->lang.'_text']).'</h2>';

                if ($file['filename'] && file_exists($file['path'].$file['filename']))
                    $output.= '</a>';

                $output.= '</li>';
            }
        }

        $output.= '
</ul>
</div>';

        $this->outPut($output);
    }

    /**
     * news list
     */
    function news_list() {
        //get news parents
        if ($this->page['parent'] == 'parent') {
            $param = array(
                'single'	=> false,
                'table'		=> 'page',
                'where'		=> 'template="news" AND parent="'.$this->page['id'].'"',
                'order' 	=> 'jrk DESC',
            );
            $parents = $this->getSQL($param);
            $parentsparent = ' parent="'.$this->page['id'].'"';

            if (is_array($parents) && !empty($parents)) {
                foreach($parents as $row) {
                    $topic[$row['id']] = $row[$this->lang];

                    $parentsparent.= ' OR parent="'.$row['id'].'"';
                }
            }
        } else {
            $parentsparent = 'parent="'.$this->page['id'].'"';
        }

        $param = array(
            'single'	=> false,
            'table'		=> 'page',
            'where'		=> '('.$parentsparent.') AND page="list"',
            'order' 	=> 'timestamp DESC, jrk DESC',
        );
        $news = $this->getSQL($param);

        $parent = '';
        if (is_array($news) && !empty($news)) {
            foreach($news as $row) {
                if (!empty($parent))
                    $parent.= ' OR ';

                $parent.= 'parent="'.$row['id'].'"';
            }
        }

        if (!empty($parent)) {
            $param = array(
                'single'	=> false,
                'table'		=> 'images',
                'where'		=> $parent,
                'order' 	=> 'jrk DESC',
            );
            $images = $this->getSQL($param);
        }

        if (is_array($images) && !empty($images)) {
            foreach($images as $row) {
                $image[$row['parent']] = $row;
            }
        }
        /*
            $output.= '
        <div class="news_list">
        <ul>';

            if (is_array($news)) {
                foreach($news as $row) {
                    if ($row['filename'])
                        $size = getimagesize($row['path'].$row['filename']);

                    $output.= '
          <li>
            <a href="#" onclick="openWhiteBox('.$row['id'].', \'news\')">
              <h1>'.stripslashes($row[$this->lang]).'</h1>
              <h2>'.date('d.m', $row['timestamp']).'</h2>
              <h3>'.stripslashes($topic[$row['parent']]).'</h3>
              <img class="lazy" src="'.DIR.'images/spacer.gif" title="'.stripslashes($row[$this->lang.'_topic']).'" data-original="'.URL.'/'.$image[$row['id']]['path'].'tnb/'.$image[$row['id']]['filename'].'" width="'.$size[0].'" height="'.$size[1].'" />
            </a>
          </li>';
                }
            }

            $output.= '
        </ul>
        </div>';
        */
        $output = '
<div class="news_list">
<div class="news_list_inner">';

        if (is_array($news) && !empty($news)) {
            foreach($news as $row) {
                if ($image[$row['id']]['filename'] && file_exists($image[$row['id']]['path'].'tnb/'.$image[$row['id']]['filename']))
                    $size = getimagesize($image[$row['id']]['path'].'tnb/'.$image[$row['id']]['filename']);

                $output.= '
  <div>
    <a href="#" onclick="openWhiteBox('.$row['id'].', \'news\')">
      <h1>'.stripslashes($row[$this->lang]).'</h1>
      <h2>'.date('d.m', $row['timestamp']).'</h2>
      <h3>'.stripslashes($topic[$row['parent']]).'</h3>
      <img class="lazy" src="'.DIR.'images/spacer.gif" title="'.stripslashes($row[$this->lang.'_topic']).'" data-original="'.URL.'/'.$image[$row['id']]['path'].'tnb/'.$image[$row['id']]['filename'].'" width="'.$size[0].'" height="'.$size[1].'" />
    </a>
  </div>';
            }
        }

        $output.= '</div></div>';
        $this->outPut($output);
    }

    /**
     * contact form topics
     * @return mixed
     */
    function getTopics() {
        $param = array(
            'single'	=> false,
            'select'	=> 'id, '.$this->lang.'_topic',
            'table'		=> 'page',
            'where'		=> 'parent="'.$this->page['id'].'" AND hidden="no"',
            'order'		=> 'jrk DESC',
        );
        $topics = $this->getSQL($param);

        for ($i = 0; $i < count($topics); $i++) {
            $list[$topics[$i]['id']] = $topics[$i][$this->lang.'_topic'];
            $list['ids'][$i] = $topics[$i]['id'];
        }

        return $list;
    }

    /**
     * contact form with sumitter
     */
    function contactForm() {
        global $specific;
        $topics = $specific->getTopics();
        $output = '';
        if (isset($this->message))
            $output.= '
<div class="overlay" onclick="parent.location=\''.$this->SEOlink($this->page['id']).'\'"></div>
<div class="whitebox_wrapper">
  <div class="whitebox_display">
    <img src="'.DIR.'images/close.png" title="close" class="whitebox_close" onclick="parent.location=\''.$this->SEOlink($this->page['id']).'\'">
    '.$this->message.'
  </div>
</div>';

        $output.= '
<div class="form">
  <table>
  <tr>
    <td class="topic">'.$this->trans('contact_form_topic').'</td>
    <td>
    <form method="post" action="" name="contact" id="contact_form">
      <span>'.$this->trans('contact_form_text_1').'</span>
<div class="contact_topic">
<div id="contact_topic_display">'.($topics[intval($_GET['topic'])] ? stripslashes($topics[intval($_GET['topic'])]) : $this->trans('contact_form_select_topic')).'</div>
<input type="hidden" name="topic" id="topic" value="'.($topics[intval($_GET['topic'])] ? stripslashes($topics[intval($_GET['topic'])]) : $this->trans('contact_form_select_topic')).'" />
<ul>';

        for ($i = 0; $i < count($topics); $i++) {
            if (isset($topics['ids'][$i]))
                $output .= '<li>' . stripslashes($topics[$topics['ids'][$i]]) . '</li>';
        }
        $output.= '
</ul>
</div>
      <span>'.$this->trans('contact_form_text_2').'</span>

      <label for="name" class="label_name">'.$this->trans('contact_form_name').':</label>
      <label for="email" class="label_lname">'.$this->trans('contact_form_lname').':</label>
<br />
      <input type="text" name="name" id="name" class="input_name" value="'.$this->form_info['name'].'" />
      <input type="text" name="lname" id="lname" class="input_lname" value="'.$this->form_info['lname'].'" />
<br />
      <label for="name" class="label_street">'.$this->trans('contact_form_street').':</label>
      <label for="email" class="label_appartment">'.$this->trans('contact_form_appartment').':</label>
<br />
      <input type="text" name="street" id="street" class="input_street" value="'.$this->form_info['street'].'" />
      <input type="text" name="appartment" id="appartment" class="input_appartment" value="'.$this->form_info['appartment'].'" />
<br />
      <label for="email" class="label_town">'.$this->trans('contact_form_town').':</label>
<br />
      <input type="text" name="town" id="town" class="input_town" value="'.$this->form_info['town'].'" />
<br />
      <span>'.$this->trans('contact_form_text_3').'</span>
<br />
      <label for="name" class="label_phone">'.$this->trans('contact_form_phone').':</label>
      <input type="text" name="phone" id="phone" class="input_phone" value="'.$this->form_info['street'].'" />
<br />
      <label for="email" class="label_email">'.$this->trans('contact_form_email').':</label>
      <input type="text" name="email" id="email" class="input_email" value="'.$this->form_info['appartment'].'" />
<br />
      <label for="text" class="contact_form_text">'.$this->trans('contact_form_text').':</label>
<br />
      <img src="'.DIR.'images/textareatop.gif" title="border" class="contact_image" />
      <textarea name="text" id="text" rows="1" cols="1">'.$this->form_info['text'].'</textarea>
      <img src="'.DIR.'images/textareabottom.gif" title="border" class="contact_image" />
<br />
      <input type="submit" value="'.$this->trans('contact_form_send').'" class="contact_button" />
      <input type="hidden" name="act" value="form_'.$this->page['template'].'" />
      <input type="hidden" name="url" value="page='.$_GET['page'].'" />
    </form>
    </td>
  </tr>
  </table>
</div>';

        $this->outPut($output);
    }

    /**
     * bonus header
     */
    function bonus_header() {
        $param = array(
            'single'	=> false,
            'table'		=> 'images',
            'where'		=> 'parent="'.($this->page['parent'] != 'parent' ? $this->page['parent'] : $this->page['id']).'"',
            'order'		=> 'jrk',
        );
        $images = $this->getSQL($param);

        $output= '
<div class="bonus_line">';

        for ($i = 0; $i < 4; $i++) {
            $row = $images[$i];

            $dir = $row['path'].'270/';
            $link = '';
            $style = '';

            if ($row['filename'] && file_exists($dir.$row['filename']))
                $style = ' style="background-image: url(\''.URL.'/'.$dir.$row['filename'].'\'); "';
            if ($row[$this->lang])
                $link = ' onclick="parent.location=\''.$row[$this->lang].'\'"';

            $output.= '
  <div class="bonus_image_'.($i + 1).'"'.$style.$link.'></div>';
        }

        $this->outPut($output);
    }

    /**
     * catalog gallery
     */
    function catalogGallery() {
        global $specific;
        $output = '<div class="page_catalog_gallery">';

        $specific->checkPriceListLogin();

        if ($this->page['template'] == 'pricelist' && !$_SESSION['pricelist_login']) {
            $output.= '<div class="page_catalog_gallery page_pricelist_gallery">';

            $param = array(
                'single'		=> true,
                'table'			=> 'images',
                'where'			=> 'parent="'.$this->page['id'].'"',
                'order'			=> 'jrk',
                'limit'			=> 1,
            );
            $image = $this->getSQL($param);

            $output.= '<img src="'.URL.'/'.$image['path'].'_orig/'.$image['filename'].'" title="" />';

            $output.= '<div class="overlay"></div>
                        <div class="whitebox_wrapper">
                            <div class="whitebox_display">
                                <img src="'.DIR.'images/close.png" title="close" class="whitebox_close" onclick="parent.location=\''.$this->SEOlink($this->page['parent']).'\'" title="" />
                                <h2>'.$this->trans('pricelist_login_h1').'</h2>';

            if ($_SESSION['pricelist_login_failed']) {
                $output.= '<h2>'.$_SESSION['pricelist_login_failed'].'</h2>';
                unset($_SESSION['pricelist_login_failed']);
            }

            $output.= ' <form method="post" action="">
                        <label for="pl_username">'.$this->trans('pricelist_login_username').'</label>
                        <input type="text" name="pl_username" id="pl_username" />
                        <label for="pl_password">'.$this->trans('pricelist_login_password').'</label>
                        <input type="password" name="pl_password" id="pl_password" />
                        <input type="hidden" name="act" value="pricelist_login" />
                        <input type="submit" value=" " class="button" />
                    </form>
                </div>
            </div>
        </div>';
        } else {
            $param = array(
                'single'		=> true,
                'table'			=> 'images',
                'where'			=> 'parent="'.$this->page['id'].'"',
                'order'			=> 'jrk',
                'limit'			=> 1,
            );
            $image = $this->getSQL($param);

            $param = array(
                'table'			=> 'files',
                'where'			=> 'id="'.$this->page['id'].'"',
                'order'			=> 'jrk',
            );
            $file = $this->getSQL($param);
            $file['path'] = 'upload/pdf/';

            $dir = 'upload/pdf/'.dms::dirnamereturner($file['filename']).'/';
            $list = glob($dir . "*.jpg");

            $param = array(
                'table'			=> 'pdf',
                'where'			=> 'file_id="'.$this->page['id'].'"',
            );
            $pdf = $this->getSQL($param);

            if ($pdf['type'] == 'landscape')
            {
                $output.= '<div class="catalog_landscape">';
            }

            if (count($list)) {
                $output.= '<ul class="jcarousel-skin-tango-pdf">';

                for ($i = 0; $i < count($list); $i++) {
                    $ssize = '';
                    $size = getimagesize($list[$i]);
                    $ratio = $size[1] / $size[0];

                    $ssize = ' background-size: auto 475px;';
                    if (475/$ratio < 356)
                        $ssize = ' background-size: 356px auto;';

                    $output.= '<li style="background-image:url(\''.URL.'/'.$list[$i].'\');'.$ssize.'">';

                    if ($file['filename'] && file_exists($file['filename']))
                    {
                        $output.= '
                        <div class="page_catalog_icoset">
                          <a href="'.URL.'/'.$file['filename'].'#page='.($i + 1).'" class="enlarge"></a>
                          <a href="'.URL.'/download.php?file='.substr($file['filename'], 11).'" class="download"></a>
                          <a href="'.URL.'/'.$file['filename'].'" class="print"></a>
                        </div>';
                    }


                    $output.= '</li>';
                }

                $output.= '</ul>';
            }

            if ($pdf['type'] == 'landscape')
            {
                $output.= '</div>';
            }

            $output.= '<img src="'.URL.'/'.$image['path'].'_orig/'.$image['filename'].'" title="" />';
            $output.= '</div>';
        }

        $this->outPut($output);
    }

    /**
     * catalog gallery
     */
    function pricelistGallery() {
        global $specific;
        $output= '
<div class="page_catalog_gallery page_pricelist_gallery">';

        $param = array(
            'single'		=> true,
            'table'			=> 'images',
            'where'			=> 'parent="'.$this->page['id'].'"',
            'order'			=> 'jrk',
            'limit'			=> 1,
        );
        $image = $this->getSQL($param);

        $output.= '
  <img src="'.URL.'/'.$image['path'].'_orig/'.$image['filename'].'" title="" />';

        $specific->checkPriceListLogin();

        if ($_SESSION['pricelist_login']) {
            $param = array(
                'single'		=> true,
                'table'			=> 'files',
                'where'			=> 'id="'.$this->page['id'].'"',
                'order'			=> 'jrk',
                'limit'			=> 1,
            );
            $file = $this->getSQL($param);

            $output.= '
	<script type="text/javascript">
	window.open("'.URL.'/pdf.php?id='.$file['id'].'")
	</script>
</div>';
        } else {
            $output.= '
<div class="overlay"></div>
<div class="whitebox_wrapper">
  <div class="whitebox_display">
    <img src="'.DIR.'images/close.png" title="close" class="whitebox_close" onclick="parent.location=\''.$this->SEOlink($this->page['parent']).'\'" title="" />
    <h2>'.$this->trans('pricelist_login_h1').'</h2>';

            if ($_SESSION['pricelist_login_failed']) {
                $output.= '
    <h2>'.$_SESSION['pricelist_login_failed'].'</h2>';

                unset($_SESSION['pricelist_login_failed']);
            }

            $output.= '
    <form method="post" action="">
	<label for="pl_username">'.$this->trans('pricelist_login_username').'</label>
	<input type="text" name="pl_username" id="pl_username" />
	<label for="pl_password">'.$this->trans('pricelist_login_password').'</label>
	<input type="password" name="pl_password" id="pl_password" />
	<input type="hidden" name="act" value="pricelist_login" />
    <input type="submit" value=" " class="button" />
	</form>
  </div>
</div>';
        }

        $this->outPut($output);
    }

    /**
     * pricelist login check + 1h timeout
     */
    function checkPriceListLogin() {
        if (isset($_POST['act']) && $_POST['act'] == 'pricelist_login') {
            if ($_POST['pl_username'] == $this->configuration['pricelist_user'] && $_POST['pl_password'] == $this->configuration['pricelist_password']) {
                $_SESSION['pricelist_login'] = 'login';
            } else {
                $_SESSION['pricelist_login_failed'] = $this->trans('pricelist_login_failed');
            }
        }
    }

    /**
     * search results
     * @return string|void
     */
    function showSearchResults() {
        $keyword = mysql_escape_string($_GET['keyword']);
        $lang = $this->lang;
        $output = '';

        if ($keyword) {
            $output.= '<ul class="searchresults">';

            $param = array(
                'single'		=> false,
                'fields'		=> $lang.', id, template, parent',
                'table'			=> 'page',
                'where'			=> '('.$lang.' LIKE "%'.$keyword.'%" OR '.$lang.'_text LIKE "%'.$keyword.'%" OR '.$lang.'_topic LIKE "%'.$keyword.'%") AND hidden="no"',
            );
            $res = $this->getSQL($param);

            for ($i = 0; $i < count($res); $i++) {
                if ($res[$i]['parent'] != 'parent') {
                    $param = array(
                        'fields'		=> 'template',
                        'table'			=> 'page',
                        'where'			=> 'id="'.$res[$i]['parent'].'"',
                    );
                    $parent = $this->getSQL($param);
                }

                $output.= '
  <li><a href="'.($res[$i]['template'] == 'whitebox' || $parent['template'] == 'news' ? '#" onclick="openWhiteBox('.$res[$i]['id'].($parent['template'] == 'news' ? ', \'news\'' : '').')' : $this->SEOlink($res[$i]['id'])).'">'.$res[$i][$lang].'</a></li>';
            }

            $output.= '
</ul>';
        }

        return $output;
    }

    function text_background() {
        $param = array(
            'table'		=> 'color',
            'where'		=> 'page_id="'.$this->page['id'].'"',
        );
        $color = $this->getSQL($param);
        $style = null;

        if ($color['text'])
            $style.= ($style ? ' ' : '').'color: #'.$color['text'].';';
        if ($color['bg'])
            $style.= ($style ? ' ' : '').'background-color: #'.$color['bg'].';';

        return ' style="'.$style.'"';
    }

    function catalog_bg_white() {
        $param = array(
            'table'		=> 'color',
            'where'		=> 'page_id="'.$this->page['id'].'"',
        );
        $color = $this->getSQL($param);

        if ($color['bg'])
            return ' class_'.$color['bg'];
    }
}

$specific = new specific();
