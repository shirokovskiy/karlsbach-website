<?php

/* Database prototype class - �ldfunktsioonid p�ringute t��tlemise lihtsustamiseks*/
class Database {

    //Data
    public $name;
    public $host;
    public $link;
    public $error;

    //Accessors/Manipulators
    function Database($name = SQL_NAME, $host = SQL_HOST) { //constructor
        $this->name = $name;
        $this->host = $host;
        $this->link = 0;
        $this->connect();
        mysql_query("SET NAMES 'utf8'", $this->getLink());
    }

    function connect($username = SQL_USER, $password = SQL_PASS) {
        $this->setLink(	@mysql_pconnect($this->getHost(), $username, $password));
        if($this->getLink()==false) {
            $this->postErrorMsg('Failed to connect');
            return false;
        }

        elseif(@mysql_select_db($this->getName(), $this->getLink())==false) {
            $this->postErrorMsg();
            return false;
        } else {

            return $this->isConnected();
        }
    }

    function disconnect() {
        mysql_close($this->getLink());
        $this->setLink(0);
    }

    function postErrorMsg($msg='') {
        if(empty($msg))	{
            if (!DEBUG) {
                echo '<div style="width: 100%; padding: 3px; color: #FFFFFF; background: #FF0000; font-family: Verdana; font-size: x-small;">'.$this->error=mysql_error($this->getLink()).'</div>';  // fixme: vneshij CSS
            } else {
//					echo '<div style="width: 100%; padding: 3px; color: #FFFFFF; background: #FF0000; font-family: Verdana; font-size: x-small;">Error</div>';  // fixme: vneshij CSS
            }
        } else {
            if (!DEBUG) {
                echo '<div style="width: 100%; padding: 3px; color: #FFFFFF; background: #FF0000; font-family: Verdana; font-size: x-small;\">'.$this->error=mysql_error($this->getLink()).'<br><br><b>'.$this->error=$msg.'</b></div>';
            } else {
//					echo '<div style="width: 100%; padding: 3px; color: #FFFFFF; background: #FF0000; font-family: Verdana; font-size: x-small;">Error</div>';  // fixme: vneshij CSS
            }
        }
    }

    function query($sql) {
        $result=@mysql_query($sql, $this->getLink());
        if(!$result) {
            $this->postErrorMsg($sql);
            return false;
        } else {
            return new QueryResult($this, $result);
        }
    }

    function setLink($link) {
        $this->link = $link;
    }

    function getLink() {
        return $this->link;
    }

    function getHost() {
        return $this->host;
    }

    function getName() {
        return $this->name;
    }

    function isConnected() {
        return ($this->link != 0);
    }
}

/* QueryResult  class */
class QueryResult {
    public $database;
    public $resultId;
    public $errorMsg;
    public $currentRow;
    public $indexReset;

    function QueryResult($database, $resultId) { //Constructor
        $this->database = $database;
        $this->resultId =  $resultId;
        $this->currentRow   = 0;
        $this->ErrorMsg = ($resultId != false) ? '' : $database->getErrorMsg();
        echo mysql_error();
    }

    function clear() {
        mysql_free_result($this->getResultId());
    }


    function getResultId() {
        return $this->resultId;
    }

    function &getDatabase() {
        return $this->database;
    }

    function isSuccess() {
        return ($this->resultId != 0);
    }

    function getRowCount() {
        return mysql_num_rows($this->getResultId());
    }

    function getFieldCount() {
        return mysql_num_fields($this->getResultId());
    }

    function getFieldInfo ( $field ) {
        return mysql_fetch_field($this->getResultId(), $field);
    }

    function getRow($index = 0) {
        static $result=0;
        if( ($result==0) || ($this->indexReset=='change') ) {
            $this->currentRow==$index ? '' : $this->currentRow=$index;
            mysql_data_seek($this->getResultId(), $this->currentRow);
            $this->indexReset='static';
        }

        ++$result;
        ++$this->currentRow;

        return mysql_fetch_array($this->getResultId());
    }

    function getErrorMsg() {
        return $this->errorMsg;
    }
}
