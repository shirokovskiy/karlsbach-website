<?php
include_once('setup.php');

class Admin extends DMS {

    function __construct() {
    }

    //directs to POST or specific url
    function admin_redirect($special = '') {
//        header('Location: '.($different != '' ? $different : 'http://'.$_SERVER['SERVER_NAME'].$_POST['redirect']));

        //build 1.01
//        header('Location: '.($special ? $special : $_POST['redirect']));

        //build 1.02
        if (!$special) {
            if ($pos = strpos($_POST['redirect'], 'cur_lang')) {
                $start = substr($_POST['redirect'], 0, $pos - 1);
                $_POST['redirect'] = $start.substr($_POST['redirect'], $pos + 13 - 1);
            }

            if ($_POST['new_lang'])
                $langadd = '&cur_lang='.$_POST['new_lang'];

            if ($_POST['new_lang_disable'] == 1)
                $langadd = '';
        }

        header('Location: '.($special ? $special : $_POST['redirect'].$langadd));
        exit;
    }

	//generate random text
    function randomString($len) {
        srand(date("s"));
        $str = '';
        $i = 0;

        while($i < $len) {
            $str.= chr((rand()%26) + 97);
            $i++;
        }

        $str = $str.substr(uniqid (""), 0, 22);

        return $str;
    }


//deals POST & GET values
    function neuter($input, $replace = false) {
//	return mysql_real_escape_string(addslashes($input));
        if ($replace)
            $input = str_replace('"', '&quot;', $input);

        $return = addslashes($input);

        return $return;
    }

//title formatting
    /*
    function title_slug($title) {
        $title = admin::cyrillicSEO($title);
        $slug = admin::str_encode($title);

        $bad = array('"', "'", '�', '�', "\n", "\r", '_',
                    chr(246), chr(228), chr(252), chr(245), chr(214), chr(196), chr(220), chr(213), );

        $good = array('','','','','','','-',
                    'o', 'a', 'u', 'o', 'O', 'A', 'U', 'O', );

    // replace strange characters with alphanumeric equivalents
        $slug = str_replace($bad, $good, $slug);

    // remove any duplicate whitespace, and ensure all characters are alphanumeric
        $bad_reg = array('/\s+/','/[^A-Za-z0-9\-]/');
        $good_reg = array('-', '');
        $slug = preg_replace($bad_reg, $good_reg, $slug);

    // and lowercase
        $slug = strtolower($slug);
        $slug = str_replace('--', '-', $slug);

        if ($slug[strlen($slug) - 1] == '-')
            $slug = substr($slug, 0, strlen($slug) - 1);

        return $slug;
    }
    */

//additionalt adjustment for some special characters
    function str_encode($string, $to = "iso-8859-9", $from = "utf8") {
        if ($to == "iso-8859-9" && $from == "utf8"){
            $str_array = array(
                chr(196).chr(177) => chr(253),
                chr(196).chr(176) => chr(221),
                chr(195).chr(182) => chr(246),
                chr(195).chr(150) => chr(214),
                chr(195).chr(167) => chr(231),
                chr(195).chr(135) => chr(199),
                chr(197).chr(159) => chr(254),
                chr(197).chr(158) => chr(222),
                chr(196).chr(159) => chr(240),
                chr(196).chr(158) => chr(208),
                chr(195).chr(188) => chr(252),
                chr(195).chr(156) => chr(220),
                chr(195).chr(149) => chr(213),
                chr(195).chr(181) => chr(245),
                chr(195).chr(164) => chr(228),
            );

            return str_replace(array_keys($str_array), array_values($str_array), $string);
        }

        return $string;
    }

//fix coding
    function utf2win($text) {
        global $w8;

        $c1 = chr(208);
        $c2 = chr(209);
        $u = false;
        $temp = $lc = "";

        for($i = 0; $i < strlen($text); $i++) {
            $c = substr($text,$i,1);

            if ($u) {
                $c = $w8[$lc.$c];
                $temp .= isset($c)?$c:"?";
                $u = false;
            } else if ($c==$c1 || $c==$c2) {
                $u = true;
                $lc = $c;
            } else
                $temp .= $c;
        }

        return $temp;
    }

//cyrillic fix for SOE
    function cyrillicSEO($text) {
        admin::u8("i",208,185); admin::u8("ch",209,134); admin::u8("u",209,131);
        admin::u8("k",208,186); admin::u8("je",208,181); admin::u8("n",208,189);
        admin::u8("g",208,179); admin::u8("sh",209,136); admin::u8("zh",209,137);
        admin::u8("z",208,183); admin::u8("h",209,133); admin::u8("",209,138);
        admin::u8("f",209,132); admin::u8("o",209,139); admin::u8("v",208,178);
        admin::u8("a",208,176); admin::u8("p",208,191); admin::u8("r",209,128);
        admin::u8("o",208,190); admin::u8("l",208,187); admin::u8("d",208,180);
        admin::u8("zh",208,182); admin::u8("e",209,141); admin::u8("ja",209,143);
        admin::u8("tsh",209,135); admin::u8("s",209,129); admin::u8("m",208,188);
        admin::u8("i",208,184); admin::u8("t",209,130); admin::u8("",209,140);
        admin::u8("",208,177); admin::u8("ju",209,142); admin::u8("I",208,153);
        admin::u8("CH",208,166); admin::u8("U",208,163); admin::u8("K",208,154);
        admin::u8("JE",208,149); admin::u8("N",208,157); admin::u8("G",208,147);
        admin::u8("SH",208,168); admin::u8("ZH",208,169); admin::u8("Z",208,151);
        admin::u8("X",208,165); admin::u8("",208,170); admin::u8("F",208,164);
        admin::u8("O",208,171); admin::u8("V",208,146); admin::u8("A",208,144);
        admin::u8("P",208,159); admin::u8("R",208,160); admin::u8("O",208,158);
        admin::u8("L",208,155); admin::u8("D",208,148); admin::u8("ZH",208,150);
        admin::u8("E",208,173); admin::u8("JA",208,175); admin::u8("TSH",208,167);
        admin::u8("S",208,161); admin::u8("M",208,156); admin::u8("I",208,152);
        admin::u8("T",208,162); admin::u8("",208,172); admin::u8("",208,145);
        admin::u8("JU",208,174); admin::u8("E",209,145); admin::u8("E",208,129);

        return admin::utf2win($text);
    }

	//generate .htaccess
    function generatehtaccess() {
        global $dms;

		// beginnig. turning on the rewrite engine, allowing access to the DMS & ensuring the current links still work
        $file = 'php_value memory_limit 256M
php_value max_execution_time 600
RewriteEngine on
Options -Indexes
DirectoryIndex index.html index.php

RewriteRule ^dms/logout/$ logout.php [L,QSA]
RewriteRule ^dms/forgot/$ index.php?dms=true&act=forgot [L,QSA]
RewriteRule ^dms/$ index.php?dms=true [L,QSA]
';

		//language links
        for ($i = 0; $i < count($dms->langlist); $i++) {
            $row = $dms->langlist[$i];
            $lang_list[] = $row['name'];

            $file.= '
RewriteRule ^'.$row['name'].'/$ '.ROOT_DIR.'/index.php?&lang='.$row['name'].' [L,QSA]';
        }

//pages
        $param = array('single'	=> false);
        $array = $dms->getSQL($param);

        for ($i = 0; $i < count($array); $i++) {
            $row = $array[$i];

            for ($j = 0; $j < count($lang_list); $j++) {
                $link = $dms->title_slug(stripslashes($row[$lang_list[$j].'']));

                if ($link)
                    $file.= '
RewriteRule ^'.$lang_list[$j].'/'.$dms->getParentTree($row['id'], $lang_list[$j]).'$ '.ROOT_DIR.'/index.php?page='.$row['id'].'&lang='.$lang_list[$j].' [L,QSA]';

                if ($link && $j == 0)
                    $file.= '
RewriteRule ^'.$dms->getParentTree($row['id'], $lang_list[$j]).'$ '.ROOT_DIR.'/index.php?page='.$row['id'].'&lang='.$lang_list[$j].' [L,QSA]';
            }

            if ($row['template'] == 'portfolio' || $row['template'] == 'news') {
                if ($row['template'] == 'news')
                    $row['template'] = 'page';

                $subchildren[] = $row;
            }
        }

//list style pages
        for ($sub = 0; $sub < count($subchildren); $sub++) {
            $subchild = $subchildren[$sub];

            $param_list = array(
                'single'	=> false,
                'table'		=> $subchild['template'],
            );
            $list_array = $dms->getSQL($param_list);

            for ($i = 0; $i < count($list_array); $i++) {
                $row = $list_array[$i];

                for ($j = 0; $j < count($lang_list); $j++) {
                    $link = $dms->title_slug(stripslashes($row[$lang_list[$j].'_topic']));

                    if ($link)
                        $file.= '
RewriteRule ^'.$lang_list[$j].'/'.$dms->getParentTree($subchild['id'], $lang_list[$j]).$link.'/$ '.ROOT_DIR.'/index.php?page='.$subchild['id'].'&id='.$row['id'].'&lang='.$lang_list[$j].' [L,QSA]';

                    if ($link && $j == 0)
                        $file.= '
RewriteRule ^'.$dms->getParentTree($subchild['id'], $lang_list[$j]).$link.'/$ '.ROOT_DIR.'/index.php?page='.$subchild['id'].'&id='.$row['id'].'&lang='.$lang_list[$j].' [L,QSA]';
                }
            }
        }

        $fp = fopen('.htaccess', 'w');
        fputs($fp, $file);
        fclose($fp);
    }

//TODO>loose the ugly SQL
    function admin_update_list() {
        global $db;
        $i = 0;

        $normal_row = array('site', 'gallery', 'lang', 'portfolio', 'headerimage');

        $table = 'page';
        if ($_GET['page'] == 'portfolio')
            $table = 'portfolio';
        elseif ($_GET['page'] == 'lang')
            $table = 'lang';
        elseif ($_GET['page'] == 'stores')
            $table = 'stores';

        if ($_GET['type'] == 'gallery') {
            $table = 'images';

            if ($_GET['page'] == 'portfolio')
                $table = 'images_portfolio';
            elseif ($_GET['page'] == 'headerimage')
                $table = 'header';
        }

        if (in_array($_GET['page'], $normal_row) || ($_GET['page'] == 'list' && $_GET['type'] == 'gallery'))
            $i = 0;
        else
            $i = count($_POST['item']) - 1;

        if (is_array($_POST['item'])) {
            foreach ($_POST['item'] as $item_id) {
                $sql = 'UPDATE '.SQL_PREFIX.$table.'
					SET jrk="'.$i.'"
					'.($_GET['type'] == 'gallery' && $_GET['page'] != 'headerimage' ? ', '.$_GET['cur_lang'].'="'.admin::neuter($_POST['textitem'][$i], true).'"' : '').'
					WHERE id="'.mysql_real_escape_string($item_id).'"';
                $res = $db->query($sql);

                if (in_array($_GET['page'], $normal_row) || ($_GET['page'] == 'list' && $_GET['type'] == 'gallery'))
                    $i++;
                else
                    $i--;
            }
        }

        if ($_GET['type'] == 'fileuploader') {
            $sql = 'SELECT *
				FROM '.SQL_PREFIX.'files
				WHERE id="'.intval($_GET['id']).'"';
            $res = $db->query($sql);
            $row = $res->getRow(0);

            if ($row) {
                $sql = 'UPDATE '.SQL_PREFIX.'files
					SET filename="'.$_POST['file'].'"
					WHERE id="'.intval($_GET['id']).'"';
            } else {
                $sql = 'INSERT
					INTO '.SQL_PREFIX.'files (id, filename)
					VALUES ("'.intval($_GET['id']).'", "'.$_POST['file'].'")';
            }
            $res = $db->query($sql);

            $sql = 'SELECT *
				FROM '.SQL_PREFIX.'pdf
				WHERE file_id="'.intval($_GET['id']).'"';
            $res = $db->query($sql);
            $row = $res->getRow(0);

            if ($row) {
                $sql = 'UPDATE '.SQL_PREFIX.'pdf
					SET type="'.$_POST['pdf_type'].'"
					WHERE file_id="'.intval($_GET['id']).'"';
            } else {
                $sql = 'INSERT
					INTO '.SQL_PREFIX.'pdf (file_id, type)
					VALUES ("'.intval($_GET['id']).'", "'.$_POST['pdf_type'].'")';
            }
            $res = $db->query($sql);
        }

        admin::admin_redirect();
    }

//TODO>loose the ugly SQL
    function admin_update_item() {
        global $db;

        $cur_lang = admin::neuter($_GET['cur_lang']);

        $order = array(
            'setup' => 'id',
            'trans' => 'group_id',
        );

        if ($_GET['page'] == 'lang') {
            for ($i = 0; $i < count($_POST['input']); $i++) {
                $sql = 'UPDATE '.SQL_PREFIX.admin::neuter($_GET['page']).'
					SET display="'.admin::neuter($_POST['input'][$i]).'"
					WHERE id="'.$_POST['marker'][$i].'"';
                $res2 = $db->query($sql);
            }
        } elseif ($_GET['page'] == 'trans' || $_GET['page'] == 'setup') {
            $sql = 'SELECT id, code'.($_GET['page'] == 'setup' ? ', type' : '').'
				FROM '.SQL_PREFIX.admin::neuter($_GET['page']).'
				ORDER BY '.$order[admin::neuter($_GET['page'])];
            $res = $db->query($sql);
            $n = $res->getRowCount();

            for ($i = 0; $i < $n; $i++) {
                $row = $res->getRow(0);

//no idea why we cannot fetch correct code from post
                $sql = 'UPDATE '.SQL_PREFIX.admin::neuter($_GET['page']).'
					SET '.$cur_lang.'="'.($row['type'] == 'dropdown' ? $_POST[$row['code']] : admin::neuter($_POST['input'][$row['id'].'_'.$cur_lang])).'"
					WHERE id="'.$row['id'].'"';
                $res2 = $db->query($sql);
            }
        } elseif ($_GET['page'] == 'site') {
//TODO>image upload here
            $image_param['uploaddir'] = 'upload/theme/';
            $filename = admin::randomString(5).$_FILES['uploadfile']['name'];
            if (move_uploaded_file($_FILES['uploadfile']['tmp_name'], $image_param['uploaddir'].$filename)) {
                chmod($image_param['uploaddir'].$filename, 0644);
            } else {
                $filename = '';
            }
            /*
                    $icon = randomString(5).RenameFile(substr($_FILES['icon']['name'], 0, strlen($_FILES['icon']['name']) - 4)).'.'.strtolower(substr($_FILES['icon']['name'], strlen($_FILES['icon']['name']) - 3, strlen($_FILES['icon']['name'])));
                    if (move_uploaded_file($_FILES['icon']['tmp_name'], $image_param['uploaddir'].$icon)) {
                        chmod($image_param['uploaddir'].$icon, 0644);
                    } else {
                        $icon = '';
                    }
            */
            if ($_GET['parent'] != $_POST['parent'] || $_POST['menu'] != $_POST['old_menu']) {
                $sql = 'SELECT MAX(jrk)
					FROM '.SQL_PREFIX.'page
					WHERE parent="'.admin::neuter($_POST['parent']).'"
						AND menu="'.$_POST['menu'].'"';
                $res = $db->query($sql);
                $row = $res->getRow(0);
                $jrk = $row[0] + 1;
            }

            $sql = 'UPDATE '.SQL_PREFIX.'page
				SET parent="'.($_GET['parent'] != $_POST['parent'] && $_POST['parent'] ? admin::neuter($_POST['parent']) : admin::neuter($_GET['parent'])).'",
					template="'.admin::neuter($_POST['template']).'",
					page="'.admin::neuter($_GET['page']).'",
					menu="'.admin::neuter($_POST['menu']).'",
					'.($jrk ? 'jrk="'.$jrk.'",' : '').'
					hidden="'.($_POST['hidden'] == 'on' ? 'yes' : 'no').'",
					coming_soon="'.($_POST['coming_soon'] == 'on' ? 'yes' : 'no').'",
					gallery="'.$_POST['dgallery'].'",
					'.($_GET['parent'] != $_POST['parent'] ? 'jrk="'.$jrk.'",' : '');

            if ($filename)
                $sql.= 'filename="'.$filename.'",';

            $sql.= '    '.$cur_lang.'="'.admin::neuter($_POST[$cur_lang]).'",
					'.$cur_lang.'_topic="'.admin::neuter($_POST[$cur_lang.'_topic']).'",
					'.$cur_lang.'_text="'.admin::neuter($_POST[$cur_lang.'_text']).'",
					'.$cur_lang.'_lead="'.admin::neuter($_POST[$cur_lang.'_lead']).'",
					'.$cur_lang.'_author="'.admin::neuter($_POST[$cur_lang.'_author']).'",
					'.$cur_lang.'_meta_desc="'.admin::neuter($_POST[$cur_lang.'_meta_desc']).'",
					'.$cur_lang.'_meta_key="'.admin::neuter($_POST[$cur_lang.'_meta_key']).'",
					'.$cur_lang.'_redirect_url="'.admin::neuter($_POST[$cur_lang.'_redirect_url']).'"';

            if ($_POST['date']) {
                $date_temp = explode('/', admin::neuter($_POST['date']));
                $time_temp = explode(':', admin::neuter($_POST['time']));

                if (!$time_temp[0])
                    $time_temp[0] = 0;
                if (!$time_temp[1])
                    $time_temp[1] = 0;

                $date = mktime($time_temp[0], $time_temp[1], 0, $date_temp[0], $date_temp[1], $date_temp[2]);
                $sql.= ', timestamp="'.$date.'"';
            }

//leave this
            $sql.= ' WHERE id="'.intval($_GET['id']).'"';
            $res = $db->query($sql);

//karlsbach hack
            if ($_POST['template'] == 'text' || $_POST['template'] == 'catalog') {
                $sql = 'SELECT *
			FROM '.SQL_PREFIX.'color
			WHERE page_id="'.intval($_GET['id']).'"';
                $res = $db->query($sql);
                $row = $res->getRow();

                if (!$row) {
                    $sql = 'INSERT
				INTO '.SQL_PREFIX.'color()
				VALUES("'.intval($_GET['id']).'", "'.$_POST['text_color'].'", "'.$_POST['bg_color'].'")';
                    $res = $db->query($sql);
                } else {
                    $sql = 'UPDATE '.SQL_PREFIX.'color
				SET text="'.$_POST['text_color'].'",
					bg="'.$_POST['bg_color'].'"
				WHERE page_id="'.intval($_GET['id']).'"';
                    $res = $db->query($sql);
                }
            }

            admin::generatehtaccess();
        } elseif ($_GET['page'] == 'list') {
            if ($_POST['hidden_product']) {
                $filename = admin::randomString(5).$_FILES['uploadfile']['name'];
                $dir = 'upload/products/groups/';

                if (move_uploaded_file($_FILES['uploadfile']['tmp_name'], $dir.$filename)) {
                    $param['quality'] = 80;
                    $param['x'] = 185;
                    $param['y'] = 200;

                    admin::resizeJPG($dir.$filename, $dir.$filename, $param['quality'], $param['x'], $param['y'], 'product');
                    @chmod($dir.$filename, 0755);
                } else
                    $filename = '';

                $_POST['dgallery'] = $filename;
            }

            $sql = 'SELECT *
				FROM '.SQL_PREFIX.'page
				WHERE id="'.intval($_GET['parent']).'"';
            $res = $db->query($sql);
            $parent = $res->getRow(0);

            if ($parent['template'] == 'sale-bonus')
                $_POST['dgallery'] = $_POST[$cur_lang.'_price'].'/'.$_POST[$cur_lang.'_newprice'].'/'.$_POST[$cur_lang.'_percent'];

            $sql = 'UPDATE '.SQL_PREFIX.'page
				SET parent="'.admin::neuter($_GET['parent']).'",
					hidden="'.($_POST['hidden'] == 'on' ? 'yes' : 'no').'",
					'.($_POST['dgallery'] ? 'gallery="'.$_POST['dgallery'].'",' : '').'
					page="'.admin::neuter($_GET['page']).'",
					'.$cur_lang.'="'.admin::neuter($_POST[$cur_lang.'_topic']).'",
					'.$cur_lang.'_topic="'.admin::neuter($_POST[$cur_lang.'_topic']).'",
					'.$cur_lang.'_text="'.admin::neuter($_POST[$cur_lang.'_text']).'",
					'.$cur_lang.'_lead="'.admin::neuter($_POST[$cur_lang.'_lead']).'",
					'.$cur_lang.'_author="'.admin::neuter($_POST[$cur_lang.'_author']).'",
					'.$cur_lang.'_meta_desc="'.admin::neuter($_POST[$cur_lang.'_meta_desc']).'",
					'.$cur_lang.'_meta_key="'.admin::neuter($_POST[$cur_lang.'_meta_key']).'"';

            if ($_POST['date'] && $_POST['time']) {
                $date_temp = explode('/', admin::neuter($_POST['date']));
                $time_temp = explode(':', admin::neuter($_POST['time']));

                if (!$time_temp[0])
                    $time_temp[0] = 0;
                if (!$time_temp[1])
                    $time_temp[1] = 0;

                $date = mktime($time_temp[0], $time_temp[1], 0, $date_temp[0], $date_temp[1], $date_temp[2]);
                $sql.= ', timestamp="'.$date.'"';
            }

//leave this
            $sql.= ' WHERE id="'.intval($_GET['id']).'"';
            $res = $db->query($sql);

            admin::generatehtaccess();
        } elseif ($_GET['page'] == 'portfolio') {
            $sql = 'UPDATE '.SQL_PREFIX.'portfolio
				SET gallery="'.$_POST['dgallery'].'",
					'.$cur_lang.'="'.admin::neuter($_POST[$cur_lang]).'",
					'.$cur_lang.'_topic="'.admin::neuter($_POST[$cur_lang.'_topic']).'",
					'.$cur_lang.'_text="'.admin::neuter($_POST[$cur_lang.'_text']).'",
					'.$cur_lang.'_author="'.admin::neuter($_POST[$cur_lang.'_author']).'",
					'.$cur_lang.'_meta_desc="'.admin::neuter($_POST[$cur_lang.'_meta_desc']).'",
					'.$cur_lang.'_meta_key="'.admin::neuter($_POST[$cur_lang.'_meta_key']).'"';

            if ($_POST['date']) {
                $date_temp = explode('/', admin::neuter($_POST['date']));
                $time_temp = explode(':', admin::neuter($_POST['time']));

                if (!$time_temp[0])
                    $time_temp[0] = 0;
                if (!$time_temp[1])
                    $time_temp[1] = 0;

                $date = mktime($time_temp[0], $time_temp[1], 0, $date_temp[0], $date_temp[1], $date_temp[2]);
                $sql.= ', timestamp="'.$date.'"';
            }

            $sql.= ' WHERE id="'.intval($_GET['id']).'"';
            $res = $db->query($sql);

            admin::generatehtaccess();
        } elseif ($_GET['page'] == 'users' || $_GET['page'] == 'profile') {
            if ($_POST['password'] == $_POST['password2'] && $_POST['password'])
                $passwd = md5($_POST['password'].admin::neuter($_POST['username']));

            $sql = 'UPDATE '.SQL_PREFIX.'user
				SET '.($_GET['page'] == 'users' && $passwd ? 'username="'.admin::neuter($_POST['username']).'", ' : '').'
					'.($passwd ? 'password="'.$passwd.'", ' : '').'
					email="'.admin::neuter($_POST['email']).'",
					'.($_GET['page'] == 'users' ? 'level="'.admin::neuter($_POST['level']).'",' : '').'
					dms_lang="'.admin::neuter($_POST['lang']).'"
				WHERE id="'.intval($_GET['id']).'"';
            $res2 = $db->query($sql);

            $url = URL.'/dms/?page='.$_GET['page'];
        } elseif ($_GET['page'] == 'stores') {
            $sql = 'UPDATE '.SQL_PREFIX.'stores
				SET parent="'.admin::neuter($_GET['parent']).'",
					type="'.$_POST['store_maintype'].'",
					store="'.$_POST['store_type'].'",
					'.$cur_lang.'="'.admin::neuter($_POST[$cur_lang.'_topic']).'",
					'.$cur_lang.'_contact="'.admin::neuter($_POST[$cur_lang.'_contact']).'",
					'.$cur_lang.'_text="'.admin::neuter($_POST[$cur_lang.'_text']).'",
					map="'.admin::neuter($_POST['map']).'"';
            $sql.= ' WHERE id="'.intval($_GET['id']).'"';
            $res = $db->query($sql);

            admin::generatehtaccess();
        }

        admin::admin_redirect($url);
    }

//delete single page
    function admin_delete_item($table, $id, $redirect) {
        global $db, $dms;

        if ($_GET['page'] == 'users' || $_GET['page'] == 'ftp') {
//TODO>avatar delete
            $sql = 'DELETE
				FROM '.SQL_PREFIX.($_GET['page'] == 'ftp' ? 'ftp_' : '').'user
				WHERE id="'.intval($_GET['id']).'"';
            $res = $db->query($sql);

            if ($redirect == '_self')
                $url = URL.'/dms/?page='.$_GET['page'].($_GET['parent'] ? '&parent='.$_GET['parent'] : '').($_GET['page'] == 'ftp' ? '&act=users' : '');
        } else {
            $param = array(
                'single'		=> false,
                'table'			=> 'images',
                'where'			=> 'parent="'.intval($id).'"',
            );
            $images = $dms->getSQL($param);

            for ($i = 0; $i < count($images); $i++) {
                $row = $images[$i];

                @unlink($row['path'].$row['filename']);
                @unlink($row['path'].'tnb/'.$row['filename']);
                @unlink($row['path'].'_orig/'.$row['filename']);
                @unlink($row['path'].'475/'.$row['filename']);
                @unlink($row['path'].'644/'.$row['filename']);
                @unlink($row['path'].'200/'.$row['filename']);
                @unlink($row['path'].'100/'.$row['filename']);
                @unlink($row['path'].'380/'.$row['filename']);
                @unlink($row['path'].'270/'.$row['filename']);
            }

            $sql = 'DELETE
				FROM '.SQL_PREFIX.'images
				WHERE parent="'.intval($id).'"';
            $res = $db->query($sql);

            $sql = 'DELETE
				FROM '.SQL_PREFIX.$table.'
				WHERE id="'.intval($id).'"';
            $res = $db->query($sql);

            if ($redirect == '_self')
                $url = URL.'/dms/?page='.$_GET['page'].($_GET['parent'] ? '&parent='.$_GET['parent'] : '').($_GET['cur_lang'] ? '&cur_lang='.$_GET['cur_lang'] : '');

            admin::generatehtaccess();
        }

        admin::admin_redirect($url);
    }

    function resizeJPG($sourcefile, $targetfile, $jpegqual, $maxX, $maxY, $type = 'resize') {
        $picsize = getimagesize($sourcefile);
        $src_x = $picsize[0];
        $src_y = $picsize[1];
        $src_ratio = $src_y / $src_x;

        if ($type == 'resize') {
            $pic_w = $maxX;
            $pic_h = round($maxX * $src_ratio);
            $src_y = round($src_x * $src_ratio);
            $start_y = round(($picsize[1] - $src_y) / 2);

            if (!isset($start_x))
                $start_x = 0;
            if (!$start_y)
                $start_y = 0;

            if ($start_y < 0) {
                $start_y = 0;
                $src_y = $picsize[1];
                $src_x = round($src_y / $src_ratio);
                $start_x = round(($picsize[0] - $src_x) / 2);
            }
        } elseif ($type == 'product') {
            $maxsize = $maxX;
            $pic_h = $maxsize;
            $pic_w = round($maxsize / $src_ratio);
            $src_y = round($src_x * $src_ratio);
            $start_y = round(($picsize[1] - $src_y) / 2);

            if (!isset($start_x))
                $start_x = 0;
            if (!$start_y)
                $start_y = 0;

            if ($start_y < 0) {
                $start_y = 0;
                $src_y = $picsize[1];
                $src_x = round($src_y / $src_ratio);
                $start_x = round(($picsize[0] - $src_x) / 2);
            }

            if ($pic_h < 200) {
                $pic_h = 200;
                $pic_w = $pic_h / $src_ratio;
            }
        }

        $source_id = imagecreatefromjpeg($sourcefile);
        $target_id = imagecreatetruecolor($pic_w, $pic_h);
        $target_pic = imagecopyresampled($target_id, $source_id,
            0, 0, $start_x, $start_y,
            $pic_w, $pic_h,
            $src_x, $src_y);
        imagejpeg ($target_id, $targetfile, $jpegqual);

        return true;
    }

    //TODO>get conf parameters to specify fields to count
    function admin_add_item() {
        global $db, $setup;

        $order = array(
            'setup' => 'id',
            'trans' => 'group_id',
        );
        $check = array (
            'setup' => '1',
            'trans' => $_POST['group_id'],
        );

        if ($_GET['page'] == 'trans' || $_GET['page'] == 'setup') {
            if ($check[admin::neuter($_GET['page'])]) {
                $cur_lang = admin::neuter($_POST['cur_lang']);

                $sql = 'INSERT
					INTO '.SQL_PREFIX.admin::neuter($_GET['page']).' (code, '.$cur_lang.($_GET['page'] == 'trans' ? ', group_id' : '').')
					VALUES ("'.admin::neuter($_POST['code']).'", "'.admin::neuter($_POST[$cur_lang]).'"'.($_GET['page'] == 'trans' ? ', "'.admin::neuter($_POST['group_id']).'"' : '').')';
                $res = $db->query($sql);
            }

            $url = URL.'/dms/?page='.$_GET['page'].($_GET['cur_lang'] ? '&cur_lang='.$_GET['cur_lang'] : '');
        } elseif ($_GET['page'] == 'site') {
            $cur_lang = admin::neuter($_GET['cur_lang']);

            $sql = 'SELECT MAX(jrk)
				FROM '.SQL_PREFIX.'page
				WHERE parent="'.(admin::neuter($_GET['parent']) ? admin::neuter($_GET['parent']) : 'parent').'"
					AND menu="'.$_POST['menu'].'"';
            $res = $db->query($sql);
            $row = $res->getRow(0);
            $jrk = $row[0] + 1;

            $filename = '';
            //TODO>image upload here
            /*
                    $filename = randomString(5).RenameFile(substr($_FILES['uploadfile']['name'], 0, strlen($_FILES['uploadfile']['name']) - 4)).'.'.strtolower(substr($_FILES['uploadfile']['name'], strlen($_FILES['uploadfile']['name']) - 3, strlen($_FILES['uploadfile']['name'])));
                    if (move_uploaded_file($_FILES['uploadfile']['tmp_name'], $image_param['uploaddir'].$filename)) {
                        chmod($image_param['uploaddir'].$filename, 0644);
                    } else {
                        $filename = '';
                    }

                    $icon = randomString(5).RenameFile(substr($_FILES['icon']['name'], 0, strlen($_FILES['icon']['name']) - 4)).'.'.strtolower(substr($_FILES['icon']['name'], strlen($_FILES['icon']['name']) - 3, strlen($_FILES['icon']['name'])));
                    if (move_uploaded_file($_FILES['icon']['tmp_name'], $image_param['uploaddir'].$icon)) {
                        chmod($image_param['uploaddir'].$icon, 0644);
                    } else {
                        $icon = '';
                    }
            */

            //TODO>time check
            $date_temp = explode('/', admin::neuter($_POST['date']));
            $time_temp = explode(':', admin::neuter($_POST['time']));
            if (count($date_temp) == 3)
                $date = mktime(($time_temp[0] ? $time_temp[0] : 0), ($time_temp[1] ? $time_temp[1] : 0), 0, $date_temp[0], $date_temp[1], $date_temp[2]);

            $sql = 'INSERT
				INTO '.SQL_PREFIX.'page (
					jrk,
					parent,
					page,
					menu,
					filename,
					owner_id,
					hidden,
					coming_soon,
					template,
					gallery,
					'.$cur_lang.',
					'.$cur_lang.'_topic,
					'.$cur_lang.'_text,
					'.$cur_lang.'_lead,
					'.$cur_lang.'_author,
					'.$cur_lang.'_meta_desc,
					'.$cur_lang.'_meta_key,
					'.$cur_lang.'_redirect_url,
					timestamp,
					displaydate)
				VALUES(
					"'.$jrk.'",
					"'.admin::neuter($_POST['parent']).'",
					"'.admin::neuter($_GET['page']).'",
					"'.admin::neuter($_POST['menu']).'",
					"'.$filename.'",
					"'.(isset($current_user['id'])?$current_user['id']:'').'",
					"'.($_POST['hidden'] == 'on' ? 'yes' : 'no').'",
					"'.($_POST['coming_soon'] == 'on' ? 'yes' : 'no').'",
					"'.admin::neuter($_POST['template']).'",
					"'.$_POST['dgallery'].'",
					"'.admin::neuter($_POST[$cur_lang]).'",
					"'.admin::neuter($_POST[$cur_lang.'_topic']).'",
					"'.admin::neuter($_POST[$cur_lang.'_text']).'",
					"'.admin::neuter($_POST[$cur_lang.'_lead']).'",
					"'.admin::neuter($_POST[$cur_lang.'_author']).'",
					"'.admin::neuter($_POST[$cur_lang.'_meta_desc']).'",
					"'.admin::neuter($_POST[$cur_lang.'_meta_key']).'",
					"'.admin::neuter($_POST[$cur_lang.'_redirect_url']).'",
					"'.mktime().'",
					"'.$date.'")';
            $res = $db->query($sql);

            admin::generatehtaccess();
            //TODO>select new ID and direct to change page
            $url = URL.'/dms/?page=site'.dms::showP();
        } elseif ($_GET['page'] == 'portfolio') {
            //TODO>include globally
            $cur_lang = admin::neuter($_GET['cur_lang']);
            include('setup.php');

            $sql = 'SELECT MAX(jrk)
				FROM '.SQL_PREFIX.'portfolio
				WHERE parent="'.admin::neuter($_GET['parent']).'"';
            $res = $db->query($sql);
            $row = $res->getRow(0);
            $jrk = $row[0] + 1;

            //TODO>image upload here
            //TODO>jpg resi\ze plus the others tooo!!!!!
            $filename = admin::randomString(5).$_FILES['uploadfile']['name'];
            $dir = 'upload/portfolio/';

            if (move_uploaded_file($_FILES['uploadfile']['tmp_name'], $dir.$filename)) {
                for ($i = 0; $i < count($setup[$_GET['page']]['specpicresizeparam']); $i++) {
                    $param = $setup[$_GET['page']]['specpicresizeparam'][$i];

                    if (!file_exists($dir.$param['dir'])) {
                        mkdir($dir.$param['dir']);
                        chmod($dir.$param['dir'], 0777);
                    }

                    admin::resizeJPG($dir.$filename, $dir.$param['dir'].'/'.$filename, $param['quality'], $param['x'], $param['y']);
                    @chmod($dir.$param['dir'].'/'.$filename, 0755);
                }
            } else {
                $filename = '';
            }

//TODO>time check
            $date_temp = explode('/', admin::neuter($_POST['date']));
            $time_temp = explode(':', admin::neuter($_POST['time']));
            $date = mktime(($time_temp[0] ? $time_temp[0] : 0), ($time_temp[1] ? $time_temp[1] : 0), 0, $date_temp[0], $date_temp[1], $date_temp[2]);

            $sql = 'INSERT
				INTO '.SQL_PREFIX.'portfolio (
					jrk,
					parent,
					filename,
					'.$cur_lang.',
					'.$cur_lang.'_topic,
					'.$cur_lang.'_text,
					'.$cur_lang.'_author,
					'.$cur_lang.'_meta_desc,
					'.$cur_lang.'_meta_key,
					timestamp)
				VALUES(
					"'.$jrk.'",
					"'.admin::neuter($_GET['parent']).'",
					"'.$filename.'",
					"'.admin::neuter($_POST[$cur_lang]).'",
					"'.admin::neuter($_POST[$cur_lang.'_topic']).'",
					"'.admin::neuter($_POST[$cur_lang.'_text']).'",
					"'.admin::neuter($_POST[$cur_lang.'_author']).'",
					"'.admin::neuter($_POST[$cur_lang.'_meta_desc']).'",
					"'.admin::neuter($_POST[$cur_lang.'_meta_key']).'",
					"'.$date.'")';
            $res = $db->query($sql);

//TODO>select new ID and direct to change page
            $url = URL.'/dms/?page=portfolio&parent='.$_GET['parent'];
        } elseif ($_GET['page'] == 'list') {
            $cur_lang = admin::neuter($_GET['cur_lang']);

            $sql = 'SELECT MAX(jrk)
				FROM '.SQL_PREFIX.'page
				WHERE parent="'.(admin::neuter($_GET['parent']) ? admin::neuter($_GET['parent']) : 'parent').'"';
            $res = $db->query($sql);
            $row = $res->getRow(0);
            $jrk = $row[0] + 1;

            if ($_POST['date'] && $_POST['time']) {
                $date_temp = explode('/', admin::neuter($_POST['date']));
                $time_temp = explode(':', admin::neuter($_POST['time']));
                $date = mktime(($time_temp[0] ? $time_temp[0] : 0), ($time_temp[1] ? $time_temp[1] : 0), 0, $date_temp[0], $date_temp[1], $date_temp[2]);
            }

            if ($_POST['hidden_product']) {
                $filename = admin::randomString(5).$_FILES['uploadfile']['name'];
                $dir = 'upload/products/groups/';

                if (move_uploaded_file($_FILES['uploadfile']['tmp_name'], $dir.$filename))
                    @chmod($dir.$filename, 0755);
                else
                    $filename = '';

                $_POST['dgallery'] = $filename;
            }

            $sql = 'SELECT *
				FROM '.SQL_PREFIX.'page
				WHERE id="'.intval($_GET['parent']).'"';
            $res = $db->query($sql);
            $parent = $res->getRow(0);

            if ($parent['template'] == 'sale-bonus')
                $_POST['dgallery'] = $_POST[$cur_lang.'_price'].'/'.$_POST[$cur_lang.'_newprice'].'/'.$_POST[$cur_lang.'_percent'];

            $sql = 'INSERT
				INTO '.SQL_PREFIX.'page (
					jrk,
					parent,
					hidden,
					timestamp,
					gallery,
					page,
					'.$cur_lang.',
					'.$cur_lang.'_topic,
					'.$cur_lang.'_text,
					'.$cur_lang.'_lead,
					'.$cur_lang.'_author,
					'.$cur_lang.'_meta_desc,
					'.$cur_lang.'_meta_key)
				VALUES(
					"'.$jrk.'",
					"'.admin::neuter($_GET['parent']).'",
					"'.(admin::neuter($_POST['hidden']) == 'on' ? 'yes' : 'no').'",
					"'.mktime().'",
					"'.$_POST['dgallery'].'",
					"'.admin::neuter($_GET['page']).'",
					"'.admin::neuter($_POST[$cur_lang.'_topic']).'",
					"'.admin::neuter($_POST[$cur_lang.'_topic']).'",
					"'.admin::neuter($_POST[$cur_lang.'_text']).'",
					"'.admin::neuter($_POST[$cur_lang.'_lead']).'",
					"'.admin::neuter($_POST[$cur_lang.'_author']).'",
					"'.admin::neuter($_POST[$cur_lang.'_meta_desc']).'",
					"'.admin::neuter($_POST[$cur_lang.'_meta_key']).'")';
            $res = $db->query($sql);

            admin::generatehtaccess();
            $url = URL.'/dms/?page=list&parent='.intval($_GET['parent']);
        } elseif ($_GET['page'] == 'users') {
            if ($_POST['password'] == $_POST['password2']) {
                $passwd = md5($_POST['password'].admin::neuter($_POST['username']));
//TODO>user level should be selectable
                $sql = 'INSERT
					INTO '.SQL_PREFIX.'user (username, password, level, email, dms_lang)
					VALUES ("'.admin::neuter($_POST['username']).'", "'.$passwd.'", 3, "'.admin::neuter($_POST['email']).'", "'.admin::neuter($_POST['lang']).'")';
                $res = $db->query($sql);
            }
//TODO>message on failed passwords
//TODO>check for duplicate username/email

            $url = URL.'/dms/?page='.$_GET['page'];
        } elseif ($_GET['page'] == 'stores') {
            $cur_lang = admin::neuter($_GET['cur_lang']);

            $sql = 'SELECT MAX(jrk)
				FROM '.SQL_PREFIX.admin::neuter($_GET['page']).'
				WHERE parent="'.(admin::neuter($_GET['parent']) ? admin::neuter($_GET['parent']) : 'parent').'"';
            $res = $db->query($sql);
            $row = $res->getRow(0);
            $jrk = $row[0] + 1;

            $sql = 'INSERT
				INTO '.SQL_PREFIX.admin::neuter($_GET['page']).' (
					'.$cur_lang.',
					'.$cur_lang.'_text,
					'.$cur_lang.'_contact,
					type,
					store,
					jrk,
					map,
					parent'.($_POST['store_maintype'] == 2 ? ', allowed' : '').'
				)
				VALUES (
					"'.admin::neuter($_POST[$cur_lang.'_topic']).'",
					"'.admin::neuter($_POST[$cur_lang.'_text']).'",
					"'.admin::neuter($_POST[$cur_lang.'_contact']).'",
					"'.$_POST['store_maintype'].'",
					"'.$_POST['store_type'].'",
					"'.$jrk.'",
					"'.admin::neuter($_POST['map']).'",
					"'.admin::neuter($_GET['parent']).'"'.($_POST['store_maintype'] == 2 ? ', "yes"' : '').'
				)';
            $res = $db->query($sql);

            if ($_POST['store_maintype'] == 2) {
                $sql = 'UPDATE '.SQL_PREFIX.admin::neuter($_GET['page']).'
					SET allowed="yes"
					WHERE id="'.admin::neuter($_GET['parent']).'"';
                $res = $db->query($sql);

                $sql = 'SELECT parent
					FROM '.SQL_PREFIX.admin::neuter($_GET['page']).'
					WHERE id="'.admin::neuter($_GET['parent']).'"';
                $res = $db->query($sql);
                $row = $res->getRow(0);

                $sql = 'UPDATE '.SQL_PREFIX.admin::neuter($_GET['page']).'
					SET allowed="yes"
					WHERE id="'.$row['parent'].'"';
                $res = $db->query($sql);
            }

            $url = URL.'/dms/?page='.$_GET['page'].($_GET['cur_lang'] ? '&cur_lang='.$_GET['cur_lang'] : '');
        }

        admin::admin_redirect($url);
    }

//TODO>include globally
    function admin_image_delete() {
        global $db, $setup;

        $table = 'images';
        if ($_GET['page'] == 'portfolio')
            $table = 'images_portfolio';
        elseif ($_GET['page'] == 'headerimage')
            $table = 'header';

        foreach ($_POST['deleteitem'] as $id) {
            $sql = 'SELECT filename'.($_GET['page'] == 'headerimage' ? '' : ', path').'
				FROM '.SQL_PREFIX.$table.'
				WHERE id='.$id;
            $res = $db->query($sql);
            $row = $res->getRow();

            if ($_GET['page'] == 'portfolio') {
                for ($i = 0; $i < count($setup[$_GET['page']]['specpicresizeparam']); $i++)
                    @unlink('upload/portfolio/'.$setup[$_GET['page']]['specpicresizeparam'][$i]['dir'].'/'.$row['filename']);

                @unlink('upload/portfolio/'.$row['filename']);
            } elseif ($_GET['page'] == 'headerimage') {
                @unlink('upload/header/'.$row['filename']);
            } else {
                @unlink($row['path'].$row['filename']);
                @unlink($row['path'].'tnb/'.$row['filename']);
                @unlink($row['path'].'_orig/'.$row['filename']);
                @unlink($row['path'].'475/'.$row['filename']);
                @unlink($row['path'].'644/'.$row['filename']);
                @unlink($row['path'].'200/'.$row['filename']);
                @unlink($row['path'].'100/'.$row['filename']);
                @unlink($row['path'].'380/'.$row['filename']);
                @unlink($row['path'].'270/'.$row['filename']);
            }

            $sql = 'DELETE
				FROM '.SQL_PREFIX.$table.'
				WHERE id="'.$id.'"';
            $res = $db->query($sql);
        }

        admin::admin_redirect();
    }

    function admin_item_delete() {
        global $db, $dms;

        $table = 'page';
        if ($_GET['page'] == 'stores')
            $table = 'stores';

        foreach ($_POST['deleteitem'] as $id) {
            $param = array(
                'single'		=> false,
                'table'			=> 'images',
                'where'			=> 'parent="'.$id.'"',
            );
            $images = $dms->getSQL($param);

            for ($j = 0; $j < count($images); $j++) {
                $row = $images[$j];

                @unlink($row['path'].$row['filename']);
                @unlink($row['path'].'tnb/'.$row['filename']);
                @unlink($row['path'].'_orig/'.$row['filename']);
                @unlink($row['path'].'475/'.$row['filename']);
                @unlink($row['path'].'644/'.$row['filename']);
                @unlink($row['path'].'200/'.$row['filename']);
                @unlink($row['path'].'100/'.$row['filename']);
                @unlink($row['path'].'380/'.$row['filename']);
                @unlink($row['path'].'270/'.$row['filename']);
            }

            $sql = 'DELETE
				FROM '.SQL_PREFIX.'images
				WHERE parent="'.$id.'"';
            $res = $db->query($sql);

            $sql = 'DELETE
				FROM '.SQL_PREFIX.$table.'
				WHERE id="'.$id.'"';
            $res = $db->query($sql);
        }

        admin::generatehtaccess();
        admin::admin_redirect();
    }


    function admin_file_delete() {
        global $db, $dms;

        foreach ($_POST['deleteitem'] as $id) {
            $param = array(
                'table'			=> 'files',
                'where'			=> 'id="'.$id.'"',
            );
            $file = $dms->getSQL($param);

            @unlink($file['path'].$file['filename']);

            $sql = 'DELETE
				FROM '.SQL_PREFIX.'files
				WHERE id="'.$id.'"';
            $res = $db->query($sql);
        }

        admin::admin_redirect();
    }


//TODO>include globally
    function admin_portfolio_delete() {
        global $db, $dms, $setup;

        foreach ($_POST['deleteitem'] as $id) {
            $param = array(
                'single'		=> false,
                'table'			=> 'images_portfolio',
                'where'			=> 'parent="'.$id.'"',
            );
            $images = $dms->getSQL($param);

            for ($j = 0; $j < count($images); $j++) {
                $row = $images[$j];

                for ($i = 0; $i < count($setup[$_GET['page']]['specpicresizeparam']); $i++)
                    @unlink('upload/portfolio/'.$setup[$_GET['page']]['specpicresizeparam'][$i]['dir'].'/'.$row['filename']);

                @unlink('upload/portfolio/'.$row['filename']);
            }

            $sql = 'DELETE
				FROM '.SQL_PREFIX.'portfolio
				WHERE id="'.$id.'"';
            $res = $db->query($sql);
        }

        admin::admin_redirect();
    }


//show / hide language
    function admin_toggle_lang() {
        global $db;

        $url = URL.'/dms/?page='.$_GET['page'];

        $sql = 'UPDATE '.SQL_PREFIX.'lang
			SET active="'.($_GET['val'] == 'yes' ? 'yes' : 'no').'"
			WHERE id="'.intval($_GET['id']).'"';
        $res = $db->query($sql);

        admin::admin_redirect($url);
    }


    function admin_item_add() {
        echo 'test';
        exit;
    }


    function admin_image_rotate() {
        global $db, $dms;

        $param = array(
            'table'			=> 'images',
            'where'			=> 'id="'.intval($_GET['id']).'"',
        );
        $image = $dms->getSQL($param);

		//'_orig/',
        $dirs = array('', '475/', '644/', 'tnb/');

        $dir = 90;
        if ($_GET['dir'] == 'left')
            $dir = -90;

        for ($i = 0; $i < count($dirs); $i++) {
            $img = imagecreatefromjpeg($image['path'].$dirs[$i].$image['filename']);
            $rot = imagerotate($img, $dir, 0);
            imagejpeg($rot, $image['path'].$dirs[$i].$image['filename'], 100);
        }

        admin::admin_redirect(URL.$_SESSION['return_url']);
        exit;
    }


    function admin_delete_image() {
        global $db, $dms;

        $param = array(
            'select'		=> 'filename',
            'where'			=> 'id="'.intval($_GET['id']).'"',
        );
        $image = $dms->getSQL($param);

        @unlink('upload/theme/').$image['filename'];
        $sql = 'UPDATE '.SQL_PREFIX.'page
			SET filename=""
			WHERE id="'.intval($_GET['id']).'"';
        $res = $db->query($sql);

        admin::admin_redirect(URL.$_SESSION['return_url']);
        exit;
    }


    function admin_ftp_add_item() {
        global $db, $dms;

        $param = array(
            'select'	=> 'id',
            'table'		=> 'ftp_user',
            'where'		=> 'username="'.admin::neuter($_POST['username']).'"',
        );
        $double = $dms->getSQL($param);

        if (!$double && $_POST['password'] == $_POST['password2'] && $_POST['password']) {
            $passwd = md5($_POST['password'].admin::neuter($_POST['username']));

            $sql = 'INSERT
				INTO '.SQL_PREFIX.'ftp_user (username, password)
				VALUES ("'.admin::neuter($_POST['username']).'", "'.$passwd.'")';
            $res = $db->query($sql);
        }

        admin::admin_redirect($_POST['redirect']);
        exit;
    }


    function admin_ftp_update_item() {
        global $db, $dms;

        if ($_POST['password'] == $_POST['password2'] && $_POST['password'])
            $passwd = md5($_POST['password'].admin::neuter($_POST['username']));

        $sql = 'UPDATE '.SQL_PREFIX.'ftp_user
			SET '.($_GET['page'] == 'users' && $passwd ? 'username="'.admin::neuter($_POST['username']).'", ' : '').'
				'.($passwd ? 'password="'.$passwd.'"' : '').'
			WHERE id="'.intval($_GET['id']).'"';
        $res = $db->query($sql);

        admin::admin_redirect(substr($_POST['redirect'], 0, strpos($_POST['redirect'], '&id=')));
        exit;
    }


    function admin_ftp_save_user_access() {
        global $db, $dms;

        for ($i = 0; $i < count($_POST['files']); $i++) {
            $access.= (isset($access) && $access ? ';' : '').$_POST['files'][$i];
        }

        $param = array(
            'table'		=> 'ftp_access',
            'where'		=> 'user_id="'.$_POST['user'].'"',
        );
        $check = $dms->getSQL($param);

        if ($check) {
            //update
            $sql = 'UPDATE '.SQL_PREFIX.'ftp_access
				SET access="'.$access.'"
				WHERE user_id="'.$_POST['user'].'"';
        } else {
            //add
            $sql = 'INSERT
				INTO '.SQL_PREFIX.'ftp_access (user_id, access)
				VALUES ("'.$_POST['user'].'", "'.$access.'")';
        }

        $res = $db->query($sql);

        admin::admin_redirect($_POST['redirect']);
        exit;
    }


    function admin_ftp_save_schedule() {
        global $db, $dms;

        $dir = '../ftp/';
        $filelist = glob($dir . "*");

        for ($i = 0; $i < count($filelist); $i++) {
            $name = str_replace('../ftp/', '', $filelist[$i]);

            $lpos = strrpos($name, '.');
            $name = substr($name, 0, $lpos).'_'.substr($name, $lpos + 1);

            $param = array(
                'table'		=> 'ftp_schedule',
                'where'		=> 'filename="'.$name.'"',
            );
            $check = $dms->getSQL($param);

            if ($check) {
//update
                $sql = 'UPDATE '.SQL_PREFIX.'ftp_schedule
					SET date="'.$_POST[$name].'"
					WHERE filename="'.$name.'"';
            } else {
//add
                $sql = 'INSERT
					INTO '.SQL_PREFIX.'ftp_schedule (filename, date)
					VALUES ("'.$name.'", "'.$_POST[$name].'")';
            }
            $res = $db->query($sql);
        }

        admin::admin_redirect($_POST['redirect']);
        exit;
    }


    function admin_ftp_save_file_access() {
        global $db, $dms;

        for ($i = 0; $i < count($_POST['files']); $i++) {
            $access.= (isset($access) && $access ? ';' : '').$_POST['files'][$i];
        }

        $param = array(
            'single'	=> false,
            'table'		=> 'ftp_user',
        );
        $check = $dms->getSQL($param);
        $sql = 'SELECT user.*, access.access
			FROM '.SQL_PREFIX.'ftp_user as user
			LEFT JOIN '.SQL_PREFIX.'ftp_access as access
				ON access.user_id=user.id';
        $res = $db->query($sql);
        $n = $res->getRowCount();

        $param = array(
            'table'		=> 'ftp_access',
        );

        for ($i = 0; $i < $n; $i++) {
            $user = $res->getRow();

            $param['where'] = 'user_id="'.$user['id'].'"';
            $check = $dms->getSQL($param);

            if ($check) {
//update
                $access_list = explode(';', $user['access']);
                $nulist = array_merge(explode(';', $user['access']), explode(';', $access));
                $nulist = array_unique($nulist);

                unset($nuaccess);
                for ($j = 0; $j < count($nulist); $j++)
                    $nuaccess.= (isset($nuaccess) && $nuaccess ? ';' : '').$nulist[$j];

                $sql = 'UPDATE '.SQL_PREFIX.'ftp_access
					SET access="'.$nuaccess.'"
					WHERE user_id="'.$user['id'].'"';
            } else {
//add
                $sql = 'INSERT
					INTO '.SQL_PREFIX.'ftp_access (user_id, access)
					VALUES ("'.$user['id'].'", "'.$access.'")';
            }

            $db->query($sql);
        }

        admin::admin_redirect($_POST['redirect']);
        exit;
    }


}

