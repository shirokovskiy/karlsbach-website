
ALTER TABLE `dms_ftp_access` MODIFY `access` BLOB;          -- text
ALTER TABLE `dms_ftp_schedule` MODIFY `filename` BLOB;      -- varchar(250)
ALTER TABLE `dms_images` MODIFY `pyc` BLOB;                 -- varchar(1000)
ALTER TABLE `dms_lang` MODIFY `display` BLOB;               -- text
ALTER TABLE `dms_page` MODIFY `deu` BLOB;                   -- varchar(1000)
ALTER TABLE `dms_page` MODIFY `deu_text` BLOB;              -- text
ALTER TABLE `dms_page` MODIFY `deu_topic` BLOB;             -- varchar(1000)
ALTER TABLE `dms_page` MODIFY `deu_meta_desc` BLOB;         -- varchar(2500)
ALTER TABLE `dms_page` MODIFY `deu_meta_key` BLOB;          -- varchar(2500)
ALTER TABLE `dms_page` MODIFY `est` BLOB;                   -- повтор, см. выше
ALTER TABLE `dms_page` MODIFY `est_text` BLOB;
ALTER TABLE `dms_page` MODIFY `est_meta_desc` BLOB;
ALTER TABLE `dms_page` MODIFY `est_meta_key` BLOB;
ALTER TABLE `dms_page` MODIFY `pyc` BLOB;
ALTER TABLE `dms_page` MODIFY `pyc_text` BLOB;
ALTER TABLE `dms_page` MODIFY `pyc_topic` BLOB;
ALTER TABLE `dms_page` MODIFY `pyc_meta_desc` BLOB;
ALTER TABLE `dms_page` MODIFY `pyc_meta_key` BLOB;
ALTER TABLE `dms_stores` MODIFY `est` BLOB;                 -- varchar(100)
ALTER TABLE `dms_stores` MODIFY `est_text` BLOB;            -- varchar(1000)
ALTER TABLE `dms_stores` MODIFY `pyc` BLOB;                 -- varchar(100)
ALTER TABLE `dms_stores` MODIFY `pyc_text` BLOB;            -- varchar(1000)
ALTER TABLE `dms_stores` MODIFY `pyc_contact` BLOB;         -- varchar(500)
ALTER TABLE `dms_trans` MODIFY `est` BLOB;                  -- varchar(1000)
ALTER TABLE `dms_trans` MODIFY `pyc` BLOB;                  -- varchar(1000)

ALTER DATABASE `karlsbach` CHARSET=utf8; -- но можно поменять в phpMyAdmin

ALTER TABLE `dms_ftp_access` MODIFY `access` text CHARACTER SET utf8;
ALTER TABLE `dms_ftp_schedule` MODIFY `filename` varchar(255) CHARACTER SET utf8;
ALTER TABLE `dms_images` MODIFY `pyc` varchar(1000) CHARACTER SET utf8;
ALTER TABLE `dms_lang` MODIFY `display` text CHARACTER SET utf8;
ALTER TABLE `dms_page` MODIFY `deu` varchar(1000) CHARACTER SET utf8;
ALTER TABLE `dms_page` MODIFY `deu_text` text CHARACTER SET utf8;
ALTER TABLE `dms_page` MODIFY `deu_topic` varchar(1000) CHARACTER SET utf8;
ALTER TABLE `dms_page` MODIFY `deu_meta_desc` text CHARACTER SET utf8;
ALTER TABLE `dms_page` MODIFY `deu_meta_key` text CHARACTER SET utf8;
ALTER TABLE `dms_page` MODIFY `est` varchar(1000) CHARACTER SET utf8;
ALTER TABLE `dms_page` MODIFY `est_text` text CHARACTER SET utf8;
ALTER TABLE `dms_page` MODIFY `est_meta_desc` text CHARACTER SET utf8;
ALTER TABLE `dms_page` MODIFY `est_meta_key` text CHARACTER SET utf8;
ALTER TABLE `dms_page` MODIFY `pyc` varchar(1000) CHARACTER SET utf8;
ALTER TABLE `dms_page` MODIFY `pyc_text` text CHARACTER SET utf8;
ALTER TABLE `dms_page` MODIFY `pyc_topic` varchar(1000) CHARACTER SET utf8;
ALTER TABLE `dms_page` MODIFY `pyc_meta_desc` text CHARACTER SET utf8;
ALTER TABLE `dms_page` MODIFY `pyc_meta_key` text CHARACTER SET utf8;
ALTER TABLE `dms_stores` MODIFY `est` varchar(100) CHARACTER SET utf8;
ALTER TABLE `dms_stores` MODIFY `est_text` varchar(1000) CHARACTER SET utf8;
ALTER TABLE `dms_stores` MODIFY `pyc` varchar(100) CHARACTER SET utf8;
ALTER TABLE `dms_stores` MODIFY `pyc_text` varchar(1000) CHARACTER SET utf8;
ALTER TABLE `dms_stores` MODIFY `pyc_contact` varchar(500) CHARACTER SET utf8;
ALTER TABLE `dms_trans` MODIFY `est` varchar(1000) CHARACTER SET utf8;
ALTER TABLE `dms_trans` MODIFY `pyc` varchar(1000) CHARACTER SET utf8;
