<?php
if ($_GET['file']) {
	$list = glob('upload/pdf/' . "*.pdf");

	$temp = explode('/', $_GET['file']);
	$file = $temp[count($temp) - 1];
	$file = str_replace('.pdf', '', $file);

	if (in_array('upload/pdf/'.$file.'.pdf', $list)) {
		header("Content-Type: application/octet-stream");

		$file = $file.'.pdf';
		header("Content-Disposition: attachment; filename=" . urlencode($file));
		header("Content-Type: application/force-download");
		header("Content-Type: application/octet-stream");
		header("Content-Type: application/download");
		header("Content-Description: File Transfer");
		header("Content-Length: " . filesize($file));
		flush(); // this doesn't really matter.
		$fp = fopen('upload/pdf/'.$file, "r");
		while (!feof($fp)) {
		    echo fread($fp, 65536);
    		flush(); // this is essential for large downloads
		}
		fclose($fp);
	}
}
