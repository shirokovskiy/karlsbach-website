<?php
header('Content-type: application/json; charset=utf-8');
include('conf.php');

$lang = substr($_GET['lang'], 0, 3);

$db->query("SET NAMES 'utf8'");

$sql = 'SELECT *
		FROM '.SQL_PREFIX.'stores
		WHERE allowed="yes"
		ORDER BY jrk DESC';
$res = $db->query($sql);
$n = $res->getRowCount();

echo '{"markers":[ ';
for ($i = 0; $i < $n; $i++) {
	$row = $res->getRow(0);

	$pos = explode(',', $row['map']);

	$address = strip_tags(stripslashes($row[$lang.'_text']));
	$address = preg_replace("/[\n|\r|\t]+/", "", $address);
	$contact = strip_tags($row[$lang.'_contact']);
	$contact = nl2br(stripslashes($contact));
    $contact = preg_replace("/[\n|\r|\t]+/", "", $contact);

	echo '{ "latitude":'.trim($pos[0]).', "longitude":'.trim($pos[1]).', "title":"'.htmlspecialchars(stripslashes($row[$lang])).'", "content":"'.(!empty($address)?$address.'<br />':'').$contact.'" }'.($i == $n - 1 ? '' : ',');
}

echo ']}';
