#!/usr/bin/env bash
CUR_DATE="$(date +%Y-%m-%d__%H.%M)"
SOURCE="${BASH_SOURCE[0]}"
THIS_DIR="$(cd -P "$( dirname "$SOURCE" )" && pwd)"
cd $THIS_DIR
cd "../"
KB_DIR="$(pwd)"

BKP_DIR=$KB_DIR/shell/backups
DBNAME=karlsbach
DBFILE=$DBNAME.sql

DESTINATION=$BKP_DIR/$DBFILE
DESTINATIONBZ=$DESTINATION.bz2

if [ -f $DESTINATIONBZ ]
then
	echo "Delete previous bz2 file"
	mv -f $DESTINATIONBZ DESTINATION.$CUR_DATE.bkp.bz2
fi

if [ -f $DESTINATION ]
then
	echo "Backup previous SQL file"
	mv -f $DESTINATION $DESTINATION.$CUR_DATE.bkp.sql
fi

echo "Start backup remote database: $DBNAME"
ssh kb "mysqldump --defaults-extra-file=mysql.cnf --opt $DBNAME | bzip2 -9" > $DESTINATIONBZ
if [ -f $DESTINATIONBZ ]
then
    echo "Start unzip: $DESTINATIONBZ"
    bunzip2 $DESTINATIONBZ
    echo "Run DB restore process"
	mysql --defaults-extra-file=$KB_DIR/shell/mysql.cnf $DBNAME < $DESTINATION
	mv -f $DESTINATION $DESTINATION.$(date +%Y-%m-%d__%H.%M).restored.sql
	echo "RSYNC run..."

	TIME_LIMIT=5
	choice=""
    echo "Are you sure you want to RSYNC files? <y/N>"
	read -t $TIME_LIMIT choice

	if [ ! -z "$choice" ]
	then
	    case "$choice" in
          y|Y ) rsync -uar kb:www/www/ $KB_DIR;;
          n|N ) echo "no RSYNCing";;
          * ) echo "invalid choise :( no RSYNCing";;
        esac
    else
        echo "No choice for RSYNCing"
	fi

else
    echo "File has not downloaded: $DESTINATIONBZ"
fi

