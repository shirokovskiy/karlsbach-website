<?php
$setup = array(
    //main site conf
	'site' => array(
		'parentedit'			=> true,
		'templateedit' 			=> true,
		'locationedit'			=> true,
		'specpic'				=> false,
		'specpicsize'			=> 836,
		'showSEOlink'			=> true,
		'seoedit'				=> false,
		'hiddenedit'			=> true,
		'redirectedit'			=> true,
		'metadescedit'			=> true,
		'metakeyedit'			=> true,
		'menutopicedit'			=> true,
		'topicedit'				=> true,
		'dateedit'				=> false,
		'leadedit'				=> false,
		'textedit'				=> true,
		'adminlist'				=> array(
			'mainadd'			=> true,
			'level'				=> 2,
		),
	),

    //lists like blog, news
	'list' => array(
		'hiddenedit'			=> true,
		'showSEOlink'			=> true,
		'metadescedit'			=> true,
		'metakeyedit'			=> true,
		'menutopicedit'			=> false,
		'topicedit'				=> true,
		'dateedit'				=> true,
		'leadedit'				=> false,
		'textedit'				=> true,
		'adminlist'				=> array(
			'mainadd'			=> true,
			'level'				=> 1,
		),
	),

    //stores
	'stores' => array(
		'parentedit'			=> true,
		'topicedit'				=> true,
		'dateedit'				=> false,
		'leadedit'				=> false,
		'textedit'				=> true,
		'adminlist'				=> array(
			'mainadd'			=> true,
			'level'				=> 2,
		),
	),

    //lists like blog, news
	'portfolio' => array(
		'showSEOlink'			=> false,
		'specpic'				=> false,
		'specpicsize'			=> 130,
		'specpicresize'			=> true,
		'specpicresizeparam'	=> array(
			0					=> array(
				'dir'			=> '150',
				'x'				=> 150,
				'y'				=> 75,
				'quality'		=> 75,
			),
			1					=> array(
				'dir'			=> '250',
				'x'				=> 250,
				'y'				=> 250,
				'quality'		=> 80,
			),
			2					=> array(
				'dir'			=> '450',
				'x'				=> 450,
				'y'				=> 250,
				'quality'		=> 85,
			),
			3					=> array(
				'dir'			=> '800',
				'x'				=> 800,
				'y'				=> 600,
				'quality'		=> 85,
			),
		),
		'hiddenedit'			=> false,
		'metadescedit'			=> false,
		'metakeyedit'			=> false,
		'menutopicedit'			=> false,
		'topicedit'				=> true,
		'dateedit'				=> false,
		'leadedit'				=> false,
		'textedit'				=> true,
		'portfoliolink'			=> true,
		'adminlist'				=> array(
			'mainadd'			=> true,
			'level'				=> 0,
		),
	),
);
