<?php
if (!isset($limit)) {
    $limit = 'all';
}

//default values
$dmsdefault = array(
    'default' => array(
        'theme'					=> 'default',
        'blog_lead_show'		=> false,
        'blog_items_per_page'	=> 5,
        'blog_date_format'		=> 'd.m.Y',
    ),

    //sql query default values
    'sql' => array(
        'obj'			=> false,
        'single'		=> true,
        'fields'		=> '*',
        'table'			=> 'page',
        'where'			=> '',
        'order'			=> '',
        'limit'			=> '',
    ),

    //header imgaes
    'headerimage' => array(
        'single'	=> false,
        'table'		=> 'header',
        'where'		=> 'filename!=""',
        'order'		=> 'RAND()',
        'limit'		=> ($limit == 'all' ? '' : $limit),
        'default'	=> 'headerimage.jpg',
    ),

    //language display
    'lang' => array(
        'display'		=> 'text', // text || image
        'dir'			=> URL.'/'.TEMPLATE_DIR.'default/images/lang/',
        'hover'			=> true,
        'hover_image'	=> 'A',
        'filetype'		=> '.jpg',
        'show_active'	=> true,
        'mainclass'		=> 'lang',
    ),

    //SEO link
    'seo' => array(
        'allow_language'=> true,
        'table'			=> 'page',
    ),

    //parent tree
    'tree' => array(
        'showid'		=> false,
        'show_real_name'=> false,
        'table'			=> 'page',
        'display_add'	=> '',
    ),

    //menu
    'menu' => array(
        'parent'		=> 'main',
        'style'			=> 'ul',
        'sqlparent'		=> 'parent',
        'add_before'	=> '',
        'add_after'		=> '',
        'location'		=> 'main',
        'expandtolower'	=> array('main'),
    ),

    //dms main menu
    'mainmenu' => array(
        0 => array(
            'name' => 'users',
            'level' => 2,
            'icon' => 'http://www.karlsbach.eu/template/dms/images/icon/users.jpg',
            'translation' => 'dms_menu_users',
        ),
        1 => array(
            'name' => 'trans',
            'level' => 2,
            'icon' => 'http://www.karlsbach.eu/template/dms/images/icon/trans.jpg',
            'translation' => 'dms_menu_dmstranslation',
        ),
        2 => array(
            'name' => 'setup',
            'level' => 2,
            'icon' => 'http://www.karlsbach.eu/template/dms/images/icon/setup.jpg',
            'translation' => 'dms_menu_setup',
        ),
        3 => array(
            'name' => 'lang',
            'level' => 1,
            'icon' => 'http://www.karlsbach.eu/template/dms/images/icon/lang.jpg',
            'translation' => 'dms_menu_languages',
        ),
        /*
            4 => array(
                'name' => 'headerimage',
                'level' => 2,
                'icon' => 'http://www.leonardo-da-vinci-biography.com/images/leonardo-da-vinci-mona-lisa.0.jpg',
                'translation' => 'dms_menu_headerimages',
            ),
        */
        5 => array(
            'name' => 'ftp',
            'level' => 2,
            'icon' => '/template/dms/images/icon/ftp_icon.png',
            'translation' => 'dms_menu_ftp_upload',
        ),
    ),

    'sidemenu' => array(
        0 => array(
            'allow' => true,
            'name' => 'site',
            'level' => 4,
            'display' => '',
        ),
    ),

    //allowed templates
    'allowed_templates' => array('site', 'users', 'trans', 'list', 'setup', 'portfolio', 'profile', 'lang', 'headerimage', 'stores', 'ftp'),

    'mainlist' => array(
        'page' => 'page',			//sql page
        'level' => 0,				//starting level
        'where' => ' 1',			//sql WHERE
        'order' => 'jrk',			//sql ORDER
        'recursive' => true,		//recursive loop
    ),

    'editor' => array(
        'text' => array(
            'recursive' => true,		//recursive loop
        ),
    ),

    'gallery' => array(
        'galleryimage'	=> 'gallery.png',
    ),

    //portfolio defaults
    //points to setup.php > portfolio > specpicresizeparam number
    'portfolio' => array(
        'footerpath'	=> 0,
        'pagepath'		=> 1,
        'innerlink'		=> true,
        'galleryimage'	=> 'portfolio.png',
    ),
);
