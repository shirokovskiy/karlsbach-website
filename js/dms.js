$(function() {
//in case we have #auth_username e.g. it's login page we focus on #auth_username input field
	if ($("#auth_username").size())
		$("#auth_username").focus();

//unselect while clicking on the empty box
	$("#galleryBox").click(function() {
		$('.ui-selected', '#selectable').each(function() {
			$('#selectable li').removeClass();
		});
	});

	$("#inserDGallery").click(function() {
//import site URL
		var URL = $('#dgalleryURL').val();

		$('.ui-selected', '#selectable').each(function() {
			if ($('#selectable li').index( this ) >= 0) {
				var index = $('#selectable li').index(this);
				var image = $('#item' + (index)).val();
				var dir = $('#dir' + (index)).val();
				var add = $('#add' + (index)).val();

				$('#select-result').val($('#select-result').val() + '<li class="itemimage"><a rel="gallery" href="' + URL + '/' + dir + image + '" class="opengallery" style="background-image:url(\'' + URL + '/' + dir + add + image + '\');">&nbsp;</a></li>');

//				$('#select-result').val($('#select-result').val() + '<li class="itemimage"><a href="' + URL + '/upload/images/' + image + '" class="opengallery"><img src="' + URL + '/upload/images/tnb/' + image + '" alt="" /></a></li>');
			}
		});

		var oEditor = CKEDITOR.instances.editorid;
		var html = '<ul class="gallery">' + $('#select-result').val() + '</ul>';
		var newElement = CKEDITOR.dom.element.createFromHtml( html, oEditor.document );
		oEditor.insertElement( newElement );
//not the best solution as we'll have space in the beginning of line
//but without it empty tab will be deleted
		var html = '<p>&nbsp;</p>';
		var newElement = CKEDITOR.dom.element.createFromHtml( html, oEditor.document );
		oEditor.insertElement( newElement );

//TODO>place cursor outside of gallery!
/*
var s = oEditor.getSelection();
// do something
var range = new CKEDITOR.dom.range( oEditor.document );
range.startOffset = 20;
range.endOffset = 20;
s.selectRanges(range); // restore it
*/
		$('#dgallerycontrol').hide();
		$('#galleryBox').hide();
	});

	$("#closeDGallery").click(function() {
		$('#dgallerycontrol').hide();
		$('#galleryBox').hide();
	});
});

function insertDGallery() {
	$('#galleryBox').show();
	$('#dgallerycontrol').show();
	$('#select-result').val('');
	$('#selectable').selectable('destroy');

//disable visual selection
	$('.ui-selected', '#selectable').each(function() {
		$('#selectable li').removeClass();
		$('#selectable li').addClass('ui-state-default');
	});

	$('#selectable').selectable();
}