$(function() {
    $(window).resize(function() {
		if ($('html').width() < 980) {
			$('html').css('overflow-x', 'auto');
		} else {
			$('html').css('overflow-x', 'hidden');
		}
	});
});