<?php
ob_start();
session_start();

require_once 'conf.php';

if (!isset($_SESSION['GeoPositioned']) && !preg_match('/dms/', $_SERVER['REQUEST_URI'])) {
    $myIP = $_SERVER['REMOTE_ADDR']=='127.0.0.1' ? '188.134.80.143' : $_SERVER['REMOTE_ADDR']; // Saint-Petersburg, Russia
    $myIP = $_SERVER['REMOTE_ADDR']=='127.0.0.1' ? '66.249.78.29' : $_SERVER['REMOTE_ADDR']; // Beverly Hills, USA
    $myIP = $_SERVER['REMOTE_ADDR']=='127.0.0.1' ? '144.76.166.251' : $_SERVER['REMOTE_ADDR']; // Kiez, Germany
    $myIP = $_SERVER['REMOTE_ADDR']=='127.0.0.1' ? '81.7.14.169' : $_SERVER['REMOTE_ADDR']; // Hermsdorf, Germany
    $myIP = $_SERVER['REMOTE_ADDR']=='127.0.0.1' ? '79.134.215.30' : $_SERVER['REMOTE_ADDR']; // Tallin, Estonia
    $myIP = $_SERVER['REMOTE_ADDR']=='127.0.0.1' ? '62.65.192.82' : $_SERVER['REMOTE_ADDR']; // Tallin, Estonia

    $objGeoIP = geoip_open(GEOIPCITY_DAT, GEOIP_STANDARD);
    $rcGeoIP = geoip_record_by_addr($objGeoIP, $myIP);

    if ( !is_null($rcGeoIP) ) {
        $_SESSION['GeoPositioned'] = true;
        if (isset($rcGeoIP->country_name)) {
            switch ($rcGeoIP->country_name) {
                case 'Estonia':
                    if (!preg_match("/^\/est\//", $_SERVER['REQUEST_URI'])) {
                        $_SERVER['REQUEST_URI'] = preg_replace("/^\/.{3}\//", "/est/", $_SERVER['REQUEST_URI']);
                        header('Location: '.$_SERVER['REQUEST_URI']);
                        exit;
                    }
                    break;
                case 'Germany':
                    if (!preg_match("/^\/deu\//", $_SERVER['REQUEST_URI'])) {
                        $_SERVER['REQUEST_URI'] = preg_replace("/^\/.{3}\//", "/deu/", $_SERVER['REQUEST_URI']);
                        header('Location: '.$_SERVER['REQUEST_URI']);
                        exit;
                    }
                    break;
                case 'Russian Federation':
                    if ($_SERVER['REQUEST_URI'] != '/') {
                        if (!preg_match("/^\/pyc\//", $_SERVER['REQUEST_URI'])) {
                            $_SERVER['REQUEST_URI'] = preg_replace("/^\/.{3}\//", "/pyc/", $_SERVER['REQUEST_URI']);
                            header('Location: '.$_SERVER['REQUEST_URI']);
                            exit;
                        }
                    }
                    break;

                default:
                    if ($_SERVER['REQUEST_URI'] != '/eng/') {
                        header('Location: /eng/');
                        exit;
                    }
                    break;
            }
        } else {
            #
        }
    }

    geoip_close($objGeoIP);
}

$dms->site();
$dms->outPut(ob_get_contents(), true);
$dms->clearBuffer();

if ($_SERVER['REMOTE_ADDR'] == '188.134.80.143') {
    echo "<!-- ".$_SERVER['REMOTE_ADDR']."-->";
    echo "<!-- ".$_SERVER['REQUEST_URI']."-->";
}
