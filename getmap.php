<?php
// map location getter for DMS

if ($_GET['address']) {
	$url = str_replace(' ', '+', trim($_GET['address']));

	$file = file('http://maps.googleapis.com/maps/api/geocode/json?address='.$url.'&sensor=true');

	for ($i = 0; $i < count($file); $i++) {
		$temp = explode(' : ', $file[$i]);
		$temp[0] = str_replace('"', '', trim($temp[0]));

		if ($temp[0] == 'location')
			$mark = $i;
	}

	$lat_temp = explode(' : ', $file[$mark + 1]);
	$lng_temp = explode(' : ', $file[$mark + 2]);

	$lat = trim($lat_temp[1]);
	$lng = trim($lng_temp[1]);

	echo $lat.$lng;
}
