<?php
ob_start();
session_start();

require_once 'conf.php';

$sql = 'SELECT *
		FROM '.SQL_PREFIX.'page
		WHERE id="'.intval($_GET['id']).'"';
$res = $db->query($sql);
$row = $res->getRow(0);

//get only 3 first digits for lang
$lang = substr($_GET['lang'], 0, 3);

if ($_GET['type'] == 'news') {
	$sql = 'SELECT '.$lang.'
			FROM '.SQL_PREFIX.'page
			WHERE id="'.$row['parent'].'"
			LIMIT 1';
	$res = $db->query($sql);
	$parent = $res->getRow(0);

	$sql = 'SELECT *
			FROM '.SQL_PREFIX.'images
			WHERE parent="'.intval($_GET['id']).'"
			ORDER BY jrk
			LIMIT 1';
	$res = $db->query($sql);
	$image = $res->getRow(0);
} elseif ($_GET['type'] == 'bonus') {
	$sql = 'SELECT *
			FROM '.SQL_PREFIX.'images
			WHERE parent="'.intval($_GET['id']).'"
			ORDER BY jrk
			LIMIT 4';
	$res = $db->query($sql);
}

if ($_GET['type'] == 'bonus') {
	$output.= '
<script>
$(".imageover").hover(function() {
	var style = $(this).attr("style");
	var nu = style.substr(23, style.length - 26);
	nu = nu.replace("/100/", "/475/");

	$("#bonus_real_image").css("background-image", "url("+nu+")");
}, function() {
	var def = $("#bonus_real_image_default").val();

	$("#bonus_real_image").css("background-image", "url("+def+")");
});
</script>

<div class="whitebox_images">
<div class="whitebox_small_images">';

	for ($i = 0; $i < 4; $i++) {
		$image = $res->getRow(0);

		if ($i == 0)
			$mainimage = $image;
		else
			if ($image['filename'] && file_exists($image['path'].'100/'.$image['filename']))
				$output.= '
<a rel="gallery" href="'.URL.'/'.$image['path'].$image['filename'].'" title="" class="imageover opengallery whitebox_image_'.($i + 1).'" style="background-image: url(\''.URL.'/'.$image['path'].'100/'.$image['filename'].'\');"></a>';
	}

	$output.= '
</div>
<input type="hidden" id="bonus_real_image_default" value="'.URL.'/'.$mainimage['path'].'475/'.$mainimage['filename'].'" />
<a rel="gallery" href="'.URL.'/'.$mainimage['path'].$mainimage['filename'].'" title="" class="opengallery whitebox_image" style="background-image: url(\''.URL.'/'.$mainimage['path'].'475/'.$mainimage['filename'].'\');" id="bonus_real_image"></a>';

	$output.= '
</div>';
}

$output.= '
<img src="'.DIR.'images/close.png" alt="close" class="whitebox_close" onclick="closewhitebox()" />';

if ($_GET['type'] == 'bonus')
	$output.= '
<div class="whitebox_bonus_text">';

$output.= '
<h1 class="topic">'.stripslashes($row[$lang.'_topic']).'</h1>';

if ($_GET['type'] == 'news') {
	$output.= '
<h2 class="topic">'.stripslashes($parent[$lang]).'</h2>
<h2 class="topic topic_bold">'.date('d.m', $row['timestamp']).'</h2>
<div class="whitebox_image whitebox_image_news" style="background-image: url(\''.URL.'/'.$image['path'].'644/'.$image['filename'].'\');"></div>';
}
$output.= '
<div class="whotebox_content">'.stripslashes($row[$lang.'_text']).'</div>';

if ($_GET['type'] == 'bonus' && $row['gallery'] && $row['gallery'] != 'none') {
	$sql = 'SELECT '.$lang.'
			FROM '.SQL_PREFIX.'trans
			WHERE code="whitebox_price" || code="currency"';
	$res = $db->query($sql);
	$trans = $res->getRow(0);

	$price = explode('/', $row['gallery']);

	$output.= '
  <h1 class="topic">'.stripslashes($trans[$lang]).'</h1>';

	$trans = $res->getRow(0);

	$output.= '
  <div class="oldprice">'.$price[0].' '.stripslashes($trans[$lang]).'</div>
  <div class="newprice">'.$price[1].' '.stripslashes($trans[$lang]).'</div>
  <div class="percent">-'.$price[2].' %</div>
</div>';
}

echo $output;
